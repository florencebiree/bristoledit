============================
Bristoledit internal plugins
============================

Some plugins provide core Bristoledit functionality, and are loaded
automatically. Here is the list:

asyncrun
========

Allow to run a command in the background

filecompleter
=============

Provides autocompletion when browsing the file system.

pythonshell
===========

Provides a python shell for the command bar (the shell of CTRL+T).

pythonshellcompleter
====================

Provides useful autocompletion for the python shell.

