====================
interpreter protocol
====================

An interpreter is a class that satisfy the given protocol. A plugin that add an
interpreter must return the interpreter class, not an instance of the
interpreter.

::

    # see bristol.plugins.pythonshell.PythonShell
    # for implementation example
    __init__(local_env = None)          # local_env is the Python dict of environment object
    run(ucommand, no_history=False)     # run a command, return the result
    add_to_history(ucommand)            # add a command to the history without running it
    history_prev()                      # return the previous command in the history
    history_next()                      # return the next command in the history

