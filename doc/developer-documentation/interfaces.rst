==================
interface protocol
==================

*Need to be completed...*

::

    # see bristol.interface.dummy.main.DummyInterface
    # or bristol.interface.dummy.main.CursesInterface
    # for implementation examples
    
    # public protocol
    current: the current (focused) widget (can be None)
    current.go_right(step=1, one_page=False): go right in the focused widget
    current.go_left(step=1, one_page=False): go left in the focused widget
    current.go_down(step=1, one_page=False): go down in the focused widget
    current.go_up(step=1, one_page=False): go up in the focused widget
    current.go_home(): go at the start of line
    current.go_end(): go at the end of line
    current.insert_mode: True if insert, False if overwrite
    current.delch(step=1, before=True): delete some characters
    current.toogle_mark(): set or unset the mark
    current.copy(): copy the selection in the clipboard
    current.cut(): cut the selection in the clipboard
    current.paste(): paste the content of the clipboard
    shell_mode(start_text=None): switch to shell mode
    
    # internal protocol
    _editor: the running editor
    _screen_charset: the charset used by the interface to display text
    _main(): end the initialization, enter the main loop
    _exit(): call after editor cleanup, exit the interface and the program
    _exec(cmd): execute a command in the internal shell
    _ask(prompt, default=None, help_msg=None, history_prev=None, history_next=None): generic way to ask something
    _ask_a_filename(prompt, default=None): ask the user for a filename
    _ask_a_question(prompt, choices): ask a question to the user
    _set_status_msg(msg): define the current status message
    
    
