=================================
The Bristoledit startup procedure
=================================

This document explain the different steps of the Bristoledit startup::

    start (from command line in bristol.editor.main())
     |
     |- instanciate the interface
     |  |
     |  |- instanciate the Editor
     |  |  |
     |  | [ intialize the editor]
     |  |  |
     |  |  |- Load the configuration
     |  |  |- Load enabled plugins
     |  |  |    [...]
     |  |
     | [ initialize the interface ]
     |
     \- run the interface main()
        |
        |- run the _std_init() of editor
        |   |
        |   |- open files or create a new one
        |   |
        |   |- Load the hacks file
        |
        \- main loop
    
