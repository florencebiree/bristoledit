========================
bristol.buffer protocole
========================

::

    from bristol.buffer import Buffer
    
    # use help(Buffer) for more information
    
    # public protocol (user interface)
    buffer.reload(encoding=None, endofline=None): reload the current buffer from the disk
    buffer.save(filename=None, encoding=None, endofline=None): save the buffer to the disk
    buffer.save_as(filename=None, encoding=None, endofline=None): save the buffer under another name
    buffer.indent(position=None, delta=0): indent as the previous line
    buffer.switch_auto_indent(): switch the auto indent mode
    buffer.go_to_line(lineno=None): go to the given line number
    buffer.text: the text
    # dynamic per buffer user settingsBuffer.encoding: the encoding of the buffer text                 
    buffer.endofline: the end of line of the buffer text
    buffer.tab_size: the used tab size (in characters)               
    buffer.indent_with_spaces: if indentation must use spaces        
    buffer.auto_indent: if auto_indentation is enabled
    # other public methods
    buffer.indent(): indent the current line
    buffer.normalize_indent(): use the characters for indentation in all the text
    buffer.go_to_line(): go to the given line
    
    # private protocol (developer interface)
    buffer._editor(): weak ref to the running editor
    buffer._config(): weak ref to the buffer config component manager
    # buffer-relative data
    buffer._is_modified: boolean flag set on modification
    buffer._position: current cursor position in the buffer
    buffer._mark_position: current mark position in the buffer, or None
    buffer._filename: file name or None
    # user settings
    buffer._completer: the running autocompleter object for this buffer
    buffer._mimetype: the mime type of the buffer content
    # methods for mime types and use settings
    buffer._detect_mimetype: try to detect the mime type of the buffer
    buffer._init_settings: initialize dynamic per-buffer settings
    # position management
    buffer._line_of_position(): return the line number of a given position
    buffer._index_of_position(): return the index inside the line of a given position
    buffer._start_of_line(): return the starting position of the given line
    # Text content management
    buffer._number_of_line(): return the number of lines
    buffer._get_line(): return the content of a line from its line number
    buffer._set_text(): modify the text in a more precise manner than with buffer.text
    buffer._open(): load the content of the buffer from the disk
    buffer._indent_len(): compute the indent len of a string
    # Interface to delegable tools
    buffer._get_completer(): return (if possible) a suitable completer for the content of the buffer
    
    
