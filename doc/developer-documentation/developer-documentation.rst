=======================
Developer documentation
=======================

* `Coding conventions </bristoledit/developer-documentation/conventions>`__
* `The startup procedure </bristoledit/developer-documentation/startup>`__
* `How to write a plugin </bristoledit/developer-documentation/plugin-how-to>`__ 

Main bristol object protocols
=============================

How to use core foundation of Bristoledit:

* `editor </bristoledit/developer-documentation/editor>`__
* `buffer </bristoledit/developer-documentation/buffer>`__ 

Standard object protocols
=========================

Specifications to be implemented to make such objects:

* `interfaces </bristoledit/developer-documentation/interfaces>`__
* `interpreter </bristoledit/developer-documentation/interpreter>`__
* completer (TODO)
