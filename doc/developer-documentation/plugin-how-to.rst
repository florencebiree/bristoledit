==============
Plugins how to
==============

A lot of functionality of Bristol are provided by plugins.

Locations of plugins
====================

Officials plugins are python files loaded from the ``bristol.plugins`` package.
User plugins are loaded from the ``~/.local/share/bristoledit``.

The name of the plugin is the plugin ID, used to call it in other piece of code
or in the configuration.

The dummy plugin is an example of plugin::

    class DummyPlugin(object):                                                       
        """This is the dummy plugin for Bristol"""                                   
                                                                                     
        def __init__(self, toolkit):                                                 
            """Initialize the plugin"""                                              
            self.toolkit = toolkit                                                   
            # make a hook !                                                          
            @self.toolkit.improve(toolkit.Buffer, 'answer')                          
            def answer(oldself):                                                     
                """Return the answer to the life, the universe and everything"""     
                oldself.set_text(oldself.text + u'\n42')                             
            answer._userdoc_ = _(u"The answer to the life, the universe and "        
                                 u"everything.")
    
    def start_plugin(toolkit):                                                       
        """Start the plugin"""                                                       
        # If the plugin depends on others plugins, use:                              
        #if toolkit.depends_loaded(['plugin1', 'plugin2']):                          
        return DummyPlugin(toolkit)
    
In order to improve existing piece of code, you should uses hooks, as 
described in 
`hooks.py <http://code.filyb.info/bristoledit/trunk/bristol/utils/hooks.py>`__.

