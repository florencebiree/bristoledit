=======================
bristol.editor protocol
=======================

::

    from bristol.editor import Editor
    
    # use help(Editor) for more information
    
    # public protocol
    Editor.interface: the current interface object
    Editor.buffers: the list of opened buffers
    Editor.current: the current buffer
    Editor.config: the configuration object
    Editor.plugins: the plugins manager object
    Editor.new(): create a new buffer
    Editor.open(filename=None, encoding=None, endofline=None): open a file in a new buffer
    Editor.switch_to(target_buffer): set target_buffer as the current buffer
    Editor.next(): switch to the next buffer
    Editor.prev(): switch to the previous buffer
    Editor.close(target_buffer=None, keep_a_buffer_open=True): close the current or the target buffer
    Editor.exit(): exit the program
    
    # internal protocol
    Editor._std_init(): ends the initialization of the editor
    Editor._application: the name of the application, used for config path, localization, etc
    Editor._noplugin: True if plugins should be disabled
    
