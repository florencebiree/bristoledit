==================
Coding conventions
==================

First of all, `The PEP 8 <http://www.python.org/dev/peps/pep-0008/>`__.

Everything here is just Bristoledit-specific adds to the PEP.

Public (for users) method
=========================

In the Python world, a method name without leading underscore (like my_method)
is a public method. In the Bristoledit world, because users has a direct access 
to Python objects, such method will be public for the user. So, if the method 
is intended for Bristoledit developers only, it should begin by an underscore.

To provide a good help to users, all public methods should have
a "user docstring".

Example::

    # i18n stuff, providing the _ method to mark translatable strings
    
    class MyPublicObject(object):
        """Docstring, how to use this object for developers"""
        
        def __init__(self):
            """Docstring, how to use this method for developers"""
            # Here we set the object user doc (ie what this object is for users).
            self._userdoc_ = _("A wonderful object")
            
            def spam(self):
                    """Docstring, for developers"""
                    # Do some stuff. This method is public, and can be invoked either by
                    # users or by developers.
                    pass
            spam._userdoc_ = _("Spamify the wonderful object") # what do this method
            
            def _graal(self):
                    """Docstring, for developers"""
                    # This method will be hidden to users (but there is a story about
                    # consenting adults, and users can always use it if they really want to)
                    # The primary target of this method is developers. So, there is no need
                    # to define a _userdoc_.
                    pass
            
            def __meaning_of_life(self):
                    """Docstring, for developers"""
                    # This method is hidden for everyone outside the class (except for
                    # consenting adults).
                    # Use this convention only if you have a very, very good reason.
                    #
                    # In fact, the only real use of this convention is when you hook an
                    # existing object, and you need to save data inside it.
                    # You can then name your attribute __yourname, to avoid name collisions.
                    pass
    
