========================
Bristoledit key bindings
========================

Bristoledit comes with a number of default key bindings. They are designed to be
almost the same than in any X11 program. Some key bindings are editor's one and
cannot be changed (due to restrictions of terminals). Some others comes from
the Curses interface, some from Bristoledit itself, and the last from plugins.

Key bindings are defined in the ``~/.config/bristoledit/hacks.py`` file, and
can be mostly reconfigured (use ``editor.config.hacks_edit()``.

Default key bindings
====================

	* CTRL + SPACE or CTRL + @: go to the next file
	* CTRL + A: go to the start of the line
	* CTRL + B: enable/disable auto indent (for middle click copy/paste)
	* CTRL + C: copy the current selection
	* CTRL + D: delete the next character (editor shortcut)
	* CTRL + E: go to the end of the line
	* CTRL + F: search with ``grep -n`` (shell plugin)
	* CTRL + G: next result from the search/command (shell plugin)
	* CTRL + H: backspace (editor shortcut)
	* CTRL + I: indent (editor shortcut)
	* CTRL + K: erase the end of line (editor shortcut)
	* CTRL + L: go to the given line
	* CTRL + M: new line (editor shortcut)
	* CTRL + N: new file
	* CTRL + O: open a file
	* CTRL + P: place a mark (start a selection)
	* CTRL + Q: quit the editor
	* CTRL + R: replace with ``sed s///`` (shell plugin)
	* CTRL + S: save the current file
	* CTRL + T: switch to the command mode (with the python ``editor.`` prompt)
	* ALT  + t: switch to the command mode (with the ``!`` shell prompt)
	* CTRL + U: undo (undo plugin)
	* CTRL + V: paste
	* CTRL + W: close the current document
	* CTRL + X: cut the current selection
	* CTRL + Y: switch into the vertical edition mode
	* CTRL + Z: suspend the editor and go back to the shell (use fg to return)
	* F3: search with ``grep -n`` (shell plugin)
	* F5: compile using ``make || ant`` (shell plugin)
	* F6: open the file in a viewer (viewer plugin)
	* F7: search all misspelled words to check them (spell checker plugin)
	* F8: provide spell suggestions for the current word (spell checker plugin)
	* F9: show the definition of the current symbol (symbol collector plugin)
	* F10: go to the definition of the current symbol (symbol collector plugin)
	* F11: go to the current position in the viewer (viewer plugin)

Editing key bindings
====================

Excepting for editor key bindings, all shortcuts can be modified through the
configuration file (use ``editor.config.edit()``), or using the bind method:
``editor.config.bind('editor.myfunction')``.
This will ask you to press the key shortcut you want to bind with
``editor.myfunction`` (take care to quote the function when you call bind).
