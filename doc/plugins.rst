============================
Bristoledit official plugins
============================

A lot of Bristoledit features are brought by plugins. You can enable or disable
plugins in the bristoledit configuration file (``editor.config.edit()``), and
configure some options for theses plugins. There is also some plugins required
by interfaces.

Here is the list of officials (mainly enabled by default) plugins:

addnewline
==========

The addnewline plugin will make sure there is an empty new line at the end of
the file when you save it.

backupcopy
==========

The backupcopy plugin will save a ~ copy of the old file when you save a new
one with the same name.

ctags
=====

This plugin use exuberant ctags to make the list of all symbols (functions,
classes, etc) in opened files.
You can use F9 on a symbol to show its definition in the status bar, or F10 to
go to the definition (and F10 again to get back).

pygmentization
==============

Provides syntax hightlighting using Pygments.

shell
=====

Add ways to interact with your system using shell commands. It make the internal
shell (CTRL-E or CTRL-T) recognize ``! commands``, ``| pipes`` and 
``| backpipes |``, walking around results from theses commands, and add a
search (CTRL-F) command using ``grep -n``, and a compile (F5) command using
``make || ant``.

smartautoindent
===============

Auto-indent on '{' for C-like files, and on ':' for python files.

spellchecker
============

Allow to spell check your text on various language using your installed
spell checker. It can manage file formats like HTML, LaTeX, Troff or po to not
report errors on tags.

statcompleter
=============

Provide autocompletion on words used in the file.

stripwspaces (not enabled by default)
=====================================

Strip whitespaces at the end of lines when saving the file.

tagscompleter (not enabled by default)
======================================

An autocompleter based on symbols collected by the ctags plugin.
You can enable it for some mime types in the Buffer section of the configuration
file.

template
========

Provide templates for current file types.

undo
====

Allow to undo your modification (try CTRL-U).

view
====

Allow to view your file in a viewer (like pdf viewer for latex files).
Support SyncTeX.

wrap
====

Allow to automatically wrap lines to less than 80 characters will typing.
Enabled for LaTeX files by default.
