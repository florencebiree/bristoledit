=======================================
Bristoledit - Installation instructions
=======================================

Dependencies
============

Bristoledit depends on python >= 3.3, and uses the following modules:
  * xdg (<http://www.freedesktop.org/wiki/Software/pyxdg>)
  * Chardet (<http://chardet.feedparser.org/>)
  * Pygments (<http://pygments.org/>)
  * Pyenchant (<http://www.rfk.id.au/software/pyenchant/index.html>)

For the (not currently working) GTK+ interface:
  * PyGTK (<http://pygtk.org>)
  * PyGtkSourceView (<http://pygtk.org/pygtksourceview/>)

Bristoledit can also use the following utilities if they are installed:
  * xclip (<http://sourceforge.net/projects/xclip/>)
  * wl-clipboard (<https://github.com/bugaevc/wl-clipboard>)
  * exuberant ctags (<http://ctags.sourceforge.net/>)

Installation
============

You can install Bristoledit with the following instructions:
    # python setup.py install

Next you can run Bristoledit by:
    $ bristoledit

