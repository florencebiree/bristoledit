============
Bristoledit
============

Bristoledit try to be a full featured text editor, for developers who like easy
and powerful interfaces.
Bristoledit aims to be an editor easy to learn, thanks to an usual key binding
layout and an object command line with complete autocompletion.
Bristoledit respects the UN*X philosophy: it's only a text editor (not an IDE),
but it try to do its job perfectly. It is designed to be used under any modern
UN*X system, in text or graphical mode.

Bristoledit is a libre software, released under the
`GPLv3+ <http://www.gnu.org/licenses/gpl-3.0.html>`__.

Installation:
=============

See the INSTALL file for installation instructions.

If you have the dependencies installed, you can also run Bristoledit without
installing it. Just run:

	$ bin/bristoledit

Quick usage:
============

Run:

	$ bristoledit

This will open a new file for editing. To run some editor command (like save a
file, open another, etc), use the keyboard shortcut Control-T. This open a
little command line.

With the ``editor.`` object, you can control the editor, and with
``editor.current.``, you can control the current file. Trust the autocompletion!

Try also:

	$ bristoledit --help

for more information about command line options.

Because you love your shell
===========================

If you know how to use your UN*X shell, you know how to do a lot of things in
Bristoledit. In the Bristoledit shell (Control-T), remove the `editor.` thing 
(or open the shell with Control-E), and run your command with::

  ! mycommand --parameter

You can also make pipes::

  | wc -l

and get the results of you commands with a "backpipe"::

  | sed -e 's/a/A/g' |

Check the search (Control-F) function: it's just your old plain grep.

Make the Bristoledit you like
=============================

Try also `editor.config.edit()` to modify your configuration. And you should 
feel at home with the default keyboard shortcuts, they are the same than in 
any X11 program (see the 
`key bindings list </doc/keybindings.rst>`__).

There is also `editor.config.hacks_edit()` to define new commands in 
Bristoledit, customize existing behaviour, and everything you can imagine,
thanks to Python.

A lot of functionality of Bristoledit are brings through plugins. See
the `complete list of plugins <doc/plugins.rst>`__.

Bristoledit? Why?
=================

Bristoledit stands for "the BRIght Side Of Life text ediTor", where the T has
walk in the middle of crucified letters.

Why? Because you should "always Look on the Bright Side of Life":
<http://www.youtube.com/watch?v=jHPOzQzk9Qo>

License and authors:
====================

Bristoledit is released under the terms of the GNU GPLv3 or any later version.
Read the COPYING and AUTHORS files.
