# -*- coding: utf-8 -*-
###############################################################################
#       pluginmanager.py
#       
#       Copyright © 2009-2024, Florence Birée <florence@biree.name>
#       
#       This file is a part of bristol
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Plugin manager for Bristoledit

This module provides:
    the PluginToolkit, a class that keep together a lot of useful things for
        plugin writters.
    
    the PluginManager, which provides the logic to load plugin in Bristoledit,
        and the user interface to manage them.
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2024, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import sys
import importlib.util
import importlib.machinery
from xdg.BaseDirectory import load_data_paths

from .buffer import Buffer
from .utils.hooks import hooks, improve

class PluginToolkit(object):
    """Various objects useful for a plugin
    
        A toolkit instance is given to plugins.
    """
    
    def __init__(self, editor, plugin_manager, plugin_id):
        """Initialize the toolkit"""
        self.editor = editor
        self.manager = plugin_manager
        self._plugin_id = plugin_id
        self.config = editor.config._get_component_manager(
            'Plugin' + self._plugin_id.title(),
            _("Configuration for the plugin %s:") % self._plugin_id,
        )
        self.hooks = hooks
        self.improve = improve
        self.Buffer = Buffer
        self.__buff_settings = {}
    
    def depends_loaded(self, list_plugin_id):
        """Declare that the plugin depends on plugins in list_plugin_id
        
        Return if all plugins was successfully loaded.
        """
        for plugin_id in list_plugin_id:
            if not self.manager.load(plugin_id):
                return False
        return True
    
    def make_buffer_property(self, setting, settingname=None, fset=None):
        """Make a per buffer property
        
            You can provide a fset function that will be called when the
            setting will be modified.
            fset(buffer_instance, value, final_fset)
            You must call final_fset(buffer_instance, value) in your fset, to
            effectively change the value.
        """
        if settingname is None:
            settingname = setting
        settingname = settingname.replace(' ', '_')
        # Do it one time for the whole class
        if hasattr(Buffer, settingname):
            return
        def make_property(setting):
            fget = (lambda buff: self.__buff_settings.get(setting,
                                        self.config[buff._mimetype, setting]))
            finalfset = (lambda buff, value :
                        self.__buff_settings.__setitem__(setting, value))
            if fset is None:
                pfset = finalfset
            else:
                pfset = (lambda buff, value:
                            fset(buff, value, finalfset))
            return property(fget, pfset)
        setattr(Buffer, settingname, make_property(setting))
        #TODO: add a way to put a usedoc on properties
        #setattr(getattr(Buffer, settingname), '_userdoc_', 'coin')

class PluginManager(object):
    """Bristoledit plugin manager
    
        Main plugin logic for Bristoledit, provides also the user interface for
        plugins.
    """
    
    def __init__(self, editor):
        """Initialize the plugin manager"""
        self._editor = editor
        self._userdoc_ = _("Plugin manager for Bristoledit.")
        self._loaded = {}
        self._config = self._editor.config._get_component_manager(
            'PluginManager',
            _("Plugins management system:")
        )
        # default: officials plugins
        self._config.set_default({
            'enabled': (['smartautoindent', 'shell', 'pygmentization',
                         'spellchecker', 'ctags', 'undo', 'addnewline',
                         'backupcopy', 'template', 'wrap', 'viewer'],
                       _("List of plugins to be loaded at the startup")),
        })
        # build the list of plugin import paths
        self._path = list(load_data_paths(self._editor._application))
        self._path.append(os.path.abspath(
            os.path.join(os.path.dirname(__file__), 'plugins')
        ))
        # load start-up plugins
        if not self._editor._noplugin:
            for plugin_id in self._config['enabled']:
                self.load(plugin_id)

    __str__ = lambda self: self._userdoc_
    __unicode__ = __str__
    
    def load(self, plugin_id, reload=False):
        """Try to load a plugin, based on its id.
        
        Return if the plugin was successfully loaded.
        """
        if plugin_id in self._loaded and not reload:
            _debug_print("Plugin: %s already loaded" % plugin_id)
            return True
        
        try:
            #(mfile, pname, desc) = imp.find_module(plugin_id, self._path)
            #plugin_specs = importlib.util.find_spec(plugin_id, self._path)
            plugin_specs = importlib.machinery.PathFinder().find_spec(plugin_id, self._path)
        except ImportError:
            _debug_print("Plugin: ImportError when loading %s" % plugin_id)
            return False
        else:
            try:
                #plugin_mod = imp.load_module(plugin_id, mfile, pname, desc)
                plugin_mod = importlib.util.module_from_spec(plugin_specs)
                plugin_specs.loader.exec_module(plugin_mod)

                self._loaded[plugin_id] = plugin_mod.start_plugin(
                    PluginToolkit(self._editor, self, plugin_id)
                )
                _debug_print("Plugin: loading %s" % plugin_id)
                return True
            except ImportError:
                _debug_print("Plugin: ImportError when loading %s" % plugin_id)
                return False
            #finally:
            #    mfile.close()
    load._userdoc_ = _("Load a plugin.")
    
    def __getitem__(self, plugin_id):
        """Return the plugin with plugin_id
        
            plugins.__getitem__(plugin_id) <==> plugins[plugin_id]
        """
        return self._loaded[plugin_id]
    
    def __contains__(self, plugin_id):
        """Return if the plugin_id is loaded
        
            plugins.__contains__(plugin_id) <==> plugin_id in plugins
        """
        return plugin_id in self._loaded
