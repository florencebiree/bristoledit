# -*- coding: utf-8 -*-
###############################################################################
#       tests/test_buffer.py
#       
#       Copyright © 2010-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Unit tests for bristoledit buffers"""

import unittest
import os
import sys

# fix import path
dirname = os.path.dirname(__file__)
if dirname == '':
    dirname = '.'
dirname = os.path.realpath(dirname)
up = lambda adir: os.path.split(adir)[0]
updir = up(up(dirname))
if updir not in sys.path:
    sys.path.append(updir)

from bristol.utils.upy import *

# fix i18n support
if not isinbuiltins('_'):
    setbuiltins('_', lambda t:t)

from bristol.buffer import Buffer, DEFAULT_CONFIG

# tests

class DummyCompMan(object):
    def set_default(self, conf, hacks=None):
        self.conf = conf
    def __getitem__(self, i):
        if isinstance(i, tuple):
            i = i[1]
        v = self.conf.get(i, None)
        if isinstance(v, tuple):
            v = v[0]
        return v

class DummyConfMan(object):
    def _get_component_manager(self, compid, doc=None):
        return DummyCompMan()

class DummyEditor(object):
    def __init__(self):
        self.config = DummyConfMan()

class BufferTest(unittest.TestCase):
    
    def new_buffer(self):
        return Buffer(DummyEditor())
    
    def test_init_settings(self):
        b = self.new_buffer()
        self.assertEquals(b.encoding, DEFAULT_CONFIG['encoding'][0])
        self.assertEquals(b.endofline, DEFAULT_CONFIG['end of line'][0])
        self.assertEquals(b.tab_size, DEFAULT_CONFIG['tab size'][0])
        self.assertEquals(b.indent_with_spaces, DEFAULT_CONFIG['use spaces for tabs'][0])
        self.assertEquals(b.auto_indent, DEFAULT_CONFIG['auto indent'][0])
    
    # text / line tests:
    # instead of testing one method after the other, we test all methods for
    # each kind of operation
    
    def test_full_set_text(self):
        b = self.new_buffer()
        sample = 'Here is a piece of\ntext, with two lines.'
        b.text = sample
        self.assertEquals(b.text, sample)
        self.assertEquals(b._number_of_line(), 2)
        self.assertEquals(b._get_line(0), sample.split('\n')[0] + '\n')
        self.assertEquals(b._get_line(1), sample.split('\n')[1])
        self.assertEquals(b._start_of_line(0), 0)
        self.assertEquals(b._start_of_line(1), 19)
        self.assertEquals(b._index_of_position(3), 3)
        self.assertEquals(b._index_of_position(23), 4)
        self.assertEquals(b._line_of_position(3), 0)
        self.assertEquals(b._line_of_position(23), 1)
    
    def test_insert_single_line(self):
        b = self.new_buffer()
        sample = 'Here is a piece of\ntext, with two lines.'
        b.text = sample
        t = ' cake. And also '
        wanted = 'Here is a piece of cake. And also \ntext, with two lines.'
        b._set_text(t, None, insert_position=18)
        self.assertEquals(b.text, wanted)
        self.assertEquals(b._number_of_line(), 2)
        self.assertEquals(b._get_line(0), wanted.split('\n')[0] + '\n')
        self.assertEquals(b._get_line(1), wanted.split('\n')[1])
        self.assertEquals(b._start_of_line(0), 0)
        self.assertEquals(b._start_of_line(1), 35)
        self.assertEquals(b._index_of_position(3), 3)
        self.assertEquals(b._index_of_position(42), 7)
        self.assertEquals(b._line_of_position(3), 0)
        self.assertEquals(b._line_of_position(42), 1)
    
    def test_insert_new_line(self):
        b = self.new_buffer()
        sample = 'Here is a piece of\ntext, with two lines.'
        b.text = sample
        t = '\n'
        wanted = 'Here is a\n piece of\ntext, with two lines.'
        b._set_text(t, None, insert_position=9)
        self.assertEquals(b.text, wanted)
        self.assertEquals(b._number_of_line(), 3)
        self.assertEquals(b._get_line(0), wanted.split('\n')[0] + '\n')
        self.assertEquals(b._get_line(1), wanted.split('\n')[1] + '\n')
        self.assertEquals(b._get_line(2), wanted.split('\n')[2])
        self.assertEquals(b._start_of_line(0), 0)
        self.assertEquals(b._start_of_line(1), 10)
        self.assertEquals(b._start_of_line(2), 20)
    
    def test_insert_new_line_at_the_end(self):
        b = self.new_buffer()
        sample = 'Here is a piece of\ntext, with two lines.'
        b.text = sample
        t = '\n'
        wanted = 'Here is a piece of\ntext, with two lines.\n'
        b._set_text(t, None, insert_position=40)
        self.assertEquals(b.text, wanted)
        self.assertEquals(b._number_of_line(), 3)
        self.assertEquals(b._get_line(0), wanted.split('\n')[0] + '\n')
        self.assertEquals(b._get_line(1), wanted.split('\n')[1] + '\n')
        self.assertEquals(b._get_line(2), wanted.split('\n')[2])
        self.assertEquals(b._start_of_line(0), 0)
        self.assertEquals(b._start_of_line(1), 19)
        self.assertEquals(b._start_of_line(2), 41)
    
    def test_insert_many_lines(self):
        b = self.new_buffer()
        sample = 'Here is a piece of\ntext, with two lines.\n'
        b.text = sample
        t = 'cake.\nAnd another story:\nIt is about a '
        wanted = 'Here is a piece of\ncake.\nAnd another story:\n'
                   'It is about a text, with two lines.\n'
        b._set_text(t, None, insert_position=19)
        self.assertEquals(b.text, wanted)
        self.assertEquals(b._number_of_line(), 5)
        for i in range(4):
            self.assertEquals(b._get_line(i), wanted.split('\n')[i] + '\n')
        self.assertEquals(b._get_line(4), wanted.split('\n')[4])
        lastlen = 0
        for i in range(b._number_of_line()):
            self.assertEquals(b._start_of_line(i), lastlen)
            lastlen += len(b._get_line(i))
    
    def test_del_inside_line(self):
        b = self.new_buffer()
        sample = 'Here is a piece of\ntext, with two lines.\n'
        b.text = sample
        wanted = 'Here is a\ntext, with two lines.\n'
        b._set_text('', None, 0, delete_position=9, delete_len=9)
        self.assertEquals(b.text, wanted)
        self.assertEquals(b._number_of_line(), 3)
        for i in range(2):
            self.assertEquals(b._get_line(i), wanted.split('\n')[i] + '\n')
        self.assertEquals(b._get_line(2), wanted.split('\n')[2])
        lastlen = 0
        for i in range(b._number_of_line()):
            self.assertEquals(b._start_of_line(i), lastlen)
            lastlen += len(b._get_line(i))
    
    def test_del_new_line(self):
        b = self.new_buffer()
        sample = 'Here is a piece of\ntext, with two lines.\n'
        b.text = sample
        wanted = 'Here is a piece oftext, with two lines.\n'
        b._set_text('', None, 0, delete_position=18, delete_len=1)
        self.assertEquals(b.text, wanted)
        self.assertEquals(b._number_of_line(), 2)
        self.assertEquals(b._get_line(0), wanted.split('\n')[0] + '\n')
        self.assertEquals(b._get_line(1), wanted.split('\n')[1])
        lastlen = 0
        for i in range(b._number_of_line()):
            self.assertEquals(b._start_of_line(i), lastlen)
            lastlen += len(b._get_line(i))
    
    def test_del_many_lines(self):
        b = self.new_buffer()
        sample = 'Here is a\npiece of\ntext, with two lines.\n'
        b.text = sample
        wanted = 'Here is two lines.\n'
        b._set_text('', None, 0, delete_position=8, delete_len=22)
        self.assertEquals(b.text, wanted)
        self.assertEquals(b._number_of_line(), 2)
        self.assertEquals(b._get_line(0), wanted.split('\n')[0] + '\n')
        self.assertEquals(b._get_line(1), wanted.split('\n')[1])
        lastlen = 0
        for i in range(b._number_of_line()):
            self.assertEquals(b._start_of_line(i), lastlen)
            lastlen += len(b._get_line(i))
    
    def test_del_at_last_line(self):
        b = self.new_buffer()
        sample = 'Here is some text.'
        b.text = sample
        wanted = 'is some text.'
        b._set_text('', None, 0, delete_position=0, delete_len=5)
        self.assertEquals(b.text, wanted)
    
    def test_cursor_position(self):
        b = self.new_buffer()
        sample = 'Here is a\npiece of\ntext, with two lines.\n'
        b.text = sample
        wanted = 'Here is two lines.\n'
        b._set_text('', 2, 0)
        self.assertEquals(b._position, 2)
    
    # I/O functions
    
    def test_open(self):
        # TODO: test that...
        pass
    
    def test_reload(self):
        # TODO: test that...
        pass
    
    def test_save(self):
        # TODO: test that...
        pass
    
    def test_save_as(self):
        # TODO: test that...
        pass
    
    def test_indent(self):
        # indent as the last line
        b = self.new_buffer()
        sample = '''
		indent with two tabs
no indent
	    indent with tabs+spaces
no indent
'''
        b.text = sample
        wanted = '''
		indent with two tabs
		no indent
	    indent with tabs+spaces
	    no indent
'''
        b.indent(b._start_of_line(2))
        b.indent(b._start_of_line(4))
        self.assertEquals(b.text, wanted)
        
        # indent one level more than the last line (with Tab)
        b = self.new_buffer()
        b.indent_with_spaces = False
        sample = '''
		indent with two tabs
no indent
	    indent with tabs+spaces
no indent
'''
        b.text = sample
        wanted = '''
		indent with two tabs
			no indent
	    indent with tabs+spaces
	    	no indent
'''
        b.indent(b._start_of_line(2), delta=1)
        b.indent(b._start_of_line(4), delta=1)
        self.assertEquals(b.text, wanted)
        
        # indent one level more than the last line (with spaces)
        b = self.new_buffer()
        b.indent_with_spaces = True
        sample = '''
		indent with two tabs
no indent
	    indent with tabs+spaces
no indent
'''
        b.text = sample
        wanted = '''
		indent with two tabs
		    no indent
	    indent with tabs+spaces
	        no indent
'''
        b.indent(b._start_of_line(2), delta=1)
        b.indent(b._start_of_line(4), delta=1)
        self.assertEquals(b.text, wanted)
        
        # indent one level less than the last line (with tabs)
        b = self.new_buffer()
        b.indent_with_spaces = False
        sample = '''
		indent with two tabs
no indent
	    indent with tabs+spaces
no indent
'''
        b.text = sample
        wanted = '''
		indent with two tabs
	no indent
	    indent with tabs+spaces
	no indent
'''
        b.indent(b._start_of_line(2), delta=-1)
        b.indent(b._start_of_line(4), delta=-1)
        self.assertEquals(b.text, wanted)
        
        # indent one level less than the last line (with spaces)
        b = self.new_buffer()
        b.indent_with_spaces = True
        sample = '''
		indent with two tabs
no indent
	    indent with tabs+spaces
no indent
'''
        b.text = sample
        wanted = '''
		indent with two tabs
    no indent
	    indent with tabs+spaces
    no indent
'''
        b.indent(b._start_of_line(2), delta=-1)
        b.indent(b._start_of_line(4), delta=-1)
        self.assertEquals(b.text, wanted)
        
        # indent one level more than the current line (with spaces)
        b = self.new_buffer()
        b.indent_with_spaces = True
        sample = '		indent with two tabs'
        b.text = sample
        wanted = '		    indent with two tabs'
        b.indent(b._start_of_line(0), ref=0, delta=1)
        self.assertEquals(b.text, wanted)
        # normalize
        wanted = '            indent with two tabs'
        b.indent(b._start_of_line(0), ref=0, delta=0, normalize=True)
        self.assertEquals(b.text, wanted)
        
        # indent one level more than the current line (with tab)
        b = self.new_buffer()
        b.indent_with_spaces = False
        sample = '   	indent with two tabs'
        b.text = sample
        wanted = '   		indent with two tabs'
        b.indent(b._start_of_line(0), ref=0, delta=1)
        self.assertEquals(b.text, wanted)
        # normalize
        wanted = '			indent with two tabs'
        b.indent(b._start_of_line(0), ref=0, delta=0, normalize=True)
        self.assertEquals(b.text, wanted)
        
        # indent one level less than the current line (with spaces)
        b = self.new_buffer()
        b.indent_with_spaces = True
        sample = '		indent with two tabs'
        b.text = sample
        wanted = '    indent with two tabs'
        b.indent(b._start_of_line(0), ref=0, delta=-1)
        self.assertEquals(b.text, wanted)
                
        # indent one level more than the current line (with tab)
        b = self.new_buffer()
        b.indent_with_spaces = False
        sample = '   	indent with two tabs'
        b.text = sample
        wanted = '	indent with two tabs'
        b.indent(b._start_of_line(0), ref=0, delta=-1)
        self.assertEquals(b.text, wanted)
        
    def test_indent_len(self):
        b = self.new_buffer()
        b.tab_size = 4
        self.assertEquals(b._indent_len('    '), 1)
        self.assertEquals(b._indent_len('\t'), 1)
        self.assertEquals(b._indent_len('     '), 2)
    
    def test_normalize_indent(self):
        # with tabs
        b = self.new_buffer()
        b.indent_with_spaces = False
        sample = '''
		indent with two tabs
	one indent with tab
	    indent with tabs+spaces
    	  indent with 2.5 levels
        two indents with spaces
'''
        b.text = sample
        wanted = '''
		indent with two tabs
	one indent with tab
		indent with tabs+spaces
			indent with 2.5 levels
		two indents with spaces
'''
        b.normalize_indent()
        self.assertEquals(b.text, wanted)
        
        # with spaces
        b = self.new_buffer()
        b.indent_with_spaces = True
        sample = '''
		indent with two tabs
	one indent with tab
	    indent with tabs+spaces
    	  indent with 2.5 levels
        two indents with spaces
'''
        b.text = sample
        wanted = '''
        indent with two tabs
    one indent with tab
        indent with tabs+spaces
            indent with 2.5 levels
        two indents with spaces
'''
        b.normalize_indent()
        self.assertEquals(b.text, wanted)
    
    def test_go_to_line(self):
        b = self.new_buffer()
        b.text = '''first line
second line
third line
'''
        b.go_to_line(2)
        self.assertEquals(b._line_of_position(b._position), 1)
        b.go_to_line(0)
        self.assertEquals(b._line_of_position(b._position), 0)
        b.go_to_line(-1)
        self.assertEquals(b._line_of_position(b._position), 3)
        b.go_to_line(1)
        self.assertEquals(b._line_of_position(b._position), 0)
        
    # TODO (later): _get_completer, _get_attribute_map

# test suite
def test_suite():
    tests = [ unittest.makeSuite(BufferTest) ]
    return unittest.TestSuite(tests)

if __name__ == '__main__':
    unittest.main(defaultTest='test_suite')
