# -*- coding: utf-8 -*-
###############################################################################
#       tests/test_config.py
#       
#       Copyright © 2010-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Unit tests for bristoledit config module"""

import unittest
import os
import sys

# fix import path
dirname = os.path.dirname(__file__)
if dirname == '':
    dirname = '.'
dirname = os.path.realpath(dirname)
up = lambda adir: os.path.split(adir)[0]
updir = up(up(dirname))
if updir not in sys.path:
    sys.path.append(updir)

from bristol.utils.upy import *

# fix i18n support
if not isinbuiltins('_'):
    setbuiltins('_', lambda t:t)

# tests

from bristol.config import ComponentManager, ConfigurationManager 

class CompManTest(unittest.TestCase):
    
    def __init__(self, coin):
        unittest.TestCase.__init__(self, coin)
        class Module(object):
            pass
        class DummyConfigMan(object):
            def __init__(self):
                self._module = Module()
            def _flush(self, compman):
                compdict = {}
                setattr(self._module, compman.component_id, compdict)
                for setting in compman.default:
                    if isinstance(compman.default[setting], tuple):
                        value = compman.default[setting][0]
                    else:
                        value = compman.default[setting]
                    compdict[setting] = value
        self.config = DummyConfigMan()
        self.compid = 0
    
    def new_comp_man(self, cid=None):
        if cid is None:
            cid = 'testID' + str(self.compid)
            self.compid += 1
        c = ComponentManager(cid, self.config, 'testdoc')
        return c
    
    def test_set_default(self):
        c = self.new_comp_man('testset')
        c.set_default({'spam': 42})
        self.assertEquals(self.config._module.testset['spam'], 42)
        c.set_default({'spam': 43})
        self.assertEquals(self.config._module.testset['spam'], 42)
    
    def test_hasdict(self):
        c = self.new_comp_man()
        self.assertEquals(c._hasdict(), False)
        c.set_default({'spam': 42})
        self.assertEquals(c._hasdict(), True)
    
    def test_getdict(self):
        c = self.new_comp_man()
        c.set_default({'spam': 42})
        self.assertEquals(c._getdict()['spam'], 42)
    
    def test_getitem(self):
        c = self.new_comp_man()
        c.set_default({
            'spam': 42,
            'eggs': {
                'spam': 43,
            },
            'sgge': {},
        })
        self.assertEquals(c['spam'], 42)
        self.assertEquals(c['eggs', 'spam'], 43)
        self.assertEquals(c['sgge', 'spam'], 42)
    
    def test_getdoc(self):
        c = self.new_comp_man()
        c.set_default({
            'spam': (42, 'Some spam'),
            'eggs': 24,
        })
        self.assertEquals(c.getdoc('spam'), 'Some spam')
        self.assertEquals(c.getdoc('eggs'), '')
    
    def test_unicode(self):
        # TODO: test the Python code generation...
        pass

class ConfigManTest(unittest.TestCase):
    # TODO
    pass
            
# test suite
def test_suite():
    tests = [unittest.makeSuite(CompManTest), unittest.makeSuite(ConfigManTest)]
    return unittest.TestSuite(tests)

if __name__ == '__main__':
    unittest.main(defaultTest='test_suite')
