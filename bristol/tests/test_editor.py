# -*- coding: utf-8 -*-
###############################################################################
#       tests/test_editor.py
#       
#       Copyright © 2010-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Unit tests for bristoledit editor"""

import unittest
import os
import sys

# fix import path
dirname = os.path.dirname(__file__)
if dirname == '':
    dirname = '.'
dirname = os.path.realpath(dirname)
up = lambda adir: os.path.split(adir)[0]
updir = up(up(dirname))
if updir not in sys.path:
    sys.path.append(updir)

from bristol.utils.upy import *

# tests

from bristol.editor import Editor
from bristol.interfaces.dummy.main import DummyInterface

class EditorTest(unittest.TestCase):
    
    def init_editor(self, full=True):
        i = DummyInterface(Editor, None)
        e = i._editor
        if full:
            e._std_init()
        return e
    
    def test_init(self):
        e = self.init_editor(full=False)
        # what to assert?
    
    def test_std_init(self):
        e = self.init_editor(full=False)
        # open files or set a new buffer, then load hacks 
        # no filenames
        e._std_init(None)
        self.assertEquals(len(e.buffers), 1)
        del e
        
        e = self.init_editor(full=False)
        # various dummy filenames
        fnames = ( '_dummy42_' + str(i) for i in range(3) )
        e._std_init(fnames)
        self.assertEquals(len(e.buffers), 3)
    
    def test_new(self):
        e = self.init_editor()
        oldnbbuffer = len(e.buffers)
        b = e.new()
        self.assertEquals(len(e.buffers), oldnbbuffer + 1)
    
    def test_open(self):
        e = self.init_editor()
        oldnbbuffer = len(e.buffers)
        b = e.open('dummyname')
        self.assertEquals(len(e.buffers), oldnbbuffer + 1)
    
    def test_switch_to(self):
        e = self.init_editor()
        b1 = e.current
        b2 = e.new()
        self.assertEquals(e.current, b1)
        # test switch_to(buffer_instance)
        self.assertEquals(e.switch_to(b2), b2)
        self.assertEquals(e.current, b2)
        # test switch_to(index)
        self.assertEquals(e.switch_to(0), b1)
        self.assertEquals(e.current, b1)
    
    def test_next(self):
        e = self.init_editor()
        b1 = e.current
        b2 = e.new()
        b3 = e.new()
        self.assertEquals(e.current, b1)
        
        self.assertEquals(e.next(), b2)
        self.assertEquals(e.current, b2)
        
        self.assertEquals(e.next(), b3)
        self.assertEquals(e.current, b3)
        
        self.assertEquals(e.next(), b1)
        self.assertEquals(e.current, b1)
    
    def test_prev(self):
        e = self.init_editor()
        b1 = e.current
        b2 = e.new()
        b3 = e.new()
        self.assertEquals(e.current, b1)
        
        self.assertEquals(e.prev(), b3)
        self.assertEquals(e.current, b3)
        
        self.assertEquals(e.prev(), b2)
        self.assertEquals(e.current, b2)
        
        self.assertEquals(e.prev(), b1)
        self.assertEquals(e.current, b1)
    
    def test_next_prev(self):
        e = self.init_editor()
        b1 = e.current
        b2 = e.new()
        self.assertEquals(e.current, b1)
        
        self.assertEquals(e.next(), b2)
        self.assertEquals(e.current, b2)
        
        self.assertEquals(e.prev(), b1)
        self.assertEquals(e.current, b1)
    
    def test_close(self):
        e = self.init_editor()
        b0 = e.current
        b1 = e.new()
        # close a targeted non-current buffer
        e.close(b1)
        self.assertEquals(len(e.buffers), 1)
        del b1
        # close the current buffer, check the switch to the next one
        b1 = e.new()
        e.close()
        self.assertEquals(len(e.buffers), 1)
        self.assertEquals(e.current, b1)
        # close the buffer, don't keep one open
        e.close(keep_a_buffer_open=False)
        self.assertEquals(len(e.buffers), 0)
        self.assertEquals(e.current, None)
    
    def test_exit(self):
        # the actual dummy interface will call sys.exit, we don't want that in
        # tests
        pass
        
# test suite
def test_suite():
    tests = [ unittest.makeSuite(EditorTest) ]
    return unittest.TestSuite(tests)

if __name__ == '__main__':
    unittest.main(defaultTest='test_suite')
