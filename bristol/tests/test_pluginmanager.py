# -*- coding: utf-8 -*-
###############################################################################
#       tests/test_pluginmanager.py
#       
#       Copyright © 2010-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Unit tests for bristoledit plugin manager module"""

import unittest
import os
import sys

# fix import path
dirname = os.path.dirname(__file__)
if dirname == '':
    dirname = '.'
dirname = os.path.realpath(dirname)
up = lambda adir: os.path.split(adir)[0]
updir = up(up(dirname))
if updir not in sys.path:
    sys.path.append(updir)

from bristol.utils.upy import *

# fix i18n support
if not isinbuiltins('_'):
    setbuiltins('_', lambda t:t)

# tests

from bristol.pluginmanager import PluginToolkit, PluginManager

class PlugManTest(unittest.TestCase):
    # TODO
    pass
            
# test suite
def test_suite():
    tests = [unittest.makeSuite(PlugManTest)]
    return unittest.TestSuite(tests)

if __name__ == '__main__':
    unittest.main(defaultTest='test_suite')
