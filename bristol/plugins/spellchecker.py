# -*- coding: utf-8 -*-
###############################################################################
#       plugins/spellchecker.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Official libEnchant spell checker plugin for Bristoledit."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import re
import enchant
from enchant.checker import SpellChecker
from enchant.tokenize import EmailFilter, URLFilter, Filter, tokenize
from enchant.tokenize import wrap_tokenizer, get_tokenizer

from bristol.utils.highlighting import AttributeMap
from bristol.utils.completion import Suggestion, get_current_token

class latex_tokenize(tokenize):
    """Tokenizer that splits LaTeX documents into checkable chunks.
    
    This tokenizer removes comments, commands etc and yields chunks
    of checkable text from a LaTeX document.
    """
    _comment = re.compile(r"%.*$", re.M)
    _block_tag = re.compile(r"""
        \\            # Start of the tag
        (?:           # either
            begin|    #    begin
            end|      #    end
            use.*     #    use.* (for packages, themes)
        )
        \{[^}]*\}     # tag name
    """, re.VERBOSE)
    _normal_tag = re.compile(r"""
        \\                # Start of the tag
        .*?               # tag name
        (?:\[.*?\]|\s|\{) # either a space, a [ or a { to close the tag
    """, re.VERBOSE)
    _patterns = [_comment, _block_tag, _normal_tag]
    
    def __init__(self, text):
        """Initialize the tokenizer"""
        tokenize.__init__(self, text)
        # build the list of LaTeX chunks
        self.latex_tag = []
        for pattern in self._patterns:
            res = pattern.finditer(text.tounicode())
            if res:
                for match in res:
                    self.latex_tag.append((match.start(), match.end()))
        self.latex_tag.sort()
            
    def next(self):
        """Return the next checkable chunk of text"""
        text = self._text
        offset = self.offset
        for i in range(len(self.latex_tag)):
            spos, epos = self.latex_tag[i]
            if offset < spos:
                self.offset = spos
                return (text[offset:spos], offset)
            elif spos <= offset < epos:
                offset = epos
                self.offset = offset
        if offset < len(text):
            self.offset = len(text)
            return (text[offset:], offset)
        else:
            raise StopIteration()

class troff_tokenize(tokenize):
    """Tokenizer that splits troff/man documents into checkable chunks.
    
    This tokenizer removes comments and macros and yields chunks
    of checkable text from a troff/man document.
    """
    _comment = re.compile(r'.\".*$', re.M)
    _macro = re.compile(r"""
        ^                 # Start of the line
        \.                # Start of the macro
        [A-Z]+            # macro name
        \s+               # some spaces
    """, re.VERBOSE | re.M)
    _patterns = [_comment, _macro]
    
    def __init__(self, text):
        """Initialize the tokenizer"""
        tokenize.__init__(self, text)
        # build the list of troff chunks
        self.troff_tag = []
        for pattern in self._patterns:
            res = pattern.finditer(text.tounicode())
            if res:
                for match in res:
                    self.troff_tag.append((match.start(), match.end()))
        self.troff_tag.sort()
            
    def next(self):
        """Return the next checkable chunk of text"""
        text = self._text
        offset = self.offset
        for i in range(len(self.troff_tag)):
            spos, epos = self.troff_tag[i]
            if offset < spos:
                self.offset = spos
                return (text[offset:spos], offset)
            elif spos <= offset < epos:
                offset = epos
                self.offset = offset
        if offset < len(text):
            self.offset = len(text)
            return (text[offset:], offset)
        else:
            raise StopIteration()

class html_tokenize(tokenize):
    """Tokenizer that splits (x)html documents into checkable chunks.
    
    This tokenizer removes comments, tags, etc and yields chunks
    of checkable text from a (x)html document.
    """
    # works for tags, dtd, xml header
    _tag = re.compile(r"<.*?>", re.S)
    _comment = re.compile(r"<!--.*?-->", re.S)
    _entity = re.compile(r"&[a-z]*;", re.S)
    _patterns = [_comment, _tag, _entity]
    
    def __init__(self, text):
        """Initialize the tokenizer"""
        tokenize.__init__(self, text)
        # build the list of html tags
        self.html_tag = []
        for pattern in self._patterns:
            res = pattern.finditer(text.tounicode())
            if res:
                for match in res:
                    self.html_tag.append((match.start(), match.end()))
        self.html_tag.sort()
            
    def next(self):
        """Return the next checkable chunk of text"""
        text = self._text
        offset = self.offset
        for i in range(len(self.html_tag)):
            spos, epos = self.html_tag[i]
            if offset < spos:
                self.offset = spos
                return (text[offset:spos], offset)
            elif spos <= offset < epos:
                offset = epos
                self.offset = offset
        if offset < len(text):
            self.offset = len(text)
            return (text[offset:], offset)
        else:
            raise StopIteration()

class po_tokenize(tokenize):
    """Tokenizer that splits po gettext documents into checkable chunks.
    
    This tokenizer removes comments and msgid text and yields chunks
    of checkable text from a po document.
    """
    _comment = re.compile(r'#.*$', re.M)
    _header = re.compile(r"""
        msgid\s+""\s*         # Empty msgid
        msgstr\s+""\s*        # Start of the msgstr header (first line)
        (?:\s*"[^\n]*"\s*)*   # Other lines
    """, re.VERBOSE)
    _msgid = re.compile(r"""
        msgid\s+"[^\n]*"\s*     # Start of the msgid (first line)
        (?:\s*"[^\n]*"\s*)*     # Other lines
    """, re.VERBOSE)
    _msgstr = re.compile(r"msgstr")
    _patterns = [_comment, _header, _msgid, _msgstr]
    
    def __init__(self, text):
        """Initialize the tokenizer"""
        tokenize.__init__(self, text)
        # build the list of po chunks
        self.po_tag = []
        for pattern in self._patterns:
            res = pattern.finditer(text.tounicode())
            if res:
                for match in res:
                    self.po_tag.append((match.start(), match.end()))
        self.po_tag.sort()
            
    def next(self):
        """Return the next checkable chunk of text"""
        text = self._text
        offset = self.offset
        for i in range(len(self.po_tag)):
            spos, epos = self.po_tag[i]
            if offset < spos:
                self.offset = spos
                return (text[offset:spos], offset)
            elif spos <= offset < epos:
                offset = epos
                self.offset = offset
        if offset < len(text):
            self.offset = len(text)
            return (text[offset:], offset)
        else:
            raise StopIteration()

class AbortCheck(Exception):
    """Raised when the user wants to abort a full check."""
    pass

class SpellCheckerCompleter(object):
    """Provide spelling suggestions"""
    
    def __init__(self, suggestion_list):
        """Initialize the completer"""
        self.suggestions = suggestion_list
    
    def suggest(self, buffer_src):
        """Provide spelling suggestions"""
        token = get_current_token(buffer_src.text, buffer_src._position)
        return [
            Suggestion(ulabel, umissing_chars=ulabel[len(token):])
            for ulabel in self.suggestions
            if ulabel.startswith(token)
        ]

class SpellCheckerInterface(object):
    """Provie an interface to the spell checker"""
    
    def __init__(self, editor, config, get_checker):
        """Initialize the interface"""
        self._editor = editor
        self._config = config
        self._get_checker = get_checker
        self._userdoc_ = _("Interface for the spell checker.")
    
    #def language(self, language):
    #    """Change the spell checker language for the current buffer"""
    #    setattr(self._editor.current, '__spell_check_language', language)
    #    # TODO: dirty forced refresh
    #    self._editor.current._set_text(self._editor.current.text)
    #language._userdoc_ = _("Set the language of the text.")
    
    def suggest(self, position=None):
        """Provide spell check suggestions about the current word
        
        If position is not None, provide suggestions about the word at this
        position.
        """
        if position is None:
            position = self._editor.current._position
        chkr = self._get_checker(self._editor.current)
        lineno = self._editor.current._line_of_position(position)
        start_of_line = self._editor.current._start_of_line(lineno)
        line_content = self._editor.current._get_line(lineno)
        chkr.set_text(line_content)
        error = None
        for err in chkr:
            if (err.wordpos <= (position - start_of_line) < (err.wordpos +
                                                                len(err.word))):
                # find the error
                error = err
                break
        if error is None:
            message = _("This word has a good spelling.")
        else:
            try:
                message = self._suggest(
                    error.word,
                    error.wordpos,
                    error.dict,
                    start_of_line,
                    error,
                )
            except AbortCheck:
                message = _("Check aborted.")
        return message
    suggest._userdoc_ = _("Provide spelling suggestions for this word.")
            
    def _suggest(self, word, wordpos, edict, ref, error, nxt=False):
        """Provide suggestions with an enchant error, and a start text ref."""
        suggestions = (
            [_("add"), _("ignore")] +
            ([_("next")] if nxt else []) +
            edict.suggest(word)
        )
        #self._editor.plugins.load('askingsomething')
        prompt = _("Correction for %s: ") % word
        result = self._editor.interface._ask(
            prompt,
            completer = SpellCheckerCompleter(suggestions)
        )
        if result == _("add"):
            #edict.add(word)
            error.add(word)
            message = _("Word %s added to the dictionnary.") % word
            # TODO: dirty forced refresh
            self._editor.current._set_text(self._editor.current.text)
        elif result == _("ignore"):
            error.ignore_always(word)
            message = _("Misspelled word ingnored.")
            # TODO: dirty forced refresh
            self._editor.current._set_text(self._editor.current.text)
        elif result == _("next"):
            # next? should ignore a word just one time
            pass
        elif not result:
            raise AbortCheck
        else:
            self._editor.current._set_text(
                result, None,
                ref + wordpos,
                ref + wordpos,
                len(word)
            )
            message = _("Word replaced by %s.") % result
        return message
    
    def check_all(self):
        """Check all misspelled words of the text"""
        message = _("All misspelled words checked.")
        chkr = self._get_checker(self._editor.current)
        chkr.set_text(self._editor.current.text)
        error_list = []
        for error in chkr:
            (word, wordpos) = (error.word, error.wordpos)
            # move the cursor to the word
            self._editor.current._set_text('', wordpos, wordpos)
            try:
                self._suggest(word, wordpos, chkr.dict, 0, error)
            except AbortCheck:
                message = _("Check aborted.")
                break
            chkr.set_text(self._editor.current.text)
        return message
    check_all._userdoc_ = _("Check all misspelled words of this text.")
    
    #def switch_online_check(self):
    #    """Enable or disable the online check"""
    #    if not hasattr(self._editor.current, '__spell_check'):
    #        cur_status = self._config[self._editor.current._mimetype, 'enabled']
    #    else:
    #        cur_status = getattr(self._editor.current, '__spell_check')
    #    setattr(self._editor.current, '__spell_check', not cur_status)
    #    # TODO: dirty forced refresh
    #    self._editor.current._set_text(self._editor.current.text)
    #switch_online_check._userdoc_ = _("Enable or disable the online check.")

class EnchantPlugin(object):
    """Enchant plugin to provide a spell checker for bristol"""
    
    filters = {
        'email': EmailFilter,
        'url': URLFilter,
    }
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
        # register default settings
        default_config = {
            'enabled': (False, _("Define if the online check is enabled.")),
            'filters': (['email', 'url'],
                _("Avoid checking some text (email, url, latex, html).")),
            'text/x-tex': {
                'enabled': True,
                'filters': ['email', 'url', 'latex'],
            },
            'text/html': {
                'enabled': True,
                'filters': ['email', 'html'],
            },
            'text/x-rst': {
                'enabled': True,
                'filters': ['email', 'url'],
            },
            'application/x-troff': {
                'enabled': True,
                'filters': ['email', 'url', 'troff'],
            },
            'application/x-gettext': {
                'enabled': True,
                'filters': ['email', 'url', 'po'],
            },
        }
        try:
            language = enchant.Dict().tag
        except enchant.Error:
            language = None
        default_config['language'] = (language,
                                        _("Spell check language."))
        self.toolkit.config.set_default(default_config, [
            "editor.config.key['KEY_F(7)'] = "
                "'editor.spellchecker.check_all()'",
            "editor.config.key['KEY_F(8)'] = "
                "'editor.spellchecker.suggest()'",
        ])
        # set interface hooks
        type(toolkit.editor).spellchecker = SpellCheckerInterface(
            self.toolkit.editor,
            self.toolkit.config,
            self.get_checker,
        )
        # register buffer settings
        def full_refresh(buff, value, finalfset):
            """Force a full refresh of the buffer after a buffer property
                change
                
            FIXME: dirty forced refresh
            """
            finalfset(buff, value)
            buff._set_text(buff.text)
        
        toolkit.make_buffer_property('enabled', 'spellchecker_enabled',
                                     full_refresh)
        toolkit.make_buffer_property('language', fset=full_refresh)
        
        # set internal hooks
        @self.toolkit.hooks(toolkit.Buffer, '_get_attribute_map', self)
        def _get_attribute_map(oldself, old_get_attribute_map,
                                        min_position=None, max_position=None):
            attr_map = old_get_attribute_map(oldself, min_position,
                                             max_position)
            # if enabled, perform an spell check on those data
            enabled = oldself.spellchecker_enabled
            if enabled:
                chkr = self.get_checker(oldself)
                if chkr is None:
                    return attr_map
                # get bad words
                chkr.set_text(oldself.text[min_position:max_position])
                for err in chkr:
                    # hijack the attribute map
                    attr_map[err.wordpos:err.wordpos+len(err.word)] = \
                                                'code_style.generic.badword'
            return attr_map
    
    def get_checker(self, abuffer):
        """Return the right configured spell checker."""
        language = abuffer.language
        if not enchant.dict_exists(language):
            return None
        str_filters = self.toolkit.config[abuffer._mimetype, 'filters']
        cls_filters = [self.filters[name] for name in str_filters
                                        if name in self.filters]
        use_latex_tknz = ('latex' in str_filters)
        use_html_tknz = ('html' in str_filters)
        use_troff_tknz = ('troff' in str_filters)
        use_po_tknz = ('po' in str_filters)
        # get the spell checker
        if ((not hasattr(abuffer, '__spell_checker')) or
                        (getattr(abuffer, '__spell_checker').lang != language)):
            if use_latex_tknz:
                # Combine latex_tokenize with standard tokenizer
                tknz = wrap_tokenizer(latex_tokenize,
                                      get_tokenizer("en", filters=cls_filters))
            elif use_html_tknz:
                # Combine html_tokenize with standard tokenizer
                tknz = wrap_tokenizer(html_tokenize,
                                      get_tokenizer("en", filters=cls_filters))
            elif use_troff_tknz:
                # Combine troff_tokenize with standard tokenizer
                tknz = wrap_tokenizer(troff_tokenize,
                                      get_tokenizer("en", filters=cls_filters))
            elif use_po_tknz:
                # Combine po_tokenize with standard tokenizer
                tknz = wrap_tokenizer(po_tokenize,
                                      get_tokenizer("en", filters=cls_filters))
            else:
                tknz = get_tokenizer("en", filters=cls_filters)
            chkr = SpellChecker(language, tokenize=tknz)
            setattr(abuffer, '__spell_checker', chkr)
        else:
            chkr = getattr(abuffer, '__spell_checker')
        return chkr

def start_plugin(toolkit):
    """Start the plugin"""
    return EnchantPlugin(toolkit)
