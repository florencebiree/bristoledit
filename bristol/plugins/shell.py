# -*- coding: utf-8 -*-
###############################################################################
#       plugins/shell.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""A simple way to acces to shell commands, pipes, etc.

This plugin provide three modes to interact with the shell:
    command mode (editor.exec('cmd') or ! cmd) : run the command, show the
        relevant result / or return status.
    pipe mode (editor.current.pipe('cmd') or | cmd) : run the command with the
        content of the current file in stdin.
    backpipe mode (editor.current.backpipe('cmd') or | cmd |) : run the command
        with the content of the current file in stdin, get the result from
        stdout and replace the current buffer with it.
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2010-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import sys
import os
import re
import subprocess

class ShellPlugin(object):
    """Allow to acces to shell commands"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        self._asyncrun = toolkit.manager._loaded['asyncrun'].run
        self.results = None
        self.current = -1
        self.last_command = None
        self.running = []
        
        self.toolkit.config.set_default({}, [
            "editor.config.key['M-t'] = 'editor.command()'", 
            "define[editor.current].search = "
                                        "'editor.current.pipe(\"grep -n \\\'\", "
                                        "\"\\\'\")'",
            "define[editor.current].replace = 'editor.current.backpipe("
                                        "\"sed \\\'s/\", \"//g\\\'\")'",
            "define[editor].compile = '! make || ant &'",
            "editor.config.key['^F'] = 'editor.current.search()'",
            "editor.config.key['^R'] = 'editor.current.replace()'",
            "editor.config.key['KEY_F(3)'] = 'editor.current.next_result()'",
            "editor.config.key['^G'] = 'editor.current.next_result()'",
            "editor.config.key['KEY_F(5)'] = "
                                "'editor.current.save() ; editor.compile()'",
        ])
        
        self.rex = re.compile(r'''
            ^(?:             # no filename
                |            # or
             ([^:]+):        # a filename
            )
            (\d*):           # a line number
            (.*)             # and a message
            ''', re.X | re.M
        )
        
        # Define hooks
        @self.toolkit.improve(type(toolkit.editor), 'command')
        def command(oldself, command=None, after=None):
            """Run a shell command"""
            return self.run_shell_command(command, after=after)
        command._userdoc_ = _("Execute a shell command.")
        
        @self.toolkit.improve(toolkit.Buffer, 'pipe')
        def pipe(oldself, command=None, after=None):
            """Run a shell command with the buffer as stdin"""
            return self.run_shell_command(command, oldself, after=after)
        pipe._userdoc_ = _("Execute a command with the buffer as stdin.")
        
        @self.toolkit.improve(toolkit.Buffer, 'backpipe')
        def backpipe(oldself, command=None, after=None):
            """Run a command and replace this buffer with the result."""
            return self.run_shell_command(command, oldself, True, after=after)
        backpipe._userdoc_ = _("Run a command and get back the result.")
        
        @self.toolkit.improve(toolkit.Buffer, 'next_result')
        def next_result(oldself):
            """Go to the next search result"""
            if not self.results:
                return
            self.current = (self.current + 1) % len(self.results)
            afile, lineno, msg = self.results[self.current]
            afile = afile.strip()
            if afile and afile != self.toolkit.editor.current._filename:
                # try to find the right file
                buffer_found = False
                for buff in toolkit.editor.buffers:
                    if afile == buff._filename:
                        buffer_found = True
                        toolkit.editor.switch_to(buff)
                if not buffer_found:
                    #return _("File %s not open.") % afile.encode(
                    #    self.toolkit.editor.interface.screen_charset)
                    #)
                    # display the line instead of an error
                    return ':'.join(str(r) for r in self.results[self.current])
            # go to the right line
            self.toolkit.editor.current.go_to_line(lineno)
            # display the message
            return (
                afile +
                (':(%d/%d):' % (self.current + 1, len(self.results))) +
                msg
            )
        next_result._userdoc_ = _("Go to the next search result.")
        
        @self.toolkit.hooks(type(self.toolkit.editor), '_check_events', self)
        def _check_events(oldself, old_check_events):
            ret = old_check_events(oldself)
            stillrunning = []
            for process in self.running:
                retcode = process.is_terminated()
                if retcode is not None:
                    # the process is dead, try to get some output
                    content = ""
                    while True:
                        try:
                            content += process.readline().decode(
                                self.toolkit.editor.interface._screen_charset,
                                'replace'
                            )
                        except process.Empty:
                            break
                    ret = self.parse_output(content, retcode, process.cmd+"&")
                else:
                    stillrunning.append(process)
            self.running = stillrunning
            return ret
                
        # hook the python interpreter
        pyshclass = self.toolkit.manager['pythonshell']
        @self.toolkit.hooks(pyshclass, 'run', self)
        def run(oldself, oldrun, ucommand, no_history=False):
            """Intercept shell commands in the editor shell"""
            try:
                stripped_cmd = ucommand.strip()
            except AttributeError:
                stripped_cmd = None
            if not stripped_cmd or stripped_cmd[0] not in '!|':
                # Normal python command
                return oldrun(oldself, ucommand, no_history)
            else:
                # This is a shell command!
                # Run the oldrun just because is good
                oldrun(oldself, ucommand, True)
                # Run the shell command
                cur_buffer = None
                backpipe = False
                retval = self.run_shell_command(
                    stripped_cmd[1:], cur_buffer, backpipe, reprompt=False
                )
                if retval:
                    return retval
                else:
                    return ""
    
    def run_shell_command(self, command=None, cur_buffer=None, backpipe=False,
                          reprompt=True, after=None, background=False):
        """Run an external command
            if cur_buffer is not None, a pipe is made to feed the command
            with the content of the buffer in stdin
            if backpipe is True, the result of the commande is kept to set
            the content of the buffer
            if reprompt, a new prompt is open to edit the command
                if after, after is the string to be displayed after the cursor
            if background, the new process will go to the background
        """
        if cur_buffer:
            ind = '|'
        else:
            ind = '!'
        if not command:
            command = ""
        if not after:
            after = u''
        if backpipe:
            after += ' |'
        # get the command if not already (or complete it)
        if not command and self.last_command:
            prompt = '>>> [%s] ' % (self.last_command[0])
        else:
            prompt = '>>> '
        command = '%s %s' % (ind, command)
        if reprompt:
            command = self.toolkit.editor.interface._ask(
                prompt=prompt,
                default=command,
                help_msg=_('Leave empty to re-run the last one.'),
                afterchars=after,
            )
            if not command:
                command = ""
        # remove the prompt
        command = command.strip()
        if command.startswith('!'):
            command = command[1:]
            cur_buffer = None
        elif command.startswith('|'):
            if cur_buffer is None:
                cur_buffer = self.toolkit.editor.current
            command = command[1:]
        if command.endswith('|'):
            command = command[:-1]
            backpipe = True
            background = False
        elif command.endswith('&'):
            command = command[:-1]
            background = True
            backpipe = False
        else:
            backpipe = False
            background = False
        command = command.strip()
        # run again the old command
        if reprompt and not command and self.last_command:
            return self.run_shell_command(
                self.last_command[0],
                self.last_command[1],
                self.last_command[2],
                reprompt=False,
            )
        elif not command:
            return
        elif not reprompt:
            command += after
        # save as last_command
        self.last_command = (command, cur_buffer, backpipe)
        self.results = None
        self.current = -1
        # if in background:
        if background:
            process = self._asyncrun(command, shell=True)
            self.running.append(process)
            return _("Command started in background")
        #Open the new process
        process = subprocess.Popen(
            command, shell=True,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        # if in pipe mode, feed with the current file
        if cur_buffer:
            cur_data = cur_buffer.text.encode(cur_buffer.encoding)
        else:
            cur_data = None
        
        # get the output
        std_output, err_output = process.communicate(cur_data)
        std_output = std_output.decode(
                                self.toolkit.editor.interface._screen_charset,
                                'replace')
        err_output = err_output.decode(
                                self.toolkit.editor.interface._screen_charset,
                                'replace')
        
        # if in backpipe mode, and command OK, set the content of the buffer
        if process.returncode == 0 and backpipe:
            cur_buffer._set_text(std_output)
            return "| %s | -> Ok" % command
        # else try parse the output
        return self.parse_output(
            std_output + err_output,
            process.returncode,
            command = ind + " " + command
        )
        
    def parse_output(self, content, returncode, command):
        """Parse the (decoded) output of a command"""
        self.results = [
            (
                f,
                int(lineno),
                msg
            ) for (f, lineno, msg)
            in self.rex.findall(content)
            if lineno.isdigit()
        ]
        if self.results:
            # go to the first result
            return self.toolkit.editor.current.next_result()
        # else just print the text
        if content:
            msg = content
        elif returncode == 0:
            msg = "%s -> Ok" % command
        else:
            msg = (_("Error: %(cmd)s failed with the error #%(retcode)d.") %
                    {'cmd': command, 'retcode': returncode})
        return msg.replace('\n', ' ')
        
def start_plugin(toolkit):
    """Start the plugin"""
    if toolkit.depends_loaded(['pythonshell', 'asyncrun']):
        return ShellPlugin(toolkit)
