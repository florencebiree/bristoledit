# -*- coding: utf-8 -*-
###############################################################################
#       plugins/dummy.py
#       
#       Copyright © 2009-2010, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Official dummy plugin for Bristoledit."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

class DummyPlugin(object):
    """This is the dummy plugin for Bristoledit"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        # make a hook !
        @self.toolkit.improve(toolkit.Buffer, 'answer')
        def answer(oldself):
            """Return the answer to the life, the universe and everything"""
            oldself._set_text(
                new_text= '42',
                insert_position = oldself._position,
            )
        answer._userdoc_ = _("The answer to the life, the universe and "
                               "everything.")

def start_plugin(toolkit):
    """Start the plugin"""
    # If the plugin depends on others plugins, use:
    #if toolkit.depends_loaded(['plugin1', 'plugin2']):
    return DummyPlugin(toolkit)
