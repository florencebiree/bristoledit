# -*- coding: utf-8 -*-
###############################################################################
#       plugins/filecompleter.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Completer plugin for the file system in Bristoledit."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os

from bristol.utils.texthacks import END_OF_FILE
from bristol.utils.completion import Suggestion

class FileCompleter(object):
    """A filesystem autocompleter"""
    
    def __init__(self, editor):
        """Create a new completer object.
        
        editor is the current editor object
        """
        self.editor = editor
    
    def suggest(self, src_buffer):
        """Return suggestion results based on the text of src_buffer and its
        current position.
        """
        utext = src_buffer.text
        
        filename = os.path.expanduser(utext)
        rep = os.path.dirname(filename)
        rep = rep if rep else '.'
        name = os.path.basename(filename)
        
        # build a suggestion for each file in the rep directory
        # only add hidden (.) files if name starts like them
        # add a / after each directory
        dirify = lambda afile: (
            afile if not os.path.isdir(os.path.join(rep, afile))
            else afile + os.path.sep)
        try:
            suggestions = [
                Suggestion(
                    ulabel = dirify(afile),
                    umissing_chars = dirify(afile)[len(name):],
                ) for afile in sorted(os.listdir(rep))
                if ( (not name and not afile.startswith('.')) or
                     (name and afile.startswith(name))          )
            ]
        except OSError:
            suggestions = []
        
        # append the parent directory in the list
        par_dir = os.path.pardir + os.path.sep
        if (par_dir.startswith(name) and not os.path.samefile(rep, '/')):
            suggestions.insert(0, Suggestion(
                ulabel = par_dir,
                umissing_chars = par_dir[len(name):],
            ))
        return suggestions
    
def start_plugin(toolkit):
    """Return the file completer"""
    return FileCompleter
