# -*- coding: utf-8 -*-
###############################################################################
#       plugins/backupcopy.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Official plugin to save a backup (~) copy of files for Bristoledit."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os

class BackupCopyPlugin(object):
    """Backup copy on save plugin for Bristoledit"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
        @self.toolkit.hooks(toolkit.Buffer, 'save', self)
        def save(oldself, oldsave, filename=None, encoding=None, endofline=None):
            """Save a ~ copy of the file before saving."""
            if oldself._filename and (os.path.exists(oldself._filename) and
                                     filename is None):
                with open(oldself._filename, 'rb') as cur:
                    with open(oldself._filename + '~', 'wb') as bak:
                        bak.write(cur.read())
            return oldsave(oldself, filename, encoding, endofline)

def start_plugin(toolkit):
    """Start the plugin"""
    return BackupCopyPlugin(toolkit)
