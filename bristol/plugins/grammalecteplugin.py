# -*- coding: utf-8 -*-
###############################################################################
#       plugins/grammalecteplugin.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Spell and Grammar checker based on Grammalecte, grammar checker for the
french language."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

#import re
#import enchant
#from enchant.checker import SpellChecker
#from enchant.tokenize import EmailFilter, URLFilter, Filter, tokenize
#from enchant.tokenize import wrap_tokenizer, get_tokenizer

import json
import grammalecte

from bristol.utils.highlighting import AttributeMap
from bristol.utils.completion import Suggestion, get_current_token

class AbortCheck(Exception):
    """Raised when the user wants to abort a full check."""
    pass

class GrammalecteCompleter(object):
    """Provide grammar suggestions"""
    
    def __init__(self, suggestion_list):
        """Initialize the completer"""
        self.suggestions = suggestion_list
    
    def suggest(self, buffer_src):
        """Provide grammar suggestions"""
        token = get_current_token(buffer_src.text, buffer_src._position)
        return [
            Suggestion(ulabel, umissing_chars=ulabel[len(token):])
            for ulabel in self.suggestions
            if ulabel.startswith(token)
        ]

class GrammalecteInterface(object):
    """Provie an interface to Grammalecte"""
    
    def __init__(self, editor, config, get_checker):
        """Initialize the interface"""
        self._editor = editor
        self._config = config
        self._get_checker = get_checker
        self._userdoc_ = _("Interface for grammalecte.")
        
    def suggest(self, position=None):
        """Provide grammar suggestions about the current word
        
        If position is not None, provide suggestions about the word at this
        position.
        """
        if position is None:
            position = self._editor.current._position
        chkr = self._get_checker(self._editor.current)
        lineno = self._editor.current._line_of_position(position)
        start_of_line = self._editor.current._start_of_line(lineno)
        line_content = self._editor.current._get_line(lineno)
        chkr.set_text(line_content)
        error = None
        for err in chkr:
            if (err.wordpos <= (position - start_of_line) < (err.wordpos +
                                                                len(err.word))):
                # find the error
                error = err
                break
        if error is None:
            message = _("This word has a good spelling.")
        else:
            try:
                message = self._suggest(
                    error.word,
                    error.wordpos,
                    error.dict,
                    start_of_line,
                    error,
                )
            except AbortCheck:
                message = _("Check aborted.")
        return message
    suggest._userdoc_ = _("Provide spelling suggestions for this word.")
            
    def _suggest(self, word, wordpos, edict, ref, error, nxt=False):
        """Provide suggestions with grammalecte error, and a start text ref."""
        suggestions = (
            [_("add"), _("ignore")] +
            ([_("next")] if nxt else []) +
            edict.suggest(word)
        )
        #self._editor.plugins.load('askingsomething')
        prompt = _("Correction for %s: ") % word
        result = self._editor.interface._ask(
            prompt,
            completer = SpellCheckerCompleter(suggestions)
        )
        if result == _("add"):
            #edict.add(word)
            error.add(word)
            message = _("Word %s added to the dictionnary.") % word
            # TODO: dirty forced refresh
            self._editor.current._set_text(self._editor.current.text)
        elif result == _("ignore"):
            error.ignore_always(word)
            message = _("Misspelled word ingnored.")
            # TODO: dirty forced refresh
            self._editor.current._set_text(self._editor.current.text)
        elif result == _("next"):
            # next? should ignore a word just one time
            pass
        elif not result:
            raise AbortCheck
        else:
            self._editor.current._set_text(
                result, None,
                ref + wordpos,
                ref + wordpos,
                len(word)
            )
            message = _("Word replaced by %s.") % result
        return message
    
    def check_all(self):
        """Check all misspelled words of the text"""
        message = _("All misspelled words checked.")
        chkr = self._get_checker(self._editor.current)
        chkr.set_text(self._editor.current.text)
        error_list = []
        for error in chkr:
            (word, wordpos) = (error.word, error.wordpos)
            # move the cursor to the word
            self._editor.current._set_text('', wordpos, wordpos)
            try:
                self._suggest(word, wordpos, chkr.dict, 0, error)
            except AbortCheck:
                message = _("Check aborted.")
                break
            chkr.set_text(self._editor.current.text)
        return message
    check_all._userdoc_ = _("Check all misspelled words of this text.")
    
    #def switch_online_check(self):
    #    """Enable or disable the online check"""
    #    if not hasattr(self._editor.current, '__spell_check'):
    #        cur_status = self._config[self._editor.current._mimetype, 'enabled']
    #    else:
    #        cur_status = getattr(self._editor.current, '__spell_check')
    #    setattr(self._editor.current, '__spell_check', not cur_status)
    #    # TODO: dirty forced refresh
    #    self._editor.current._set_text(self._editor.current.text)
    #switch_online_check._userdoc_ = _("Enable or disable the online check.")

class GrammarChecker(object):
    """The default GrammarChecker"""
    
    default_options = {'latex': True, 'html': True}
    default_dis_rules = ()
    
    def __init__(self, options=None, disabled_rules=None):
        """Initialize the GrammarChecker"""
        self.grammar_checker = grammalecte.GrammarChecker("fr")
        self.options = self.default_options.copy()
        if options:
            self.options.update(options)
        self.disabled_rules = self.default_dis_rules
        if disabled_rules:
            self.disabled_rules += disabled_rules
        # apply options and disabled rules
        self.grammar_checker.gce.setOptions(self.options)
        for rule in self.disabled_rules:
            self.grammar_checker.gce.ignoreRule(rule)
    
    def get_paragraphs(self, tbuffer, min_position, max_position):
        """Generator of  (paragraph_text, start_position) by splitting the 
            tbuffer's text in paragraphs.
            
            This default function split text at each newline
        """
        first_line = tbuffer._line_of_position(min_position)
        last_line = tbuffer._line_of_position(max_position)
        for lineno in range(first_line, last_line):
            yield (tbuffer._get_line(lineno), tbuffer._start_of_line(lineno))
    
    def get_errors(self, tbuffer, min_position=None, max_position=None):
        """Generator of Grammalecte errors"""
        if not min_position:
            min_position = 0
        if not max_position:
            max_position = len(tbuffer.text)
        for text, startpos in self.get_paragraphs(tbuffer, min_position, max_position):
            yield json.loads(
                self.grammar_checker.generateParagraphAsJSON(
                    startpos,
                    text,
                    bSpellSugg = True,
                ))

class LatexGrammarChecker(GrammarChecker):
    """GrammarChecker with Latex presets"""
    
    default_options = {
        'latex': True,  # ignore latex tags
        'apos': False,  # vérifier les apostrophes typographiques
        'esp': False,   # vérifier les espaces surnuméraires
        'nbsp': False,  # vérifier les espaces insécables
    }
    default_dis_rules = (
        'esp_début_ligne',  # espaces en début de ligne
        'esp_fin_ligne',    # espaces en fin de ligne
    )
    
    def get_paragraphs(self, tbuffer, min_position, max_position):
        """Generator of  (paragraph_text, start_position) by splitting the 
            tbuffer's text in paragraphs.
            
            This default function split text at each newline
        """
        first_line = tbuffer._line_of_position(min_position)
        last_line = tbuffer._line_of_position(max_position)
        
        current_paragraph = ""
        start_of_par = None
        # reverse search, to see if we are not at the start of the paragraph
        for lineno in range(first_line, 0, -1):
            curline = tbuffer._get_line(lineno)
            if curline.strip():
                # line has content, is a part of the current paragraph
                current_paragraph = curline + current_paragraph
                start_of_par = tbuffer._start_of_line(lineno)
            else:
                # end of the paragraph, break and search forward
                break
        # current search inside min and max position
        for lineno in range(first_line, last_line):
            curline = tbuffer._get_line(lineno)
            if curline.strip():
                # line has content
                current_paragraph += curline
                if start_of_par is None:
                    start_of_par = tbuffer._start_of_line(lineno)
            else:
                # end of paragraph, yield only if there is a known paragraph
                if start_of_par is not None:
                    yield (current_paragraph, start_of_par)
                current_paragraph = ""
                start_of_par = None
        # go forward to end the current paragraph
        if start_of_par is not None:
            for lineno in range(last_line, tbuffer._number_of_line()):
                curline = tbuffer._get_line(lineno)
                if curline.strip():
                    # line has content
                    current_paragraph += curline
                else:
                    yield (current_paragraph, start_of_par)
                    current_paragraph = ""
                    start_of_par = None
                    break
        # yield a last time for the last paragraph
        if start_of_par is not None:
            yield (current_paragraph, start_of_par)


class GrammalectePlugin(object):
    """Grammalecte plugin to provide a french grammar checker for bristol"""
        
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
        # register default settings
        default_config = {
            'enabled': (False, _("Define if the grammar checker is enabled.")),
            'options': ({}, _("Grammalecte option, see grammalecte-cli.py -lo")),
            'disabled_rules': ((), _("Grammalecte disabled rue, see grammalecte-cli.py -lr")),
            'preset' : ("", _("Preset of rules and options")),
            'text/x-tex': {
                'enabled': True,
                'preset' : 'latex',
            },
        }
        self.toolkit.config.set_default(default_config, [
            "editor.config.key['KEY_F(7)'] = "
                "'editor.grammalecte.check_all()'",
            "editor.config.key['KEY_F(8)'] = "
                "'editor.grammalecte.suggest()'",
        ])
        # set interface hooks
        type(toolkit.editor).grammalecte = GrammalecteInterface(
            self.toolkit.editor,
            self.toolkit.config,
            self.get_checker,
        )
        # register buffer settings
        def full_refresh(buff, value, finalfset):
            """Force a full refresh of the buffer after a buffer property
                change
                
            FIXME: dirty forced refresh
            """
            finalfset(buff, value)
            buff._set_text(buff.text)
        
        toolkit.make_buffer_property('enabled', 'grammalecte_enabled',
                                     full_refresh)
        
        # set internal hooks
        @self.toolkit.hooks(toolkit.Buffer, '_get_attribute_map', self)
        def _get_attribute_map(oldself, old_get_attribute_map,
                                        min_position=None, max_position=None):
            attr_map = old_get_attribute_map(oldself, min_position,
                                             max_position)
            # if enabled, perform an spell check on those data
            enabled = oldself.grammalecte_enabled
            if enabled:
                chkr = self.get_checker(oldself)
                if chkr is None:
                    return attr_map
                #_debug_print(repr(list(chkr.get_errors(oldself))))
                # get bad words
                for paragraph in chkr.get_errors(oldself, min_position, max_position):
                    start_of_par = paragraph['iParagraph']
                    for error in paragraph['lGrammarErrors']:
                        err_start = start_of_par + error['nStart']
                        err_stop = start_of_par + error['nEnd']
                        attr_map[err_start:err_stop] = 'code_style.generic.badword'
                # get bad words
                #chkr.set_text(oldself.text[min_position:max_position])
                #for err in chkr:
                #    # hijack the attribute map
                #    attr_map[err.wordpos:err.wordpos+len(err.word)] = \
                #                                'code_style.generic.badword'
            return attr_map
    
    def get_checker(self, abuffer):
        """Return the right configured spell checker."""
        if not hasattr(abuffer, '__grammar_checker'):
            options = self.toolkit.config[abuffer._mimetype, 'options']
            disabled_rules = self.toolkit.config[abuffer._mimetype, 'disabled_rules']
            preset = self.toolkit.config[abuffer._mimetype, 'preset']
            
            if preset == 'latex':
                chkr = LatexGrammarChecker(options, disabled_rules)
            else:
                chkr = GrammarChecker(options, disabled_rules)
            setattr(abuffer, '__grammar_checker', chkr)
        else:
            chkr = getattr(abuffer, '__grammar_checker')
        return chkr

def start_plugin(toolkit):
    """Start the plugin"""
    return GrammalectePlugin(toolkit)
