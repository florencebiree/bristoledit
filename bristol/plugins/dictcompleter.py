# -*- coding: utf-8 -*-
###############################################################################
#       plugins/dictcompleter.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Completer based on used word for Bristoledit."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from bristol.buffer import Buffer
from bristol.utils.hooks import hooks
from bristol.utils.texthacks import END_OF_FILE
from bristol.utils.completion import Suggestion, get_current_token

class DictCompleter(object):
    """A statistical autocompleter for Bristol"""
    
    toolkit = None
    
    def __init__(self, editor):
        """Create a new completer object.
        
        editor is the current editor object
        """
        self.editor = editor
        self.toolkit.config.set_default({
            'min length': (2,
                    _("Minimum length of a word to propose autocompletion")),
            'word characters': ('_$',
                    _("Characters to be considered as a in a word.")),
        })

        @hooks(Buffer, '_set_text', self)
        def set_text(oldself, oldset_text, new_text=None, new_position=None,
                    insert_position=None, delete_position=None, delete_len=0):
            """Update the dictionary on buffer change"""
            utext = oldself.text
            ltok = rtok = ""
            if (delete_position is not None and hasattr(oldself, '_words')):
                # Remove the to be deleted token
                dltok = get_current_token(utext, delete_position,
                            self.toolkit.config['word characters'])
                drtok = get_current_token(utext, delete_position + delete_len,
                            self.toolkit.config['word characters'],
                            False, True)
                
                dtoks = self.words_of_text(
                    dltok +
                    oldself.text[delete_position:delete_position+delete_len] +
                    drtok)
                for word in dtoks:
                    if word in oldself._words:
                        oldself._words[word] -= dtoks[word]
                        if oldself._words[word] < 1:
                            del oldself._words[word]
            elif (insert_position is not None and hasattr(oldself, '_words')):
                # Remove the to be modified token
                ltok = get_current_token(utext, insert_position,
                            self.toolkit.config['word characters'])
                rtok = get_current_token(utext, insert_position,
                            self.toolkit.config['word characters'],
                            False, True)
            
                oldtok = get_current_token(utext, insert_position,
                            self.toolkit.config['word characters'],
                            True, True)
                if oldtok in oldself._words:
                    oldself._words[oldtok] -= 1
                    if oldself._words[oldtok] == 0:
                        del oldself._words[oldtok]
            
            oldset_text(oldself, new_text, new_position, insert_position,
                        delete_position, delete_len)
            
            if (not hasattr(oldself, '_words') or 
                        (insert_position is None and delete_position is None)):
                # full rebuild
                oldself._words = self.words_of_text(oldself.text)
            elif insert_position is not None and new_text:
                # add modified tokens
                oldself._words = self.words_of_text(
                    ltok + new_text + rtok,
                    oldself._words,
                )
    
    def suggest(self, src_buffer):
        """Return suggestion results based on the text of src_buffer and its
        current position.
        """
        utext = src_buffer.text
        if not utext or not hasattr(src_buffer, '_words'):
            return []
        position = src_buffer._position
        # isolate the current token
        token = get_current_token(utext, position,
                                  self.toolkit.config['word characters'])
        if not token or len(token) < self.toolkit.config['min length']:
            return []
        ftoken = get_current_token(utext, position,
                                    self.toolkit.config['word characters'],
                                    True, True)
        # build suggestions
        suggestions = [
            Suggestion(
                ulabel = word,
                udesc = None,
                umissing_chars = word[len(token):],
                uafter_chars = None,
            ) for word in src_buffer._words if
                word.startswith(token) and len(word) > len(token) and 
                word != ftoken
        ]
        return suggestions
    
    def words_of_text(self, utext, old_words=None):
        """Return a word dict of the text"""
        if old_words is None:
            words = {}
        else:
            words = old_words
        cur_word = ''
        for letter in utext:
            if (letter.isalnum() or
                        letter in self.toolkit.config['word characters']):
                cur_word += letter
            else:
                if cur_word in words:
                    words[cur_word] += 1
                    cur_word = ''
                else:
                    words[cur_word] = 1
                    cur_word = ''
        if cur_word in words:
            words[cur_word] += 1
        else:
            words[cur_word] = 1
        return words

def start_plugin(toolkit):
    """Return the completer"""
    DictCompleter.toolkit = toolkit
    return DictCompleter
