# -*- coding: utf-8 -*-
###############################################################################
#       plugins/template.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Plugin to provide a templating system"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import re
import datetime
from xdg.BaseDirectory import save_data_path

INCLUDE = re.compile(r'\{inc:([^}]+)\}')
TOKEN = re.compile(r'\{([^}]+)\}')

class TemplatePlugin(object):
    """Plugin to provide templating system"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
        # define default path
        default_dir = save_data_path( 
            os.path.join(self.toolkit.editor._application, 'templates')
        )
        try:
            username = os.getlogin()
        except OSError:
            username = 'Marvin'
        self.toolkit.config.set_default({
            'template path': (default_dir,
                                _("Directory where templates are saved.")),
            'year': (str(datetime.date.today().year),
                                _("Year to be added in templates.")),
            'author': (username, _("Author to be added in templates.")),
            'email': (username + '@localhost',
                                _("E-mail to be added in templates.")),
            'license': ('gplv3+', _("Template to be included as license")),
        })
        
        # write default templates if the directory is empty
        tpl_dir = self.toolkit.config['template path']
        if not os.path.isdir(tpl_dir):
            os.makedirs(tpl_dir)
        if not os.listdir(tpl_dir):
            write_default_tpl(tpl_dir)
        
        @self.toolkit.improve(type(toolkit.editor), 'template')
        def template(oldself, template_file=None):
            """Create a new file with the content of the template"""
            tpl_dir = self.toolkit.config['template path']
            if not tpl_dir.endswith(os.path.sep):
                tpl_dir += os.path.sep
            tpl_dir = tpl_dir.replace(os.path.expanduser('~'), '~')
            if template_file is None:
                template_file = self.toolkit.editor.interface._ask_a_filename(
                    prompt = _('Template: '),
                    default = tpl_dir
                )
                if not template_file:
                    return
            template_file = os.path.expanduser(template_file)
            if not os.path.exists(template_file):
                template_file = os.path.join(tpl_dir, template_file)
            if not os.path.exists(template_file):
                return
            new_buff = self.toolkit.editor.new()
            # process templates
            new_buff._set_text(self.process(template_file))
        template._userdoc_ = _("Create a new file from a template.")
    
    def process(self, tpl_name):
        """Process a template, return its content"""
        tpl_dir = self.toolkit.config['template path']
        if not os.path.exists(tpl_name):
            tpl_name = os.path.join(tpl_dir, tpl_name)
        if not os.path.exists(tpl_name):
            return ""
        # open the current template
        with open(tpl_name, 'rb') as tpl:
            content = tpl.read().decode('utf-8', 'replace')
        # process includes
        lines = content.split('\n')
        res_lines = []
        for line in lines:
            includes = INCLUDE.findall(line)
            if includes:
                line_start = line.split('{')[0]
                # load only the first include of the line (yes, it's dirty)
                inc_tpl = includes[0]
                conf_inc_tpl = self.toolkit.config[inc_tpl]
                if conf_inc_tpl:
                    inc_tpl = conf_inc_tpl
                inc_tpl = self.process(inc_tpl)
                inc_lines = inc_tpl.split('\n')
                res_lines += [line_start + iline for iline in inc_lines]
            else:
                res_lines.append(line)
        content = '\n'.join(res_lines)
        # process simples replacements
        tokens = TOKEN.findall(content)
        for token in tokens:
            val = self.toolkit.config[token]
            if val:
                if not isinstance(val, str):
                    val = val.decode('utf-8', 'replace')
                content = content.replace('{%s}' % token, val)
        return content
        
def start_plugin(toolkit):
    """Start the plugin"""
    return TemplatePlugin(toolkit)

def write_default_tpl(dir):
    """Write a set of default templates"""
    for tpl in DEFAULTS:
        name = os.path.join(dir, tpl)
        with open(name, 'w') as tpl_file:
            tpl_file.write(DEFAULTS[tpl])

DEFAULTS = {
'gplv3+': """\
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
""",

'gplv2+': """\
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301, USA.
""",

'bsd': """\
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the following disclaimer
  in the documentation and/or other materials provided with the
  distribution.
* Neither the name of the {company} nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
""",

'free-art': """\
Copyleft: this is a free work, you can copy, distribute, and
modify it under the terms of the Free Art License
<http://artlibre.org/licence/lal/en/>
""",

'c-source.c': """\
/*
 *      {filename}
 *      
 *      Copyright © {year}, {author} <{email}>
 *      
 *      This file is a part of {project}.
 *      
 *      {inc:license}
 */

#include <stdio.h>

int main(int argc, char** argv)
{
	
	return 0;
}
""",

'cpp-source.cpp': """\
/*
 *      {filename}
 *      
 *      Copyright © {year}, {author} <{email}>
 *      
 *      This file is a part of {project}.
 *      
 *      {inc:license}
 */

#include <iostream>

int main(int argc, char** argv)
{
	
	return 0;
}
""",

'Makefile': """\
CC=gcc
CFLAGS=-W -Wall -ansi -pedantic
LDFLAGS=
EXEC=execname
OBJ=src/file1.o src/file2.o

util: $(OBJ)
	$(CC) $(OBJ) -o $(EXEC)

src/file1.o: src/file1.c src/file1.h
src/file2.o: src/file1.h src/file2.h src/file2.c

install:
	cp $(EXEC) /usr/local/bin/

uninstall:
	rm /usr/local/bin/$(EXEC)

clean:
	rm -rf src/*.o

mrproper: clean
	rm -rf $(EXEC)
""",

'xhtml.html': """\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!--
    Copyright © {year}, {author} <{email}>
    
    {inc:license}
-->

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>{intitled}</title>
    <meta name="generator" content="Bristoledit" />
</head>

<body>
    
</body>
</html>
""",

'css.css': """\
/*
 *      {filename}
 *      
 *      Copyright © {year}, {author} <{email}>
 *      
 *      This file is a part of {project}.
 *      
 *      {inc:license}
 */

body {
	background-color:red;
}
""",

'java.java': """\
/*
 *      {filename}
 *      
 *      Copyright © {year}, {author} <{email}>
 *      
 *      This file is a part of {project}.
 *      
 *      {inc:license}
 */

public class {untitled} {
	public static void main (String args[]) {
		
	}
}
""",

'latex.tex': r"""%%%
 %	{filename}
 %	
 %	Copyright © {year}, {author} <{email}>
 %	
 %	{inc:license}
 %
%%%

% Loading packages
\documentclass[a4paper, 12pt]{book}
% Using UTF-8
\usepackage{ucs}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

% Metadata
\title{Title}
\author {{author}}
\date{{year}}

% Document
\begin{document}
\end{document}
""",

'Makefile-latex': """\
LATEX=pdflatex
TEX=*.tex

util: $(TEX)
	$(LATEX) $(TEX)
	$(LATEX) $(TEX)

clean:
	rm -f *.aux *.nav *.out *.d *.log *.snm *.toc *~

mrproper: clean
	rm -f *.pdf
""",

'python.py': '''\
# -*- coding: utf-8 -*-
###############################################################################
#       {filename}
#       
#       Copyright © {year}, {author} <{email}>
#       
#       This file is a part of {project}.
#       
#       {inc:license}
###############################################################################
"""{docstring}"""

__author__ = "{author}"
__version__ = "{version}"
__license__ = "GPL"
__copyright__ = "Copyright © {year}, {author} <{email}>"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import sys

if __name__ == '__main__':
    pass
''',

'ocaml.ml': """\
(** {Module description}
	
	Copyright © {year}, {author} <{email}>
	
	@author {author}
	@verison {version}
	
	This file is a part of {project}.
	
	{inc:license}
*)
""",
}
