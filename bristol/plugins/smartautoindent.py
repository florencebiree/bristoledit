# -*- coding: utf-8 -*-
###############################################################################
#       plugins/smartautoindent.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Official plugin to have a smart indent for programing languages."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

class SmartAutoIndentPlugin(object):
    """Have a smart indent for programming languages"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
        # define when the plugin does something
        self.toolkit.config.set_default({
            'colon indent': (['text/x-python'],
                               _('Indent when the last line ends by ":"')),
            'curly bracket indent': (['text/x-c', 'text/x-chdr', 'text/x-csrc',
                                      'text/x-java', 'text/css', 'text/x-c++hdr'
                                     ],
                               _('Indent when the last line ends by "{"')),
        })
        
        @self.toolkit.hooks(toolkit.Buffer, 'indent', self)
        def indent(oldself, oldindent, position=None, delta=0, ref=-1):
            """Change indent following previous character"""
            if position is None:
                position = oldself._position
            if delta == 0 and ref == -1:
                # get the last character
                curlineno = oldself._line_of_position(position)
                if curlineno > 0:
                    lastlinectn = oldself._get_line(curlineno - 1)
                    try:
                        last_char = lastlinectn.rstrip()[-1]
                    except IndexError:
                        last_char = None
                    # change indent if the last charcater is corresponding
                    # to a patern with a good mimetype
                    if (last_char == ':' and oldself._mimetype in
                            self.toolkit.config['colon indent']):
                        delta += 1
                    elif (last_char == '{' and oldself._mimetype in
                            self.toolkit.config['curly bracket indent']):
                        delta += 1
            return oldindent(oldself, position, delta, ref)

def start_plugin(toolkit):
    """Start the plugin"""
    return SmartAutoIndentPlugin(toolkit)
