# -*- coding: utf-8 -*-
###############################################################################
#       plugins/viewer.py
#       
#       Copyright © 2012-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Support viewers for documents edited with Bristoledit

    Currently, support:
            .tex -> pdf (with synctex support)

"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import shlex
from subprocess import Popen, PIPE

class ViewerPlugin(object):
    """This is the viewer plugin for Bristoledit"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        self._asyncrun = toolkit.manager._loaded['asyncrun'].run
        
        self.autopen = (
            toolkit.editor._options.viewer if toolkit.editor._options
            else False
        )
        
        self.running = []
        
        self.toolkit.config.set_default({
            'viewer': (None, _("Viewer command")),
            'ext': (None, _("The extension of the to-be-viewed file")),
            'synctex': (None, _("Option to pass to enable SyncTeX")),
            'synctex_forward': (None, 
                _("Option to pass to go to a SyncTex position")),
            'text/x-tex': {
                'viewer': 'zathura',
                'synctex': '--synctex-editor-command=synctexinfos.sh ' + 
                           '%{line} %{column} %{input}',
                'synctex_forward': '--synctex-forward=%s',
                'ext': 'pdf',
            },
        }, [
            "editor.config.key['KEY_F(6)'] = 'editor.current.view()'",
            "editor.config.key['KEY_F(11)'] = " +
                                        "'editor.current.go_in_viewer()'",
        ])
        
        # utility function
        def common_checks(oldself):
            """Check if the file is saved on the disk, 
                and a viewer is defined
                return None if ok
                else return an error message
            """
            # if the file doesn't exist on the disk
            if not os.path.exists(oldself._filename):
                return _("The file must be saved on the disk to be viewed.")
            # if no viewer for this type of file:
            elif not self.toolkit.config[oldself._mimetype, 'viewer']:
                # error
                return _("No viewer for %s files") % oldself._mimetype
            else:
                return None
        
        # make hooks !
        # New view command (editor.current.view or F6)
        @self.toolkit.improve(toolkit.Buffer, 'view')
        def view(oldself):
            """Execute the configured viewer for the current document"""
            error = common_checks(oldself)
            if error is not None:
                return error
            else:
                viewercmd = shlex.split(
                            self.toolkit.config[oldself._mimetype, 'viewer'])
                base = os.path.splitext(oldself._filename)[0]
                vfile = base + '.' + self.toolkit.config[oldself._mimetype, 'ext']
                if not os.path.exists(vfile):
                    return
                # check if synctex is enabled ('.synctex.gz file')
                synctex = self.toolkit.config[oldself._mimetype, 'synctex']
                if synctex:# and os.path.exists(base + '.synctex.gz'):
                    #syncfile = base + '.synctex.gz'
                    if isinstance(synctex, str):
                        viewercmd.append(synctex)
                # run viewer in backgroud
                viewercmd.append(vfile)
                process = self._asyncrun(viewercmd)
                process.vfile = vfile
                # allow to get events from stdout
                self.running.append(process)
                return _("Open %s") % viewercmd[0]
        view._userdoc_ = _("Open the configured viewer for this file.")
        
        # New go_in_viewer command (editor.current.go_in_view or F11)
        @self.toolkit.improve(toolkit.Buffer, 'go_in_viewer')
        def go_in_viewer(oldself):
            """Go in the viewer to the place corresponding to the current one"""
            error = common_checks(oldself)
            if error is not None:
                return error
            else:
                # if no opened viewer, open one with current file
                if not self.running:
                    view(oldself)
                try:
                    current_vfile = self.running[0].vfile
                except IndexError:
                    return _("No viewer currently opened")
                
                viewercmd = shlex.split(
                            self.toolkit.config[oldself._mimetype, 'viewer'])
                synctexf = self.toolkit.config[oldself._mimetype, 
                                                'synctex_forward']
                if not isinstance(synctexf, str):
                    return _("No synctex forward option configured.")
                
                synctex_pos = (
                    #li:col:file
                    str(oldself._line_of_position(oldself._position)) + ':'+
                    str(oldself._index_of_position(oldself._position)) +':'+
                    #os.path.basename(oldself._filename)
                    oldself._filename
                )
                
                viewercmd.append(synctexf % synctex_pos)
                
                # send call to the viewer in background
                viewercmd.append(current_vfile)
                process = self._asyncrun(viewercmd)
                return _("Go to %s in the viewer.") % synctex_pos
        go_in_viewer._userdoc_ = _("Go in the viewer to the current position.")
                
        
        # If synctex enabled, watch for events to get the position
        @self.toolkit.hooks(type(self.toolkit.editor), '_check_events', self)
        def _check_events(oldself, old_check_events):
            ret = old_check_events(oldself)
            stillrunning = []
            for process in self.running:
                if process.is_terminated() is not None:
                    # the process is dead
                    pass
                else:
                    stillrunning.append(process)
                    # try to read data from stout
                    syncctx = False
                    syncsrc = None
                    syncline = None
                    while True:
                        try:
                            line = process.readline().decode('utf-8')
                        except process.Empty:
                            break
                        else: # parse stdout
                            if 'SyncTeX result begin' in line:
                                syncctx = True
                            elif syncctx:
                                if 'SyncTeX result end' in line:
                                    syncctx = False
                                elif line.startswith('Input:'):
                                    syncsrc = line[len('Input:'):-1]
                                elif line.startswith('Line:'):
                                    syncline = int(line[len('Line:'):-1])
                    # apply sync
                    if syncsrc:
                        _debug_print('Synctex request: line %i, file %s' %
                                        (syncline, syncsrc))
                        # try to find the right file
                        for buff in toolkit.editor.buffers:
                            if (buff._filename and 
                                    os.path.samefile(syncsrc, buff._filename)):
                                buff.go_to_line(syncline)
                                toolkit.editor.switch_to(buff)
                                ret = _("Viewer: go to line %d") % syncline
                            
            self.running = stillrunning
            return ret
        
        # If the -w, --wiewer option is set, open the view on _open 
        @self.toolkit.hooks(self.toolkit.Buffer, '_open', self)
        def _open(oldself, old_open, filename, encoding=None, endofline=None):
            """Open the viewer if the option is set"""
            ret = old_open(oldself, filename, encoding, endofline)
            if self.autopen:
                oldself.view()
            return ret
        
def start_plugin(toolkit):
    """Start the plugin"""
    if toolkit.depends_loaded(['asyncrun']):
        return ViewerPlugin(toolkit)
