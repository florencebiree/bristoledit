# -*- coding: utf-8 -*-
###############################################################################
#       plugins/wrap.py
#       
#       Copyright © 2010-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bristoledit plugin to wrap line to less than 80 characters."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2010-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from bristol.utils.completion import get_current_token, tokenize
from bristol.utils.texthacks import tab_len, END_OF_FILE

class WrapPlugin(object):
    """Plugin to wrap lines to be less than 80 characters"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
        default_config = {
            'auto wrap': (False, _("Define if auto wrap is enabled.")),
            'spaces': ('',
                _("What charcaters are considered as a word separator.")),
            'text/x-tex': {
                'auto wrap': True,
            },
            'application/x-tex': {
                'auto wrap': True,
            },
        }
        self.toolkit.config.set_default(default_config)
        
        toolkit.make_buffer_property('auto wrap')
        
        # hooks
        @self.toolkit.hooks(toolkit.Buffer, '_set_text', self)
        def _set_text(oldself, oldset_text, new_text=None, new_position=None,
                      insert_position=None, delete_position=None, delete_len=0):
            """Hooks _set_text to wrap lines when they exceed 80 characters"""
            res = oldset_text(oldself, new_text, new_position, insert_position,
                              delete_position, delete_len)
            #if (self.toolkit.config[oldself._mimetype, 'auto wrap']):
            if (oldself.auto_wrap):
                newpos = self.wrap_line(oldself)
                if newpos is not None:
                    oldself._set_text(None, newpos)
            return res
        
        @self.toolkit.improve(toolkit.Buffer, 'wrap')
        def wrap(oldself):
            """Wrap all lines"""
            lineno = 0
            while (lineno < oldself._number_of_line()):
                newpos = self.wrap_line(oldself, lineno)
                if newpos is not None:
                    oldself._set_text(None, newpos)
                lineno += 1
            # force a dirty full refresh
            self.toolkit.editor.current._set_text(self.toolkit.editor.current.text)
        wrap._userdoc_ = _("Wrap all lines to make them less 80 characters.")
    
    def wrap_line(self, curbuffer, lineno=None):
        """Wrap the line lineno
        
        If lineno is None, use the current line.
        Return where the cursor should be to keep it at a similar place, or
        None.
        """
        if not lineno:
            lineno = curbuffer._line_of_position(curbuffer._position)
        init_position = curbuffer._position
        new_pos = None
        spaces = self.toolkit.config[curbuffer._mimetype, 'spaces']
        
        line_ctn = curbuffer._get_line(lineno)
        tlen = tab_len(line_ctn, curbuffer.tab_size)
        sol = curbuffer._start_of_line(lineno)
        # abort if the line is not longer than 80 characters
        while tlen >= 81:
            pos_inside = (sol <= init_position < sol + len(line_ctn))
            spaceleft = 79
            wrappos = None
            # find the starting position of the token which must be wrapped
            for (startpos, token, is_word, tklen) in tokenize(line_ctn, curbuffer.tab_size, spaces):
                if tklen > spaceleft:
                    wrappos = startpos
                    break
                else:
                    spaceleft -= tklen
            if wrappos is None:
                return None
            
            # check for big tokens, longer than the line (do not wrap)
            begining = line_ctn[:wrappos]
            if not begining or begining.isspace():
                return None
            
            # put the wrapped text to the next line
            if line_ctn[-1] != '\n':
                # last line: insert a \n at the wrappos
                curbuffer._set_text('\n', None, sol + wrappos)
                    
                if curbuffer.auto_indent:
                    indent_len = curbuffer.indent(curbuffer._start_of_line(
                                                                    lineno + 1))
                else:
                    indent_len = 0
                
                if init_position - sol > wrappos and pos_inside:
                    wrapped_txt = line_ctn[wrappos:]
                    if wrapped_txt.isspace():
                        new_ctn = curbuffer._get_line(lineno + 1)
                        new_pos = (curbuffer._start_of_line(lineno + 1) + 
                                    len(new_ctn))
                    else:
                        new_pos = (curbuffer._start_of_line(lineno + 1) +
                                init_position - sol - wrappos + indent_len)
                
            elif curbuffer._get_line(lineno + 1).isspace() or \
                    curbuffer._get_line(lineno + 1) == '':
                # the next line is not a part of the paragraph
                curbuffer._set_text('\n', None, sol + wrappos)
                if curbuffer.auto_indent:
                    indent_len = curbuffer.indent(curbuffer._start_of_line(
                                                                    lineno + 1))
                else:
                    indent_len = 0
                if init_position - sol > wrappos and pos_inside:
                    wrapped_txt = line_ctn[wrappos:-1]
                    if wrapped_txt.isspace():
                        new_ctn = curbuffer._get_line(lineno + 1)
                        new_pos = (curbuffer._start_of_line(lineno + 1) + 
                                    len(new_ctn) - 1)
                    else:
                        new_pos = (curbuffer._start_of_line(lineno + 1) +
                                init_position - sol - wrappos + indent_len)
            else:
                # save text to be removed
                wrapped_txt = line_ctn[wrappos:-1]
                
                # remove text after the wrappos
                del_pos = sol + wrappos
                del_len = len(line_ctn) - wrappos - 1
                curbuffer._set_text(delete_position=del_pos, delete_len=del_len)
                
                # add the text at the begining of the next line
                new_sol = curbuffer._start_of_line(lineno + 1)
                new_ctn = curbuffer._get_line(lineno + 1)
                for new_start in range(len(new_ctn)):
                    if new_ctn[new_start] not in ' \t':
                        break
                # strip whitespaces
                wrapped_txt = wrapped_txt.strip()
                if wrapped_txt and not wrapped_txt[-1].isspace():
                    wrapped_txt += ' ' 
                curbuffer._set_text(wrapped_txt, None, new_sol + new_start)
                
                if init_position - sol > wrappos and pos_inside:
                    new_pos = (curbuffer._start_of_line(lineno + 1) + 
                                init_position - sol - wrappos + new_start)
            
            # start again with the next line
            lineno += 1
            line_ctn = curbuffer._get_line(lineno)
            tlen = tab_len(line_ctn, curbuffer.tab_size)
            sol = curbuffer._start_of_line(lineno)

        return new_pos
        
        

def start_plugin(toolkit):
    """Start the plugin"""
    return WrapPlugin(toolkit)
