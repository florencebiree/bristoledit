# -*- coding: utf-8 -*-
###############################################################################
#       plugins/pythonshell.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Python interpreter plugin for Bristoledit"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import sys
import traceback	# only used in debug mode

class PythonInterpreter(object):
    """Python interpreter to manage the bristol command line"""
    
    def __init__(self, local_env=None):
        """Initialize a new python interpreter
        
        local_env is the default environment.
        """
        self.locals = local_env
        self.prompt = '>>> '
        self.incomplete = False
        self.single_char = False
        self.history = []
        self.hist_idx = 0
        self.result = None
    
    def run(self, ucommand, no_history=False):
        """Run a command (unicode text)
        
        Return the output as an unicode text
        If there is no given output and no error, return u'OK'
        If there is an exception, return the description of the exception
        """
        # add in history
        if not no_history:
            self.add_to_history(ucommand)
        # run the code
        try:
            # check if it can be eval() (which return a value)
            self.result = eval(ucommand, self.locals)
        except SyntaxError:
            # else try with exec (which doesn't return a value...)
            try:
                #exec ucommand in self.locals
                exec(ucommand, self.locals)
            except SystemExit:
                raise
            except:
                exc_info = sys.exc_info()
                _debug_print('PySh> ' + repr(ucommand))
                tb = traceback.format_exc()
                _debug_print(tb)
                error = exc_info[1]
                return error.args[0]
        except SystemExit:
            raise
        except:
            exc_info = sys.exc_info()
            _debug_print('PySh> ' + repr(ucommand))
            tb = traceback.format_exc()
            _debug_print(tb)
            error = exc_info[1]
            return error #.args[0]
        # in all succesful cases:
        return self.result  # even None! (printing it the interface job)

    
    def add_to_history(self, ucommand):
        """Add a command to the history
        
        (all commands executed by run goes in the history)
        """
        if ucommand in self.history: # no doubles
            self.history.remove(ucommand)
        self.history.append(ucommand)
        # reset the index
        self.hist_idx = len(self.history)
    
    def history_prev(self):
        """Return the previous entry in the history.
        
        Return None if no previous entry.
        """
        if self.hist_idx - 1 in range(len(self.history)):
            self.hist_idx -= 1
            return self.history[self.hist_idx]
        else:
            return None
    
    def history_next(self):
        """Return the next entry in the history.
        
        Return None if no previous entry.
        """
        if self.hist_idx + 1 in range(len(self.history)):
            self.hist_idx += 1
            return self.history[self.hist_idx]
        else:
            return None

def start_plugin(toolkit):
    """Return the python interpreter"""
    return PythonInterpreter
