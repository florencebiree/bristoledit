# -*- coding: utf-8 -*-
###############################################################################
#       plugins/pygmentization.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Official Pygment plugin for bristoledit (poweful syntax highlighting)."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from pygments.lexers import guess_lexer, guess_lexer_for_filename, TextLexer
from pygments.lexers import get_lexer_by_name, LEXERS, get_lexer_for_mimetype
from pygments.util import ClassNotFound
from pygments.token import Keyword, Name, Comment, String, Error, Number
from pygments.token import Operator, Generic, Whitespace, Punctuation, Other
from pygments.token import Literal
from pygments.formatter import Formatter
from pygments.formatters import HtmlFormatter, LatexFormatter
from pygments import highlight

from bristol.utils.highlighting import AttributeMap

ATTR_PREFIX = 'code_style'

def highlightIze(utext, lexer, formatterIze):
    """Pseudo pygments highlight function,
    but will take use get_tokens_unprocessed from the lexer,
    and will feed the formatIze function of the formatterIze.
    
    (You have noticed that thoses almost-as-in-pygments objects are suffixed
    by Ize, like in pygmentIze.)
    """
    tokensource = lexer.get_tokens_unprocessed(utext)
    formatterIze.formatIze(tokensource)

class BristolFormatter(Formatter):
    """This formatter build an AttributeMap in self.map.
    
    Unlike a standard pygment formatter, it will write nothing in outfile.
    Each pygment token is mapped to bristol text attribute. It's up to the
    interface attribute management to fallback to another attribute if the
    given one is not defined.
    """

    def __init__(self, min_position=None, max_position=None, **options):
        """Initialize the formatter"""
        Formatter.__init__(self, **options)
        self.map = AttributeMap()
        self.min_position = min_position if min_position is not None else 0
        self.max_position = max_position

    def format(self, tokensource, outfile):
        """Fill the AttributeMap using tokensource. Nothing will be writted in
        outfile.
        """
        # will add index before each tokensource, and call formatIze
        # if you can call formatIze directly, it should has better performances.
        tokensourceIze = []
        begin_position = 0
        for token, text in tokensource:
            tokensource.append(begin_position, token, text)
            begin_position += len(text)
        self.formatIze(tokensourceIze)
    
    def formatIze(self, tokensource):
        """Fill the AttributeMap using tokensource.
        Unlike format, tokensource is (index, token, text).
        """
        if self.min_position is not None and self.max_position is not None:
            cleansource = (
                (index, token, text)
                for (index, token, text) in tokensource
                if (index < self.max_position and
                    index + len(text) > self.min_position)
            )
        else:
            cleansource = tokensource
        
        for index, token, text in cleansource:
            start = max(self.min_position, index) - self.min_position
            if self.max_position:
                end = min(self.max_position, index + len(text))
            else:
                end = index + len(text)
            end -= self.min_position
            
            tkstr = repr(token).replace('Token', ATTR_PREFIX).lower()
            self.map[start:end] = tkstr
        
class PygmentizationPlugin(object):
    """Pygment plugin for bristoledit (poweful syntax highlighting)"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
        self.toolkit.config.set_default({
            'load_more_lexer': ([], 
               _("Load lexers stored in your ~/.local/share/bristoledit/")),
        })
        
        # Load pluggable lexers
        for lexmodname in self.toolkit.config['load_more_lexer']:
            # Ugly hack the pygments.lexer.LEXER dict
            try:
                lexmod = __import__(lexmodname)
            except ImportError:
                pass
            else:
                if hasattr(lexmod, '__all__'):
                    for lexername in getattr(lexmod, '__all__'):
                        lexer = getattr(lexmod, lexername)
                        try:
                            LEXERS[lexmodname] = (
                                lexmodname,
                                lexer.name,
                                tuple(lexer.aliases),
                                tuple(lexer.filenames),
                                tuple(lexer.mimetypes)
                            )
                        except:
                            pass
                
        @self.toolkit.hooks(toolkit.Buffer, '_detect_mimetype', self)
        def _detect_mimetype(oldself, olddetect, raw_text=None):
            """Improve the mimetype detection using pygments"""
            res = olddetect(oldself, raw_text)
            if oldself._mimetype == 'text/plain':
                setattr(oldself, '_mimetype', None)
            self.get_lexer(oldself, raw_text) # lexer detection, set mimetype
            return res
        
        # add the export_in_latex function
        @self.toolkit.improve(toolkit.Buffer, 'export_in_latex')
        def export_in_latex(oldself, filename=None):
            """Export the current buffer in latex"""
            if filename is None:
                filename = toolkit.editor.interface._ask_a_filename(
                    _("Export as:") + ' ',
                    oldself._filename + '.tex' if oldself._filename else None
                )
                if not filename:
                    return
            lexer = self.get_lexer(oldself)
            formatter = LatexFormatter(
                linenos=True,
                encoding='utf8',    # yes, hard coding is bad.
                full=True,
                title=str(oldself._filename),
            )
            outfile = open(filename, 'wc')
            highlight(oldself.text, lexer, formatter, outfile=outfile)
            outfile.close()
            return _("Export in LaTeX: done.")
        export_in_latex._userdoc_ = _("Export the current file in LaTeX.")
        
        # add the export_in_html function
        @self.toolkit.improve(toolkit.Buffer, 'export_in_html')
        def export_in_html(oldself, filename=None):
            """Export the current buffer in html"""
            if filename is None:
                filename = toolkit.editor.interface._ask_a_filename(
                    _("Export as:") + ' ',
                    oldself._filename + '.html' if oldself._filename else None
                )
                if not filename:
                    return
            lexer = self.get_lexer(oldself)
            formatter = HtmlFormatter(
                linenos=True,
                encoding=oldself.encoding,
                full=True,
                title=unicode(oldself._filename),
            )
            outfile = open(filename, 'wc')
            highlight(oldself.text, lexer, formatter, outfile=outfile)
            outfile.close()
            return _("Export in HTML: done.")
        export_in_html._userdoc_ = _("Export the current file in HTML.")
    
    def get_lexer(self, buffer, raw_text=None):
        """Return the lexer for buffer"""
        # find a lexer
        lexer = None
        try:
            oldlexer = buffer._lexer
        except AttributeError:
            oldlexer = None
        try:
            retry_times = buffer._lexer_retry
        except AttributeError:
            retry_times = 30 # to have the time to write a python shabang
        guess_text = raw_text if raw_text else buffer.text
        if retry_times > 0 and (not oldlexer or guess_text):
            if buffer and buffer._mimetype and buffer._mimetype!='text/plain':
                # guess with already detected mimetype
                try:
                    lexer = get_lexer_for_mimetype(buffer._mimetype, 
                                                    stripnl=False)
                except ClassNotFound:
                    pass
                else:
                    setattr(buffer, '_lexer', lexer)
                    setattr(buffer, '_lexer_retry', 0)
            if not lexer and buffer and buffer._filename and buffer._mimetype!='text/plain':
                # guess with filename
                try:
                    lexer = guess_lexer_for_filename(buffer._filename,
                                                     guess_text,
                                                     stripnl=False)
                except ClassNotFound:
                    pass
                else:
                    setattr(buffer, '_lexer', lexer)
                    setattr(buffer, '_lexer_retry', 0)
            if not lexer:
                # guess with content
                try:
                    lexer = guess_lexer(guess_text, stripnl=False)
                except ClassNotFound:
                    lexer = TextLexer(stripnl=False)
                else:
                    setattr(buffer, '_lexer', lexer)
                    setattr(buffer, '_lexer_retry', retry_times - 1)
            # dirty hack for python scripts (where the numpy lexer is returned)
            if lexer.name == 'NumPy':
                lexer = get_lexer_by_name('python')
            if lexer.mimetypes:
                buffer._mimetype = lexer.mimetypes[0]
        elif oldlexer:
            lexer = oldlexer
        else:
            lexer = TextLexer(stripnl=False)
        return lexer
    
    def get_map(self, buffer, min_position=None, max_position=None):
        """Return the attribute map for buffer"""
        utext = buffer.text
        # find a lexer
        lexer = self.get_lexer(buffer)
        formatter = BristolFormatter(min_position, max_position)
        highlightIze(utext, lexer, formatter)
        return formatter.map

def start_plugin(toolkit):
    """Start the plugin"""
    return PygmentizationPlugin(toolkit)
