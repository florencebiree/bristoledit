# -*- coding: utf-8 -*-
###############################################################################
#       plugins/undo.py
#       
#       Copyright © 2010-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bristoledit plugin to undo an edition of the text."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2010-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

class TextChange(object):
    """A change in a buffer"""
    
    def __init__(self, tbuffer, text, old_position=None, insert_position=None,
                 delete_position=None, deleted_text=None):
        """Initialize a new text change"""
        self.tbuffer = tbuffer
        self.text = text
        self.old_position = old_position
        self.insert_position = insert_position
        self.delete_position = delete_position
        self.deleted_text = deleted_text
        # attach me to the buffer
        if not hasattr(self.tbuffer, '_undo'):
            self.tbuffer._undo = []
        # do not create a change for the initial set_text
        if insert_position is None and not text:
            return
        self.tbuffer._undo.append(self)
    
    def pack(self):
        """Try to merge this change with the previous, only for alnum chars"""
        if not self.tbuffer._undo or self.tbuffer._undo.index(self) == 0:
            return
        oldchange = self.tbuffer._undo[self.tbuffer._undo.index(self) - 1]
        if (    (oldchange.insert_position is None) or
                (self.insert_position is None)) :
            # full text change -> not packing
            return
        if (    (oldchange.delete_position is None) and
                (self.delete_position is None) and
                (oldchange.text.isalnum()) and
                (self.text.isalnum()) and
                (oldchange.insert_position + len(oldchange.text) ==
                    self.insert_position)):
            # undo a word insertion -> packing
            oldchange.text += self.text
            self.tbuffer._undo.remove(self)
        elif (  (oldchange.delete_position is not None) and
                (self.delete_position is not None) and
                (not oldchange.text) and
                (not self.text) and
                (oldchange.deleted_text.isalnum()) and
                (self.deleted_text.isalnum())):
            # undo a word suppression -> packing
            if (oldchange.delete_position == self.delete_position):
                # oldchange delete the previous character
                oldchange.deleted_text += self.deleted_text
                self.tbuffer._undo.remove(self)
            elif (self.delete_position + 1 == oldchange.delete_position):
                # oldchange delete the next character
                oldchange.deleted_text = (self.deleted_text +
                                          oldchange.deleted_text)
                oldchange.delete_position = self.delete_position
                oldchange.insert_position = self.insert_position
                self.tbuffer._undo.remove(self)
    
    def undo(self):
        """Undo the change I do"""
        # undo the deleted text
        if self.delete_position is not None:
            new_text = self.deleted_text
            insert_position = self.delete_position
        else:
            new_text = ""
            insert_position = 0
        # if all text was replaced
        if self.insert_position is None:
            new_text = self.text
            insert_position = None
            delete_position = None
            delete_len = 0
        else:
            delete_position = self.insert_position
            delete_len = len(self.text)
        # do the undo
        self.tbuffer._set_text(
            new_text,
            self.old_position,
            insert_position,
            delete_position,
            delete_len,
        )
        # force a dirty full refresh
        self.tbuffer._set_text(self.tbuffer.text)
        # remove me from the buffer, and the new change with me
        del(self.tbuffer._undo[-1]) # the dirty full refresh
        del(self.tbuffer._undo[-1]) # the new one
        del(self.tbuffer._undo[-1]) # me

class UndoPlugin(object):
    """Plugin to undo an edition of text"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
        self.toolkit.config.set_default({}, [
            "editor.config.key['^U'] = 'editor.current.undo()'",
        ])
        
        # hooks
        @self.toolkit.hooks(toolkit.Buffer, '_set_text', self)
        def _set_text(oldself, oldset_text, new_text=None, new_position=None,
                      insert_position=None, delete_position=None, delete_len=0):
            """Hooks _set_text to change list of the buffer"""
            if delete_position is not None:
                deleted_text = oldself.text[delete_position:
                                            delete_position + delete_len]
            else:
                deleted_text = None
            if insert_position is None:
                oldtext = oldself.text
            else:
                oldtext = new_text
            old_position = oldself._position
            change = TextChange(oldself, oldtext, old_position, insert_position,
                                delete_position, deleted_text)
            change.pack()
            return oldset_text(oldself, new_text, new_position, insert_position,
                               delete_position, delete_len)
        
        @self.toolkit.improve(toolkit.Buffer, 'undo')
        def undo(oldself):
            """Undo the last change in the buffer"""
            if not hasattr(oldself, '_undo'):
                return
            if oldself._undo:
                oldself._undo[-1].undo()
        undo._userdoc_ = _("Undo the last change")

def start_plugin(toolkit):
    """Start the plugin"""
    return UndoPlugin(toolkit)
