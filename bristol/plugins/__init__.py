# -*- coding: utf-8 -*-
###############################################################################
#       plugins/__init__.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Official bristol plugins package.

Here you can found the plugins:
  * bristol.plugins.addnewline              A plugin to ensure there is a new
                                            line at the end of the file.
  * bristol.plugins.backupcopy              A plugin to save a ~ copy on save.
  * bristol.plugins.ctags                   Use ctags to find definitons.
  * bristol.plugins.dictcompleter           A completer of used words.
  * bristol.plugins.dummy                   Does nothing, only for tests and
                                            example.
  * bristol.plugins.filecompleter           Suggestions when browsing files.
  * bristol.plugins.pygmentization          Syntax highlighting using Pygments.
  * bristol.plugins.pythonshell             A python interpreter.
  * bristol.plugins.pythonshellcompleter    Autocompleter for the python shell
                                            of Bristol.
  * bristol.plugins.shell                   A simple way to acces to shell
                                            commands, pipes, etc.
  * bristol.plugins.smartautoindent         Auto-indent on : or {
  * bristol.plugins.spellchecker            A smart spell checker.
  * bristol.plugins.stripwspaces            Strip whitespaces at the end of line
  * bristol.plugins.tagscompleter           Suggestion using ctags.
  * bristol.plugins.template                Create new files using templates.
  * bristol.plugins.undo                    Allow to cancel a modification.
  * bristol.plugins.wrap                    Wrap lines to 80 characters
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"
