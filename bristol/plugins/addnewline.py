# -*- coding: utf-8 -*-
###############################################################################
#       plugins/addnewline.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Add a new line at the end of the file for Bristoledit."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

class AddNewLinePlugin(object):
    """Make sure there is a new line at the end of the file"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
        @self.toolkit.hooks(toolkit.Buffer, 'save', self)
        def save(oldself, oldsave, filename=None, encoding=None, endofline=None):
            """Make sure there is a new line at the end of the file"""
            if oldself.text and oldself.text[-1] != '\n':
                oldself._set_text('\n', None, len(oldself.text))
            return oldsave(oldself, filename, encoding, endofline)

def start_plugin(toolkit):
    """Start the plugin"""
    return AddNewLinePlugin(toolkit)
