# -*- coding: utf-8 -*-
###############################################################################
#       plugins/stripwspaces.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Official plugin to strip whitespaces at the end of line when saving."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2010-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"


class StripWSpacesPlugin(object):
    """Strip whitespaces at the end of each lines when saving the file"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
        @self.toolkit.hooks(toolkit.Buffer, 'save', self)
        def save(oldself, oldsave, filename=None, encoding=None, endofline=None):
            """Strip whitespaces at the end of lines when saving the file"""
            for lineno in range(oldself._number_of_line()):
                start_of_line = oldself._start_of_line(lineno)
                ctn_line = oldself._get_line(lineno)
                end_of_ctn = len(ctn_line.rstrip())
                wspaces_len = len(ctn_line) - end_of_ctn - 1 #\n
                oldself._set_text('', None,
                    start_of_line + end_of_ctn,
                    start_of_line + end_of_ctn,
                    wspaces_len
                )
            return oldsave(oldself, filename, encoding, endofline)

def start_plugin(toolkit):
    """Start the plugin"""
    return StripWSpacesPlugin(toolkit)
