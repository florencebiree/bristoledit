# -*- coding: utf-8 -*-
###############################################################################
#       plugins/tagscompleter.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Autocompleter based on symbols collected by the ctags plugin"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from bristol.buffer import Buffer
from bristol.utils.hooks import hooks
from bristol.utils.texthacks import END_OF_FILE
from bristol.utils.completion import Suggestion, get_current_token

class TagsCompleter(object):
    """Autocompleter based on symbols collected by the ctags plugin"""
    
    toolkit = None
    
    def __init__(self, editor):
        """Create a new completer object.
        
        editor is the current editor object
        """
        self.editor = editor
        self.toolkit.config.set_default({
            'min length': (2,
                    _(u("Minimum length of a word to propose autocompletion"))),
            'word characters': (u('_$'),
                    _(u("Characters to be considered as a in a word."))),
        })
        self.collector = self.toolkit.editor.plugins.get['ctags']
    
    def suggest(self, src_buffer):
        """Return suggestion results based on the text of src_buffer and its
        current position.
        """
        utext = src_buffer.text
        position = src_buffer._position
        # isolate the current token
        token = get_current_token(utext, position,
                                  self.toolkit.config['word characters'])
        if not token or len(token) < self.toolkit.config['min length']:
            return []
        ftoken = get_current_token(utext, position,
                                    self.toolkit.config['word characters'],
                                    True, True)
        # get the tag file
        tfile = self.collector.tagfile()
        
        _debug_print(repr(tfile.tags()))
        
        # build suggestions
        suggestions = [
            Suggestion(
                ulabel = tagname,
                udesc = None,
                umissing_chars = tagname[len(token):],
                uafter_chars = None,
            )
            for tagname in tfile.tags()
            if tagname.startswith(token)
               and len(tagname) > len(token)
               and tagname != ftoken
        ]
        
        return suggestions

def start_plugin(toolkit):
    """Return the tags completer"""
    if toolkit.depends_loaded(['ctags']):
        TagsCompleter.toolkit = toolkit
        return TagsCompleter
    else:
        return None
