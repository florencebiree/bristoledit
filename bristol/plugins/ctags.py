# -*- coding: utf-8 -*-
###############################################################################
#       plugins/ctags.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Use ctags to collect symbols for Bristoledit.

This plugin is designed for exuberant ctags, to support a lot of languages.
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import weakref
from subprocess import Popen, PIPE
from bristol.utils.completion import get_current_token

CTAGS_CMD = (
    ['ctags-exuberant', '-n', '-f', '-'],
    ['exuberant-ctags', '-n', '-f', '-'],
    ['ctags'          , '-n', '-f', '-'],
)

class Tag(dict):
    """A tag from a tag file"""
    
    def __init__(self, tagline):
        """Parse a tag line (in the extended Vi tags file format)"""
        dict.__init__(self)
        # Detect the line format
        if tagline.startswith('!_TAG_'):
            # nothing interresting here...
            return
        if ';"' in tagline:
            # extended vi tags format
            oldpart, newpart = tagline.split(';"')
        else:
            # old vi format
            oldpart = tagline
            newpart = None
        # Parse the old part
        oldfields = oldpart.split('\t')
        if len(oldfields) == 3:
            self['name'], self['file'], self['address'] = oldfields
            if self['address'].isdigit():
                self['lineno'] = int(self['address'])
        # Parse the new part
        if newpart:
            attrs = [attr.split(':') for attr in newpart.split('\t')]
            for attr in attrs:
                if len(attr) == 1:
                    # tag kind
                    self['kind'] = attr[0]
                else:
                    self[attr[0]] = attr[1]

class TagFile(object):
    """Container of tags"""
    
    def __init__(self, filename=None, istagfile=False, tagfiles=None,
                 linked_buffer=None):
        """Create a tag file, either from a existing file, or by combining
            existing tagfiles
        
        filename can be the path of a text file that will be parsed by ctags,
        or an existing ctags file (in this case, set istagfile to True).
        
        linked_buffer can be a reference to the source buffer.
        """
        self.filename = filename
        self.istagfile = istagfile
        self._tags = {}
        self.tagfiles = None
        self._tag_set = None
        if linked_buffer:
            self.src = weakref.ref(linked_buffer)
        else:
            self.src = None
        if filename:
            self.reload()
        else:
            self.tagfiles = tagfiles
    
    def reload(self):
        """Reload this tag file (and recursively included tag files)"""
        self._tag_set = None
        content = None
        if self.filename:
            if self.istagfile:
                # read the ctags file
                with open(self.filename, 'r') as ctf:
                    content = ctf.read()
            elif os.path.exists(self.filename):
                # run ctags
                for ctags_cmd in CTAGS_CMD:
                    try:
                        ctags = Popen(ctags_cmd + [self.filename], stdout=PIPE,
                                      stderr=open(os.devnull, 'w'))
                    except OSError:
                        pass
                    else:
                        (content, stderr) = ctags.communicate()
                        break
            
            if content is None:
                return
            
            content = content.decode('utf-8', 'ignore')
            
            for line in content.split('\n'):
                t = Tag(line)
                if 'name' in t:
                    t['src'] = self.src
                    tname = t['name']
                    if not tname in self._tags:
                        self._tags[tname] = []
                    self._tags[tname] += [t]
        else:
            for tagfile in self.tagfiles:
                tagfile.reload()
    
    def tags(self):
        """Return the set of tags in this tagfile"""
        if self.filename:
            if not self._tag_set:
                self._tag_set = set()
                self._tag_set = self._tag_set.union(self._tags.keys())
        else:
            need_rebuild = False
            for tagfile in self.tagfiles:
                if not tagfile._tag_set:
                    need_rebuild = True
            if need_rebuild or not self._tag_set:
                self._tag_set = set()
                for tagfile in self.tagfiles:
                    self._tag_set = self._tag_set.union(tagfile.tags())
        return self._tag_set
    
    def __contains__(self, tagname):
        """Return if this tagfile contains the tag tagname"""
        return tagname in self.tags()
    
    def __getitem__(self, tagname):
        """Return the list of tags for tagname"""
        if self.filename:
            return self._tags[tagname]
        else:
            tagv = []
            for tagfile in self.tagfiles:
                if tagname in tagfile:
                    tagv += tagfile[tagname]
            if not tagv:
                raise KeyError(tagname)
            return tagv

class CTagsPlugin(object):
    """Symbol Collector using ctags for Bristoledit"""
    
    def __init__(self, toolkit):
        """Create a new ctags plugin."""
        self.toolkit = toolkit
        
        self.orig_buffer = None
        self.orig_position = None
        self.last_index = 0
        self.last_tkn = None
        self.same_tkn = False
        self.last_results = None
        
        # define default keyboard shortcuts
        self.toolkit.config.set_default({
            'tagfile': (None, _("Load a tag file")),
        }, [
            "editor.config.key['KEY_F(9)'] = 'editor.show_definition()'",
            "editor.config.key['KEY_F(10)'] = 'editor.go_to_definition()'",
        ])
        
        @self.toolkit.hooks(self.toolkit.Buffer, '_open', self)
        def _open(oldself, old_open, filename, encoding=None, endofline=None):
            """Update the ctags list on buffer open"""
            ret = old_open(oldself, filename, encoding, endofline)
            if not hasattr(oldself, '_tags'):
                oldself._tags = TagFile(
                    filename=oldself._filename,
                    linked_buffer=oldself,
                )
            else:
                oldself._tags.reload()
            return ret
        
        @self.toolkit.hooks(self.toolkit.Buffer, 'save', self)
        def save(oldself, oldsave, filename=None, encoding=None,
                 endofline=None):
            """Update the ctags list on buffer save"""
            ret = oldsave(oldself, filename, encoding, endofline)
            if not hasattr(oldself, '_tags'):
                oldself._tags = TagFile(
                    filename=oldself._filename,
                    linked_buffer=oldself,
                )
            else:
                oldself._tags.reload()
            return ret
        
        @self.toolkit.improve(type(self.toolkit.editor), 'show_definition')
        def show_definition(oldself):
            """Show the definition of the current token"""
            results = self.search_def()
            if not results:
                message = _("No definition found.")
            else:
                if self.same_tkn:
                    self.last_index = (self.last_index + 1) % len(results)
                abuffer, lineno = results[self.last_index]
                linectn = abuffer._get_line(lineno)
                message = str(lineno + 1) + ':' + linectn.lstrip()
                if self.toolkit.editor.current is not abuffer:
                    message = (os.path.basename(abuffer._filename) + ':' +
                                message)
            return message
        show_definition._userdoc_ = _("Show the definition of the current "
                                       "symbol.")
        
        @self.toolkit.improve(type(self.toolkit.editor), 'go_to_definition')
        def go_to_definition(oldself):
            """Go to the line and the file of the definition"""
            results = self.search_def()
            if not results:
                if (self.last_results and
                        self.last_index + 1 < len(self.last_results)):
                    # go to the next search result
                    self.last_index = ( (self.last_index + 1) %
                                        len(self.last_results) )
                    abuffer, lineno = self.last_results[self.last_index]
                    abuffer.go_to_line(lineno + 1)
                    if abuffer is not self.toolkit.editor.current:
                        self.toolkit.editor.switch_to(abuffer)
                elif self.orig_buffer is not None:
                    # go to the origine
                    self.orig_buffer._set_text(new_position=self.orig_position)
                    self.toolkit.editor.switch_to(self.orig_buffer)
                    self.orig_buffer = None
                    self.orig_position = None
                else:
                    return _("No definition found.")
            else:
                # save the current position
                self.orig_buffer = self.toolkit.editor.current
                self.orig_position = self.toolkit.editor.current._position
                # go to the definition
                abuffer, lineno = results[0]
                abuffer.go_to_line(lineno + 1)
                if abuffer is not self.toolkit.editor.current:
                    self.toolkit.editor.switch_to(abuffer)
                
        go_to_definition._userdoc_ = _("Go to the definition of the current "
                                         "symbol.")
    
    def search_def(self):
        """Search the definition of the current token
        return a list of (buffer, linenumber)
        """
        cur_tkn = get_current_token(
            self.toolkit.editor.current.text,
            self.toolkit.editor.current._position,
            '_',
            left=True,
            right=True,
        )
        if cur_tkn == self.last_tkn:
            self.same_tkn = True
        else:
            self.same_tkn = False
            self.last_tkn = cur_tkn
            self.last_index = 0
        
        # TODO: priority for the current file
        #buffer_list = [
        #    abuffer._tags for abuffer in self.toolkit.editor.buffers
        #    #if (abuffer is not self.toolkit.editor.current) and
        #    if (hasattr(abuffer, '_tags'))
        #]
        tfile = self.tagfile()
        
        try:
            tags = tfile[cur_tkn]
            if tags:
                self.last_results = [ (tag['src'](), tag['lineno'] - 1)
                    for tag in tags]
                return self.last_results
        except KeyError:
            return None
    
    def tagfile(self):
        """Return a tag file for all opened buffer (and static tags)"""
        # TODO: priority for the current file
        buffer_list = [
            abuffer._tags for abuffer in self.toolkit.editor.buffers
            #if (abuffer is not self.toolkit.editor.current) and
            if (hasattr(abuffer, '_tags'))
        ]
        return TagFile(tagfiles=buffer_list)
    
def start_plugin(toolkit):
    """Return the ctags plugin"""
    return CTagsPlugin(toolkit)
