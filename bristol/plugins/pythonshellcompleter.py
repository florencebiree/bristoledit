# -*- coding: utf-8 -*-
###############################################################################
#       plugins/pythonshellcompleter.py
#       
#       Copyright © 2009-2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Autocompleter plugin for the python shell of Bristoledit."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2023, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import inspect
from functools import reduce

from bristol.utils.texthacks import END_OF_FILE
from bristol.utils.completion import Suggestion, get_current_token

class PythonShellCompleter(object):
    """A python autocompleter based on introspection mechanisims of python"""
    
    def __init__(self, editor, local_env=None):
        """Create a new completer object.
        
        editor is the current editor object
        local_env is a dictionary with local variables.
            (default: {'editor': editor})
        
        The autocompleter will also use __builtins__.
        """
        self.editor = editor
        if local_env:
            self.locals = local_env
        else:
            self.locals = {'editor': editor}
    
    def suggest(self, src_buffer):
        """Return suggestion results based on the text of src_buffer and its
        current position.
        """
        utext = src_buffer.text
        if not utext:
            return []
        position = src_buffer._position
        # isolate the current token
        token = get_current_token(utext, position, '._')
        if not token:
            return []
        # split into objects part
        oparts = token.split('.')
        if len(oparts) == 1:
            # first parts, check in locals and globals
            results = (o for o in list(__builtins__) + list(self.locals)
                       if o.startswith(oparts[0]))
            oparent = None
        else:
            oparent = reduce(
                (lambda oparent, oname: self.get_object(oname, oparent)),
                oparts[:-1],
                None,
            )
            # other part, check in dir(oparent)
            results = (
                o for o in dir(oparent) 
                if (oparts[-1] and o.startswith(oparts[-1])) or 
                   (not oparts[-1] and not o.startswith('_'))
            )
        # build suggestions
        suggestions = [
            Suggestion(
                ulabel = str(self.end_object(result, oparent=oparent)),
                udesc = self.get_desc(result, oparent=oparent),
                umissing_chars = str(self.end_object(
                    result,
                    oparent=oparent,
                    addargspec=False,
                )[len(oparts[-1]):]),
                uafter_chars = str(self.after_object(result, oparent)),
            ) for result in results
        ]
        return suggestions
    
    def get_object(self, oname, oparent=None):
        """Return the object corresponding to the name oname, inside the
        oparent object.
        """
        if oparent is not None and oname:
            try:
                obj = getattr(oparent, oname)
            except AttributeError:
                obj = None
        elif oname in self.locals:
            obj = self.locals[oname]
        elif oname in __builtins__:
            obj = __builtins__[oname]
        else:
            obj = None
        return obj
    
    def end_object(self, oname, oparent=None, addargspec=True):
        """Return the object name completed by '(' for a function, or '.' for an
        object
        
        oparents is parent object from oname is.
        if addargspec, the names of a functions will be completed by the argspec
        of the function
        """
        obj = self.get_object(oname, oparent)
        if obj is None:
            return oname
        elif hasattr(obj, '_argspec_'):
            # already defined argspec (for hooking)
            argspec = getattr(obj, '_argspec_')
            argspec = argspec.replace('oldself, ', '').replace('oldself', '')
            argspec = argspec.replace('self, ', '').replace('self', '')
            if addargspec or argspec == '()':
                oname += argspec
            else:
                oname += '('
        elif inspect.isfunction(obj) or inspect.ismethod(obj):
            # function or module
            argspec = str(inspect.signature(obj))
            argspec = argspec.replace('oldself, ', '').replace('oldself', '')
            argspec = argspec.replace('self, ', '').replace('self', '')
            if addargspec or argspec == '()':
                oname += argspec
            else:
                oname += '('
        elif hasattr(obj, '__call__'):
            # non-python function or method, add '('
            oname += '('
        elif isinstance(obj, list) or isinstance(obj, dict):
            # list or dict, add '['
            oname += '['
        elif type(obj).__name__ not in __builtins__:
            # object, add '.'
            oname += '.'
        elif isinstance(obj, str) or isinstance(obj, bytes):
            # string, add " = '"
            if addargspec:
                oname += " ="
            else:
                oname += " = '"
        elif isinstance(obj, bool):
            # bool, add " = !itsvalue"
            oname += " = "
        else:
            # lambda object, add ' = '
            oname += " = "
        return oname
    
    def after_object(self, oname, oparent=None):
        """Return needed characters to complete the expressinon, like a ')' for
        a function or ']' for a list or a dictionnary.
        
        oparents is parent object from oname is.
        """
        obj = self.get_object(oname, oparent)
        uafter_chars = ''
        if obj is None:
            return ''
        elif hasattr(obj, '_argspec_'):
            # already defined argspec (for hooking)
            argspec = getattr(obj, '_argspec_')
            argspec = argspec.replace('oldself, ', '').replace('oldself', '')
            argspec = argspec.replace('self, ', '').replace('self', '')
            if not argspec == '()':
                uafter_chars = ')'
        elif inspect.isfunction(obj) or inspect.ismethod(obj):
            # function or module
            argspec = str(inspect.signature(obj))
            argspec = argspec.replace('oldself, ', '').replace('oldself', '')
            argspec = argspec.replace('self, ', '').replace('self', '')
            if not argspec == '()':
                uafter_chars = ')'
        elif hasattr(obj, '__call__'):
            # non-python function or method, add '('
            uafter_chars = ')'
        elif isinstance(obj, list) or isinstance(obj, dict):
            # list or dict, add ']'
            uafter_chars = ']'
        elif isinstance(obj, str) or isinstance(obj, bytes):
            # string, add "'"
            uafter_chars += "'"
        elif isinstance(obj, bool):
            uafter_chars += str(not obj)
        return uafter_chars
    
    def get_desc(self, oname, oparent=None):
        """Return the unicode description for the object name
        
        oparents is parent object from oname is.
        If an _userdoc_ attribute exists, it will be used, else it use the
        __doc__attribute, without \n.
        """
        obj = self.get_object(oname, oparent)
        out_encoding = self.editor.interface._screen_charset
        
        if obj is None:
            desc = ''
        elif isinstance(obj, int) or isinstance(obj, float):
            desc = str(obj)
        elif isinstance(obj, str):
            desc = obj.replace('\n', ' ')
        elif isinstance(obj, bytes):
            desc = obj.decode(out_encoding).replace('\n', ' ')
        elif hasattr(obj, '_userdoc_') and getattr(obj, '_userdoc_'):
            desc = getattr(obj, '_userdoc_')
        elif hasattr(obj, '__doc__') and obj.__doc__:
            desc = str(obj.__doc__.replace('\n', ' '))
        else:
            desc = ''
        return desc

def start_plugin(toolkit):
    """Return the python shell completer"""
    return PythonShellCompleter
