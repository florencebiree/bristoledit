# -*- coding: utf-8 -*-
###############################################################################
#       plugins/asyncrun.py
#       
#       Copyright © 2012-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.plugins
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Asynchronous run of programs"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from os import devnull
from subprocess import Popen, PIPE
from threading  import Thread

try:
    from Queue import Queue, Empty
except ImportError:
    from queue import Queue, Empty  # python 3.x

class AProcess(object):
    """Object to represent an asyncronous process"""
    
    def __init__(self, toolkit, args, shell=False):
        """New AProcess: start the process"""
        self.Empty = Empty
        if isinstance(args, str):
            self.cmd = args.split()[0]
        else:
            self.cmd = args[0]
        self.p = Popen(args,
            shell=shell,
            stdin=open(devnull, 'r'),
            stdout=PIPE,
            stderr=toolkit.editor._debug_log,
            bufsize=1,
            close_fds=True, #on posix
        )
        self.q = Queue()
        self.t = Thread(target=self.enqueue_output, args=(self.p.stdout, self.q))
        self.t.daemon = True # thread dies with the program
        self.t.start()
    
    def is_terminated(self):
        """Return None if the program is still running
            return its return code else
        """
        return self.p.poll()
    
    def readline(self):
        """Read a line from stdout (non blocking)
            Raise AProcess.Empty if no data
        """
        return self.q.get_nowait() # or q.get(timeout=.1)
    
    @staticmethod
    def enqueue_output(out, queue):
        for line in iter(out.readline, b''):
            queue.put(line)
        out.close()

class AsyncRunPlugin(object):
    """Asynchronous run of programs"""
    
    def __init__(self, toolkit):
        """Initialize the plugin"""
        self.toolkit = toolkit
        
    def run(self, args, shell=False):
        """Run a program, and return an AProcess object"""
        return AProcess(self.toolkit, args, shell=shell)
        
def start_plugin(toolkit):
    """Start the plugin"""
    return AsyncRunPlugin(toolkit)
