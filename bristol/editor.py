# -*- coding: utf-8 -*-
###############################################################################
#       editor.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bristoledit main object

This module also contain the mecanisisms to handle i18n, and the command line
parameters parser (the main function).
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import sys
import time
from optparse import OptionParser, OptionGroup
from bristol.utils.ubuiltins import isinbuiltins, setbuiltins

# load i18n stuff
APPLICATION = 'bristoledit'
import locale
import gettext
sysprefix = sys.prefix if hasattr(sys, 'prefix') else sys.pypy_prefix #pypy hack
locale_dirs = [os.path.join(prefix, 'share', 'locale')
                 for prefix in (
                     os.path.join(os.path.dirname(sys.argv[0]), os.path.pardir),
                     sysprefix,
                 )]
for ldir in locale_dirs:
    if os.path.exists(ldir):
        gettext.install(APPLICATION, ldir)
        break
if not isinbuiltins('_'):
    setbuiltins('_', lambda text:text)

from .buffer import Buffer, BufferExp
from .config import ConfigurationManager
from .pluginmanager import PluginManager

_instance = None # Will be set to the running instance of Editor (for hacks.py)

class ListBuffer(list):
    """A list of buffers
    
    It seems to be an usual list, but the big difference is that you can set
    a cool user docstring to this list.
    """
    pass

class Editor(object):
    """Main editor for Bristoledit
    
    It should have only one running Editor instance (available as
    editor._instance since instancied).
    
    In a normal Bristoledit startup sequence, the Editor class is passed to
    the interface constructor. The interface instanciate the Editor as it's
    first running step.
    Later in the startup sequence, in the interface main() method, the
    _std_init method of the editor should be called, to open first buffer and
    execute the hacks.py file.
    """
    
    def __init__(self, interface, options=None):
        """Initialize the Bristoledit editor
        
        The editor should be instancied by the interface, which should pass
        itself as parameter to this constructor.
        
        options is a class of command line options.
        Debugging ones:
             options.debug define if logging is enabled
             options.noplugin define if plugins are disabled
             options.miniconf define if a minimal configuration must be enabled 
        
        The initialization process of the editor is:
            1. set globals parameters (debuging, global _instance value)
            2. construct the internal buffer structure
            3. load the configuration manager (and so the configuration file)
            4. load the plugin manager (and so load enabled plugins)
        """
        global _instance
        _instance = self
        self._userdoc_ = _("The Bristoledit editor.")
        self._application = APPLICATION
        self._version = __version__
        self._options = options
        
        if options is None:
            self._debug, self._noplugin, self._miniconf = False, False, False
        else:
            self._debug = options.debug
            self._noplugin = options.noplugin
            self._miniconf = options.miniconf
        
        if self._debug:
            self._debug_log = open(os.path.join(os.path.expanduser('~'),
                                                'bristoledit.log'), 'ab')
            setbuiltins('_debug_print', lambda msg:self._debug_print(msg))
            self._debug_print('[%s] %s' % (__version__, " ".join(sys.argv)))
            self._debug_print('on Python ' + sys.version)
        else:
            self._debug_log = open(os.devnull)
            setbuiltins('_debug_print', lambda msg:None)
        
        self.interface = interface
        self.buffers = ListBuffer()
        self.buffers._userdoc_ = _("List of all buffers.")
        self.current = None
        self.config = ConfigurationManager(self, self._miniconf)
        self.plugins = PluginManager(self)
    
    def _std_init(self, filenames=None):
        """Standard initialization
        
        This method should be called at the end of the initialization of the
        interface, when the interface is ready to display buffer.
        
        If filenames is None, open an empty new buffer.
        Else, open a buffer for each file name in filenames.
        
        This will initialize the current buffer to the first opened buffer, and
        then load the hacks.py file.
        """
        lastmsg = None
        if filenames:
            # open filenames
            for filename in filenames:
                ret = self.open(filename)
                if not isinstance(ret, Buffer):
                    lastmsg = ret
        if not filenames or not self.buffers:
            # create an empty buffer
            self.new()
        # set the current buffer to the first one
        self.switch_to(self.buffers[0])
        # load hacks at the end of init
        self.config._load_hacks()
        if lastmsg:
            return lastmsg
    
    def __repr__(self):
        """Python representation of the editor"""
        return "%s.Editor(%s, filenames=%s)" % (
            self.__module__,
            type(self.interface),
            [buff._filename for buff in self.buffers],
        )

    def __str__(self):
        """Nice representation (in utf-8!) of the editor"""
        return self._userdoc_

    def __unicode__(self):
        """Nice representation of the editor"""
        return self._userdoc_
    
    def new(self):
        """Create a new buffer, switch to it and return it"""
        self.buffers.append(Buffer(self))
        return self.buffers[-1]
    new._userdoc_ = _("Create a new buffer.")
    
    def open(self, filename=None, encoding=None, endofline=None):
        """Open a file in the editor
        
        If filename is None, ask it to the user.
        If encoding is None, try to detect the encoding.
        If endofline is None, try to detect the endofline.
        endofline can be 'LF' (Unix), 'CR' (Mac) or 'CRLF' (Windows)
        
        Returns the newly created buffer.
        
        If there is an existing buffer with the same filename, return it.
        """
        if not filename:
            filename = self.interface._ask_a_filename(
                prompt=_("Open:") + " "
            )
            if not filename:
                return
        filename = os.path.expanduser(filename)
        # Check if there is not another buffer opened for this file
        if os.path.exists(filename):
            for buff in self.buffers:
                if buff._filename is not None:
                    if os.path.samefile(buff._filename, filename):
                        return buff
        
        try:
            self.buffers.append(Buffer(
                self,
                filename,
                encoding=encoding,
                endofline=endofline,
            ))
        except BufferExp as e:
            return e.message
        return self.buffers[-1]
    open._userdoc_ = _("Open a file.")
    
    def switch_to(self, target_buffer):
        """Change the current buffer to target_buffer.
        
        target_buffer can be either a Buffer object or an index in self.buffers
        
        Return the current buffer.
        """
        if type(target_buffer) == int:
            self.current = self.buffers[target_buffer]
        else:
            self.current = self.buffers[self.buffers.index(target_buffer)]
        return self.current
    switch_to._userdoc_ = _("Switch to the given buffer.")
        
    def next(self):
        """Go to the next buffer (by using self.switch_to).
        
        Return the current buffer.
        """
        next_index = (self.buffers.index(self.current) + 1) % len(self.buffers)
        return self.switch_to(next_index)
    next._userdoc_ = _("Go to the next buffer.")
    
    def prev(self):
        """Go to the previous buffer (by using self.switch_to).
        
        Return the current buffer.
        """
        next_index = (self.buffers.index(self.current) - 1) % len(self.buffers)
        return self.switch_to(next_index)
    prev._userdoc_ = _("Go to the previous buffer.")
    
    def close(self, target_buffer=None, keep_a_buffer_open=True):
        """Close a buffer.
        
        If target_buffer is None, close the current buffer.
        If the buffer is the current buffer, change the current buffer to the
        next one.
        If there is no other buffer, create a new one (else if
        keep_a_buffer_open=False).
        """
        buffer_to_del = target_buffer if target_buffer else self.current
        
        # Ask to save if the buffer was modified
        if buffer_to_del._is_modified:
            result = self.interface._ask_a_question(
                _("Do you want to save %s?") %
                    str(buffer_to_del),
                [_('Yes'), _('No')],
            )
            if result == 0:
                buffer_to_del.save()
        
        if len(self.buffers) == 1 and keep_a_buffer_open:
            # if the to-be-closed buffer is the only one, create another buffer.
            self.new()
        
        if self.current is buffer_to_del:
            if keep_a_buffer_open or len(self.buffers) > 1:
                self.prev()
            else:
                self.current = None
        
        self.buffers.remove(buffer_to_del)
        del buffer_to_del
    close._userdoc_ = _("Close the current buffer.")
    
    def exit(self):
        """Close each buffer, exit the editor
        
        The exit is done by calling interface._exit()
        """
        # close buffers
        for buff in reversed(self.buffers[:]):
            self.close(buff, keep_a_buffer_open=False)
        # close debug log
        if self._debug:
            self._debug_log.close()
        # call the interface _exit
        self.interface._exit()
    exit._userdoc_ = _("Quit the editor.")
    
    def _check_events(self):
        """Check events.
        
        This method is called in interface main loops to periodically check
        external event. It's intended to be hooked by plugins that need to
        check events.
        """
        pass
    
    def _debug_print(self, message, raw=False):
        """Print a debugging message in the debug log
        
        The message will be prefixed with the date, and ended with a new line.
        To avoid that, use raw=True
        """
        if not self._debug:
            return
        if not raw:
            self._debug_log.write(
                ('[%s] ' % time.strftime("%H:%M:%S")).encode('utf-8')
            )
        self._debug_log.write(message.encode('utf-8'))
        if not raw:
            self._debug_log.write(b'\n')
        self._debug_log.flush()

def main():
    """Start a new editor, parse arguments, select the right interface"""
    usage = _("Usage: %prog [options] [file1 file2 ...]")
    parser = OptionParser(usage=usage, version="%prog " + __version__)
    parser.add_option("-c", "--curses", dest='curses',
                      action='store_true', default=False,
                      help=_("use the curses interface"))
    parser.add_option("-g", "--gtk", dest='gtk',
                      action='store_true', default=False,
                      help=_("use the GTK+ interface"))
    parser.add_option("--screen-charset", dest='screen_charset',
                      metavar="CHARSET", default=locale.getpreferredencoding(),
                      help=_("set the character set used to display the "
                             "interface (default: %default)"))
    parser.add_option("-k", "--check", dest='checkmode',
                      action='store_true', default=False,
                      help=_("check the configuration file syntax."))
    parser.add_option("-m", "--miniconf", dest='miniconf',
                      action='store_true', default=False,
                      help=_("load a minimal configuration (for "
                      "emergencies only)"))
    # FIXME: this should be a hook in the viewer plugin…
    parser.add_option("-w", "--viewer", dest="viewer",
                      action="store_true", default=False,
                      help=_("open a viewer for files"))
    
    debug_group = OptionGroup(parser, _("Debugging options"),
                    _("Use them only if you want to report bugs or help to "
                    "improve Bristoledit."))
    debug_group.add_option("-d", "--dummy-interface",
                    action='store_true', dest='dummy', default=False,
                    help=_("use the dummy interface"))
    debug_group.add_option("--debug",
                    action='store_true', dest='debug', default=False,
                    help=_("enable debugging informations"))
    debug_group.add_option("--no-plugin",
                    action='store_true', dest='noplugin', default=False,
                    help=_("avoid to load default plugins"))
    parser.add_option_group(debug_group)
    
    (options, args) = parser.parse_args()
    
    # Select the right interface
    if options.curses:
        from bristol.interfaces.curses.main import CursesInterface as Interface
    elif options.gtk:
        from bristol.interfaces.gtk.main import GtkInterface as Interface
    elif options.dummy or options.checkmode:
        from bristol.interfaces.dummy.main import DummyInterface as Interface
    else:
        from bristol.interfaces.curses.main import CursesInterface as Interface
    
    # Init the interface
    interface = Interface(
        Editor,
        filenames=args,
        screen_charset=options.screen_charset,
        options=options,
    )
    
    if not options.checkmode:
        # Run the interface
        interface._main()
    else:
        interface._editor._std_init()
        print(_("Syntax ok."))
