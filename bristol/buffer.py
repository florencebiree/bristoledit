# -*- coding: utf-8 -*-
###############################################################################
#       buffer.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Buffer for bristoledit

    The buffer is the memory representation of an edited file.
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import sys
import locale
import mimetypes
from weakref import ref

from .utils.highlighting import AttributeMap
from .utils.texthacks import detect_end_of_line, decode_end_of_line
from .utils.texthacks import encode_end_of_line, is_text
from .utils.charsetdet import charset_detect

class BufferExp(Exception):
    def __init__(self, message):
        self.message = message
    
    def __repr__(self):
        return self.message

class Buffer(object):
    """Buffer class
    
        The buffer is the memory representation of an edited file.
    """
    
    def __init__(self, editor, filename=None, encoding=None, endofline=None):
        """Initialize a new buffer
        
        if filename is not None, open the corresponding file
        if encoding is None, try to autodetect the encoding
        if endofline is None, try to autodetect the encoding
        """
        # relation with the rest of the world
        self._editor = ref(editor)
        self._config = editor.config._get_component_manager(
            'Buffer', _("Buffer default and specific options:")
        )
        self._config.set_default(DEFAULT_CONFIG)
        
        # core data
        self.__text = ""
        self.__lines = [0]    # the line n begin at the __lines[n] position
                              # in self.__text
        
        # relative data
        self._is_modified = False
        self._position = 0
        self._mark_position = None
        self._filename = None
        
        # user settings
        self._completer = None
        
        self.__mimetype = self._config['mimetype']
        self.__settings = {} # for mime-dependent settings
        self._init_settings()
        
        self._userdoc_ = _("Buffer for %s.") % self._filename
        
        if filename:
            self._open(filename, encoding, endofline)
    
    # Python standard interface
    
    def __repr__(self):
        """Python representation of this buffer"""
        return '%s.Buffer(%s, %s, %s)' % (
            self.__module__,
            self._filename,
            self.encoding,
            self.endofline,
        )
    
    def __unicode__(self):
        """Public representation of the buffer"""
        if self._filename is None:
            return _('No name %d') % self._editor().buffers.index(self)
        else:
            return _("File '%s'") % os.path.basename(self._filename)
    
    def __str__(self):
        """Public representation of the buffer
        
            (Return a ustr/unicode object, for Python 3 compatibility)
        """
        return self.__unicode__()
    
    # Mimetype and user setting system
    
    def _set_mimetype(self, mimetype):
        """Set the mimetype, and reset mime-dependant setting"""
        self.__mimetype = mimetype
        try:
            del self._highlighter
        except AttributeError:
            pass
    _mimetype = property(fget=lambda self:self.__mimetype, fset=_set_mimetype)
    
    def _detect_mimetype(self, raw_text=None):
        """Detect the mimetype of the file"""
        if self._filename:
            (self._mimetype, guess_encoding) = mimetypes.guess_type(
                                                self._filename)
    
    def __get_setting(self, setting):
        return self.__settings.get(setting,
                                    self._config[self._mimetype, setting])
    def __set_setting(self, setting, value):
        self.__settings[setting] = value
    
    def _init_settings(self):
        """Wrap buffer attributes to use settings
        
            Allow to access to the following attributes:
                Buffer.encoding: the encoding of the buffer text
                Buffer.endofline: the end of line of the buffer text
                Buffer.tab_size: the used tab size (in characters)
                Buffer.indent_with_spaces: if indentation must use spaces
                Buffer.auto_indent: if auto_indentation is enabled
        """
        # Do it one time for the whole class
        if hasattr(Buffer, 'encoding'):
            return
        def make_property(setting):
            fget = lambda self: self.__get_setting(setting)
            fset = (lambda self, value : self.__set_setting(setting, value))
            return property(fget, fset)
        Buffer.encoding = make_property('encoding')
        Buffer.endofline = make_property('end of line')
        Buffer.tab_size = make_property('tab size')
        Buffer.indent_with_spaces = make_property('use spaces for tabs')
        Buffer.auto_indent = make_property('auto indent')
    
    # Position management
    
    def _line_of_position(self, position):
        """Return the line number corresponding to position in the text."""
        return len([pos for pos in self.__lines if pos <= position]) - 1
            
    def _index_of_position(self, position):
        """Return the index inside the line of position"""
        startpos = [pos for pos in self.__lines if pos <= position][-1]
        return position - startpos
    
    def _start_of_line(self, lineno):
        """Return the start position of the line lineno."""
        return self.__lines[lineno]
    
    # Text content management
    
    def _number_of_line(self):
        """Return the number of lines of the current buffer"""
        return len(self.__lines)
    
    def _get_line(self, lineno):
        """Return the content of the line lineno.
        
        \n at the end of line are kept.
        """
        startofline = self.__lines[lineno]
        if lineno + 1 >= len(self.__lines):
            endofline = len(self.__text)
        else:
            endofline = self.__lines[lineno + 1]
        return self.__text[startofline:endofline]
    
    def _get_text(self):
        """Return the text"""
        return self.__text
    
    def _set_text(self, new_text=None, new_position=None, insert_position=None,
                 delete_position=None, delete_len=0):
        """Change the text of the buffer.
        
        Full change or atomic change:
            
                If insert_position is None, replace the current text with
            new_text. This is equivalent to `buffer.text = 'something'`. *You
            should avoid this unless you really change the full text*, because
            it may force a force a full refresh of the buffer in interfaces.
                If you just change a part of the text, atomic changes are your
            friends.
        
        Atomic changes:
            
                If insert_position is not None, new_text will be inserted inside
            existing text at the position insert_position.
                If delete_position is not None, delete_len characters will be
            removed, from the position delete_position. This is done before any
            insert operation, so it safe to replace characters with one atomic
            change.
        
        Cursor position:
            
                If new_position is not None, change the current position inside
                the text to new_position.
        """
        # if asked, delete some characters
        if delete_position is not None and delete_len > 0:
            self.__text = (
                self.__text[:delete_position] + 
                self.__text[delete_position + delete_len:]
            )
            # sync __lines
            dline0 = self._line_of_position(delete_position)
            dline1 = self._line_of_position(delete_position + delete_len)
            startpos = self._start_of_line(dline0)
            self.__lines = (
                self.__lines[:dline0] +
                [ startpos ] +
                [ (spos - delete_len) for spos in self.__lines[dline1 + 1:] ]  
            )
            
        if insert_position is None and new_text is not None:
            # full replacement
            self.__text = new_text
            # sync _lines
            self.__lines = self.__lines_of_text(self.__text)
        elif new_text:
            # insertion
            self.__text = (
                self.__text[:insert_position] +
                new_text +
                self.__text[insert_position:]
            )
            # sync _lines
            iline = self._line_of_position(insert_position)
            iidx = self._index_of_position(insert_position)
            startpos = self._start_of_line(iline)
            ilen = len(new_text)
            new_ctn = self.__text[startpos:startpos + iidx + ilen]
            self.__lines = (
                self.__lines[:iline] +
                self.__lines_of_text(new_ctn, startpos) +
                [ (spos + ilen) for spos in self.__lines[iline+1:]]
            )
        
        if new_text or delete_len:
            self._is_modified = True
        if new_position is not None:
            self._position = new_position
    text = property(fget=_get_text) # fset cannot be hooked
    
    @staticmethod
    def __lines_of_text(text, start=0):
        """Utility method for _set_text, to build part of __lines"""
        lines = []
        for line in text.split('\n'):    # .splitlines strip trailing \n
            lines.append(start)
            start += len(line) + 1
        return lines
    
    # I/O management
    
    def _open(self, filename, encoding=None, endofline=None):
        """Open a file in this buffer
        
        If encoding is None, try to detect the encoding.
        If endofline is None, try to detect the endofline
        endofline can be 'LF' (Unix), 'CR' (Mac) or 'CRLF' (Windows)
        """
        if not isinstance(filename, str):
            filename = filename.decode(sys.getfilesystemencoding())
        if os.path.isdir(filename):
            self._filename = None
            raise BufferExp(_('"%s" is a directory...') % filename)
        
        filename = os.path.expanduser(filename)
        if os.path.exists(filename):
            try:
                reader = open(filename, 'rb')
            except IOError:
                raise BufferExp(_('Error while reading the file'))
            else:
                raw_text = reader.read()
                reader.close()
        else:
            raw_text = None
        
        # check for binary files...
        if raw_text and not is_text(raw_text):
            raise BufferExp(
                _('The file "%s" is a binary file and cannot be opened.') %
                filename
            )
        
        self._filename = filename
        
        if encoding:
            self.encoding = encoding
        elif raw_text:
            self.encoding = charset_detect(raw_text)
        if not self.encoding or self.encoding.lower() == 'ascii':
            self.encoding = self._config[self._mimetype, 'encoding']
        
        if endofline:
            self.endofline = endofline
        elif raw_text:
            self.endofline = detect_end_of_line(raw_text)
        else:
            self.endofline = self._config[self._mimetype, 'end of line']
        
        if raw_text:
            # decode raw_text
            raw_text = decode_end_of_line(raw_text, self.endofline)
            try:
                decoded_text = raw_text.decode(self.encoding, 'replace')
            except UnicodeDecodeError: #, err:
                # -> unicode(err)
                raise BufferExp(
                    _("Charset error. Try editor.current.reload('utf-8')")
                )
        
        self._detect_mimetype(self.text)
        if self._mimetype is None:
            self._mimetype = self._config['mimetype']
            
        # set text
        if raw_text:
            self._set_text(decoded_text)
            self._is_modified = False

    
    def reload(self, encoding=None, endofline=None):
        """Reload the file from the disk
        
        If encoding is None, try to detect the encoding.
        If endofline is None, try to detect the endofline
        endofline can be 'LF' (Unix), 'CR' (Mac) or 'CRLF' (Windows)
        """
        if self._filename:
            if self._is_modified:
                result = self._editor().interface._ask_a_question(
                    _('Do you want to lose your unsaved modifications '
                        'in "%s"?') % self._filename,
                    (_('Yes'), _('No')),
                )
                if result == 1:
                    return
            try:
                self._open(self._filename, encoding, endofline)
            except BufferExp as e:
                return e.message
            self._is_modified = False
    reload._userdoc_ = _("Reload the file from the disk.")
    
    def save(self, filename=None, encoding=None, endofline=None):
        """Write this buffer in a file
        
        If filename is None, use the last filename, if possible
        If encoding is None, use the last or default encoding.
        If endofline is None, use the last or default endofline
        endofline can be 'LF' (Unix), 'CR' (Mac) or 'CRLF' (Windows)
        """
        if not filename and not self._filename:
            return self.save_as()
        
        if encoding:
            self.encoding = encoding
        try:
            raw_text = self.text.encode(self.encoding)
        except UnicodeEncodeError:
            return (
                _("The file can not be saved in %s. Set another charset.") %
                self.encoding
            )
        if endofline:
            self.endofline = endofline
        raw_text = encode_end_of_line(raw_text, self.endofline)
        
        if filename:
            filename = os.path.expanduser(filename)
            if not isinstance(filename, str):
                filename = filename.decode(sys.getfilesystemencoding())
            self._filename = filename
        
        try:
            writer = open(self._filename, 'wb')
        except IOError: #, err
            return _('Error while writting the file') 
            #err.strerror.decode(self._editor().interface.screen_charset)
        else:
            writer.write(raw_text)
            writer.close()
            self._is_modified = False
        
        self._detect_mimetype()
        if self._mimetype is None:
            self._mimetype = self._config['mimetype']
    save._userdoc_ = _("Save the current file.")
    
    def save_as(self, filename=None, encoding=None, endofline=None):
        """Save the buffer with a mandatory filename
        
        If filename is None, it will be asked to the user through interface.
        If encoding is None, use the last or default encoding.
        If endofline is None, use the last or default endofline
        endofline can be 'LF' (Unix), 'CR' (Mac) or 'CRLF' (Windows)
        """
        if not filename:
            filename = self._editor().interface._ask_a_filename(
                _("Save as:") + " ",
                self._filename,
            )
            if not filename:
                return
        return self.save(
            filename,
            encoding,
            endofline,
        )
    save_as._userdoc_ = _("Save the current file under another name.")
    
    # Indentation management
    
    def indent(self, position=None, delta=0, ref=-1, normalize=False):
        """Set the indent of the line of position, to use the same indent len
        of the relative line ref, plus delta indentation level.
        
        Return the len of the new indent of the line.
        
        If position is None, use the current position.
        If normalize is True, replace existing indent with the choiced indent
            character (this is always done for delta < 0).
        """
        if position is None:
            position = self._position
        cur_line = self._line_of_position(position)
        position_inside = (cur_line == self._line_of_position(self._position))
        # get the current indent
        start_of_line = self._start_of_line(cur_line)
        cur_indent = ''
        cur_line_content = self._get_line(cur_line)
        for i in range(len(cur_line_content)):
            if cur_line_content[i] in ' \t':
                cur_indent += cur_line_content[i]
            else:
                break
        # get the ref indent
        if ref == 0:
            last_indent = cur_indent
        elif cur_line + ref >= 0:
            last_indent = ''
            last_line = self._get_line(cur_line + ref)
            for i in range(len(last_line)):
                if last_line[i] in ' \t':
                    last_indent += last_line[i]
                else:
                    break
        else:
            last_indent = ''
        # compute the new indent
        new_indent = last_indent
        if delta > 0 and not normalize:
            if self.indent_with_spaces:
                new_indent += ' ' * (self.tab_size * delta)
            else:
                new_indent += '\t' * delta
        elif delta < 0 or normalize:
            new_len = self._indent_len(last_indent) + delta
            if self.indent_with_spaces :
                new_indent = ' ' * (self.tab_size * new_len)
            else:
                new_indent = '\t' * new_len
        # insert the indent
        if position_inside:
            new_position = start_of_line + len(new_indent)
        else:
            new_position = None
        self._set_text(new_indent, new_position, start_of_line,
                      start_of_line, len(cur_indent))
        return len(new_indent)
    indent._userdoc_ = _("Indent the current line.")
        
    def _indent_len(self, indent_str):
        """Comput a (may be approximative) indent len from an indent string"""
        return (indent_str.count('\t') +
            ((indent_str.count(' ') // self.tab_size) +
            (1 if indent_str.count(' ') % self.tab_size else 0))
        )
    
    def normalize_indent(self):
        """Change indents to always use the current indentation character"""
        for lineno in range(self._number_of_line()):
            position = self._start_of_line(lineno)
            self.indent(position, delta=0, ref=0, normalize=True)
    normalize_indent._userdoc_ = _("Change indent to use the same character")
    
    # Attribute maps
    
    def _get_attribute_map(self, min_position=None, max_position=None):
        """Return an attribute map using the configured highlighter
        
        If min_position and max_position are given, return a map betwenn thoses
        positions.
        """
        try:
            highlighter = self._highlighter
        except AttributeError:
            # try to load an highlighter plugin
            highlighter_id = self._config[self._mimetype, 'syntax highlighter']
            if highlighter_id:
                self._editor().plugins.load(highlighter_id)
            try:
                highlighter = self._editor().plugins[highlighter_id]
            except KeyError:
                highlighter = None
            self._highlighter = highlighter
        if not highlighter:
            attr_map = AttributeMap()
            if min_position is not None and max_position is not None:
                attr_map[0:max_position-min_position] = 'editor_style'
            else:
                attr_map[0:len(self.text)] = 'editor_style'
            return attr_map
        else:
            return highlighter.get_map(
                self,
                min_position = min_position,
                max_position = max_position,
            )
    
    # Auto completion system
    
    def _get_completer(self):
        """Return (if possible) a suitable bristoledit completer object
        
        This follow user settings, and try to load the matching plugin.
        """
        if self._completer is None :
            compl_plugin = self._config[self._mimetype, 'autocompleter']
            if compl_plugin and self._editor().plugins.load(compl_plugin):
                try:
                    completer = self._editor().plugins[compl_plugin]
                except KeyError:
                    completer = None
                self._completer = completer(self._editor)
        return self._completer
    
    # Line management
    
    def go_to_line(self, lineno=None):
        """Go to the given line
        
        If lineno is None, ask it to the user.
        """
        if lineno is None:
            lineno = int(self._editor().interface._ask(_("Go to the line: ")))
        if lineno == 0:
            lineno = 1
        elif lineno < 0:
            lineno = self._number_of_line() + lineno + 1
        if lineno:
            new_pos = self._start_of_line(lineno - 1)
            # move the cursor with a set_text
            self._set_text('', new_pos, self._position)
    go_to_line._userdoc_ = _("Go to the given line.")

DEFAULT_CONFIG = {
    'mimetype': ('text/plain', _("Default mime type for new files")),
    'encoding': (locale.getpreferredencoding(),
                    _('Default encoding for saving files')),
    'end of line': ('LF',
                   _("Default end of line for new files (LF, CR or CRLF).")),
    'syntax highlighter': ('pygmentization',
                    _("Plugin to provide syntax highlighting or None")),
    'tab size': (4, _('Length of a tabulation')),
    'use spaces for tabs': (False,
                                _('Insert spaces instead of a tabulation')),
    'auto indent': (True, _('Auto indent on new line')),
    'autocompleter': ('statcompleter',
                                    _('Plugin to use for autocompletion')),
    # language specific configuration - python
    'text/x-python': {
        'tab size': 4,
        'use spaces for tabs': True,
    },
    # language specific configuration - makefile
    'text/x-makefile': {
        'use spaces for tabs': False,
    },
}
