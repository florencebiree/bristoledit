# -*- coding: utf-8 -*-
###############################################################################
#       config.py
#       
#       Copyright © 2009-2024, Florence Birée <florence@biree.name>
#       
#       This file is a part of bristol
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Configuration manager for Bristoledit

The configuration manager allow each component of Bristoledit (a core part,
an interface, or a plugin) to register default settings, and then to retreive
the value of a specific setting.

The configuration manager also handle the hacks.py file, which is used to
provide changes in the editor interface after the load of everything. It is
used for keybindings, for example.
A component can also register default hacks.

To get its component manager, a component must call the `_get_component_manager`
of the configuration manager.
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2024, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import sys
import importlib.util
import importlib.machinery
from xdg.BaseDirectory import save_config_path

from .buffer import Buffer
from .utils.hooks import hooks

class ComponentManager(object):
    """Configuration manager for a specific component"""
    
    def __init__(self, component_id, config, component_doc=None):
        """Initialize a new component manager
            
            component_id is the identifier (the name) of the dict in the file
            config is the configuration manager
            component_doc is the help string for the component
        """
        self.component_id = component_id
        self.config = config
        self.component_doc = component_doc
        self.default = {}
        self.hacks = []
    
    def set_default(self, default, hacks=None, directives=None):
        """Set the default values of this component
            
            default is a dictionnary, where each value may be a tuple of 
                (value, comment).
                A key can also be a dictionnary, for subsections.
                
                Example:
                default={
                    'mysetting1': 42,
                    'mysetting2': ('spam', 'Important setting!'),
                    'subsection': {
                        'mysetting1': 12,
                    },
                }
                
            values must only use base python class (ie no import in the file)
            
            directives is a liste of python functions wich will be applyed on
            the settings dictionnary in the config file.
            
            hacks is a list of python statement wich will be written in the
                hacks.py file if this is the first run of this component.
        """
        self.default = default
        self.directives = directives
        if hacks:
            self.hacks = hacks
        # if there is no data relative to this component in the config file:
        #     white defaults
        if (not self.config._module or 
            not hasattr(self.config._module, self.component_id)):
                self.config._flush(self)
    
    def _hasdict(self, subsection=None):
        """Return if the dictionnary exists in the config file
        
        If subsection is not None, return if the subsection exists.
        """
        return (
            self.config._module and
            hasattr(self.config._module, self.component_id) and
            getattr(self.config._module, self.component_id) is not None and
                (not subsection or
                subsection in getattr(self.config._module, self.component_id))
        )
    
    def _getdict(self, subsection=None):
        """Return the dictionnary corresponding to the component in the
        config file.
        
        Return None if the dictionnary doesn't exists.
        
        If subsection is not None, return the subsection if it exists.
        
        """
        if self._hasdict(subsection):
            cdict = getattr(self.config._module, self.component_id)
            if subsection:
                if subsection in cdict:
                    return cdict[subsection]
                else:
                    return None
            else:
                return cdict
        else:
            return None
    
    def __getitem__(self, item_name):
        """Return the value of the item
        
            cm.__getitem__[name] <==> cm[name]
        
            You can also use:
        
            cm[name, subitem] to query the value of `subitem` in the
            subsection `name`.
        
        """
        if isinstance(item_name, tuple):
            item_name, subitem = item_name
        else:
            subitem = None
        # Try to find a subitem in the config file
        if subitem and self._hasdict(item_name) :
            subdict = self._getdict(item_name)
            if subitem in subdict:
                return subdict[subitem]
        # Try to find a subitem in the defaults values
        if subitem and item_name in self.default:
            if subitem in self.default[item_name]:
                return self.default[item_name][subitem]
        # Fall back to search the subitem at top level
        if subitem:
            item_name = subitem
        # Try to find the item in the config file
        cdict = self._getdict()
        if cdict and item_name in cdict:
            return cdict[item_name]
        # Try to find the item in the defaults values
        if item_name in self.default:
            val = self.default[item_name]
            if isinstance(val, tuple):
                val = val[0]
            return val
        else:
            return None
    
    def getdoc(self, item_name):
        """Return the documentation of this item"""
        val = self.default[item_name]
        if not isinstance(val, tuple) or len(val) < 2:
            return ""
        else:
            return val[1]
    
    def __repr__(self):
        """Bytes string representation of the component"""
        return (self.__unicode__()).encode('utf-8')
    
    def __str__(self):
        """String representation of the component
        
        This produce the Python code will be written to the config file.
        
        Warning: return an ustr/unicode string! (we are using upy)
        """
        return self.__unicode__()
    
    def __unicode__(self):
        """String representation of the component
        
        This produce the Python code will be written to the config file.
        """
        def ppdict(cdict, indent):
            """Helper function to pretty print a dictionnary"""
            if not cdict:
                return ""
            ppstr = "\n"
            for key in cdict:
                val = cdict[key]
                if isinstance(val, tuple):
                    vrepr = repr(val[0]) + ",\n"
                    if len(val) < 2:
                        comm = None
                    else:
                        comm = val[1]
                elif isinstance(val, dict):
                    vrepr = "{"
                    vrepr += ppdict(val, indent + 4)
                    vrepr += (" " * indent) + "},\n"
                    comm = None
                else:
                     vrepr = repr(val) + ",\n"
                     comm = None
                if comm:
                    ppstr += (" " * indent) + "# " + comm + "\n"
                ppstr += (" " * indent) + repr(key) + ": " + vrepr
            return ppstr
        ppstr = ""
        if self.component_doc:
            ppstr += "# %s\n" % self.component_doc
        ppstr += "%s = {" % self.component_id
        ppstr += ppdict(self.default, 4)
        ppstr += "}\n"
        if self.directives:
            ppstr += '\n'.join('%s(%s)' % (dirname, self.component_id)
                                    for dirname in self.directives)
            ppstr += '\n'
        ppstr += '\n'
        return ppstr

class ConfigurationManager(object):
    """Main configuration manager for Bristoledit"""
    
    def __init__(self, editor, minimal=False):
        """Initialize the configuration of Bristoledit
        
        This will load the configuration file, start the keybinding mecanisism,
        and hooks Buffer.save() to reload configuration after editing.
        
        Rememeber to call _load_hacks() at the end of the startup.
        
        If minimal, the backup configuration files will be loaded instead of
        the user's ones.
        """
        self._editor = editor
        self._userdoc_ = _("Manage the configuration of Bristoledit.")
        self._config_dir = save_config_path(self._editor._application)
        if not os.path.isdir(self._config_dir):
            os.mkdir(self._config_dir)
        self._config_file = os.path.join(self._config_dir, 'config.py')
        self._module = None
        self._hacks_file = os.path.join(self._config_dir, 'hacks.py')
        self._hacks = None
        self._minimal = minimal
        
        # Load the python module, if available
        self._load()
        
        # Configuration for keyboard shortcuts
        self.key = {}
        self._key_comp = self._get_component_manager('keyBindings',
            _("Key bindings options (see hacks.py for bindings)"))
        self._key_comp.set_default({}, DEFAULT_KEYBINDINGS)
        
        @hooks(Buffer, 'save', self)
        def save(oldself, oldsave, filename=None, encoding=None,
                 endofline=None):
            """Save the buffer, and if its the config file, reload the config"""
            ret = oldsave(oldself, filename, encoding, endofline)
            if oldself._filename and os.path.exists(oldself._filename):
                if os.path.samefile(oldself._filename, self._config_file):
                    self._load()
                elif os.path.samefile(oldself._filename, self._hacks_file):
                    self._load_hacks()
            return ret
    
    __str__ = lambda self: self._userdoc_
    __unicode__ = __str__

    def _load(self):
        """Try to load the user configuration file"""
        if self._minimal:
            from .settings import minimal_config
            self._module = minimal_config
            return
        try:
            sys.path.insert(0, self._config_dir)
            #(mfile, pname, desc) = imp.find_module('config') #, [self._config_dir])
            config_specs = importlib.util.find_spec('config')
        except ImportError:
            self._module = None
        else:
            try:
                #self._module = imp.load_module('config', mfile, pname, desc)
                self._module = importlib.util.module_from_spec(config_specs)
                config_specs.loader.exec_module(self._module)
            except:
                import traceback
                traceback.print_exc(limit=0, file=sys.stderr)
                print(_("Run bristoledit with --miniconf to fix the problem."),
                       file=sys.stderr)
                sys.exit(1)
            #finally:
            #    mfile.close()
    
    def _load_hacks(self):
        """Try to load the user hacks file
        
            This must be done at the end of the startup (ie after the end of
            plugin and interface loading).
        """
        if self._minimal:
            from .settings import minimal_hacks
            self._module = minimal_hacks
            return
        try:
            #(mfile, pname, desc) = imp.find_module('hacks', [self._config_dir])
            #hacks_specs = importlib.util.find_spec('hacks', self._config_dir)
            hacks_specs = importlib.machinery.PathFinder().find_spec('hacks', [self._config_dir])
        except ImportError:
            self._hacks = None
        else:
            try:
                #self._hacks = imp.load_module('hacks', mfile, pname, desc)
                self._hacks = importlib.util.module_from_spec(hacks_specs)
                hacks_specs.loader.exec_module(self._hacks)
            except:
                #import traceback
                #traceback.print_exc(limit=0, file=sys.stderr)
                print(_("Run bristoledit with --miniconf to fix the problem."),
                       file=sys.stderr)
                raise
            #finally:
            #    mfile.close()
        
    def edit(self):
        """Open the configuration file in the editor"""
        self._editor.open(self._config_file, 'utf-8')
    edit._userdoc_ = _("Edit the configuration file.")
    
    def hacks_edit(self):
        """Open the hacks file in the editor"""
        self._editor.open(self._hacks_file, 'utf-8')
    hacks_edit._userdoc_ = _("Edit the hacks file.")
    
    def bind(self, command, key):
        """Bind a command with a key.
        
        command must be a string that will feed the python shell.
        key must be a curses.keyname-style representation of a key.
        """
        self.key[key] = command
        self._flush_hacks(
            ["editor.config.key[%s] = %s" % (repr(key), repr(command))],
            _("user defined key binding")
        )
    bind._userdoc_ = _("Bind a key with a command")
    
    def _get_component_manager(self, component_id, component_doc=None):
        """Return a component configuration manager for component_id"""
        return ComponentManager(component_id, self, component_doc)
    
    def _flush(self, component):
        """Append to the config file the default values of the component"""
        cf_exist = os.path.exists(self._config_file)
        with open(self._config_file, 'ab') as cf:
            if not cf_exist:
                cf.write(CF_HEADER.encode('utf-8'))
            #cf.write(CF_AUTO.encode('utf-8'))
            cf.write(str(component).encode('utf-8'))
        self._load()
        # Same thing with the hacks file
        if not component.hacks:
            return
        self._flush_hacks(component.hacks, component.component_id)
    
    def _flush_hacks(self, hacks, component_id):
        hf_exist = os.path.exists(self._hacks_file)
        with open(self._hacks_file, 'ab') as hf:
            if not hf_exist:
                hf.write(HF_HEADER.encode('utf-8'))
            hf.write((HF_COMP_HEADER % component_id).encode('utf-8'))
            hf.write(("\n".join(hacks)).encode('utf-8'))
            hf.write("\n\n".encode('utf-8'))
    
CF_HEADER =  ("# -*- encoding: utf-8 -*-\n" +
              "# " + _("Bristoledit configuration file") + "\n" +
              "# " + _("This file is a true Python file, take care!") + "\n" +
              "\n" +
              "from bristol.settings.color_default import color_scheme\n" +
              "#from bristol.settings.color_retro import color_scheme\n" +
              "#from bristol.settings.color_desert import color_scheme\n" +
              "\n")
CF_AUTO =  ("# " + 
            _("The next section has been automatically added by Bristoledit.") +
            "\n")

HF_HEADER =  ("# -*- encoding: utf-8 -*-\n" +
              "# " + _("Bristoledit hacks file") + "\n" +
              "# " + _("This file is loaded after plugins.") + "\n" +
              "# " + _("You can use it to customize the editor behavior.") +
                                                                       "\n" +
              "# " + _("This file is a true Python file, take care!") + "\n" +
              "from bristol.editor import _instance as editor\n" +
              "from bristol.utils.hooks import Definer\n" +
              "define = Definer(editor.interface._exec)\n\n")
             

HF_COMP_HEADER = "# " + _("Hacks for %s:") + "\n"

DEFAULT_KEYBINDINGS = [
    # Editor keybindings
    "editor.config.key['^N'] = 'editor.new()'",
    "editor.config.key['^O'] = 'editor.open()'",
    "editor.config.key['^Q'] = 'editor.exit()'",
    "editor.config.key['^W'] = 'editor.close()'",
    "editor.config.key['^@'] = 'editor.next()' # Control-space",
    "editor.config.key['M-f'] = 'editor.next()'",
    "editor.config.key['M-d'] = 'editor.prev()'",
    # Buffer keybindings
    "editor.config.key['^S'] = 'editor.current.save()'",
    "editor.config.key['^B'] = 'editor.current.auto_indent = "
                                "not editor.current.auto_indent'",
    "editor.config.key['^L'] = 'editor.current.go_to_line()'",
    # Interface main keybindings
    "editor.config.key['^T'] = 'editor.interface.shell_mode()'",
    "editor.config.key['^Z'] = 'editor.interface.suspend()'",
    # Interface text editor keybindings
    "editor.config.key['KEY_RIGHT'] = 'editor.interface.current.go_right()'",
    "editor.config.key['KEY_LEFT'] = 'editor.interface.current.go_left()'",
    "editor.config.key['KEY_DOWN'] = 'editor.interface.current.go_down()'",
    "editor.config.key['KEY_UP'] = 'editor.interface.current.go_up()'",
    "editor.config.key['KEY_HOME'] = 'editor.interface.current.go_home()'",
    "editor.config.key['KEY_END'] = 'editor.interface.current.go_end()'",
    "editor.config.key['^A'] = 'editor.interface.current.go_home()'",
    "editor.config.key['^E'] = 'editor.interface.current.go_end()'",
    "editor.config.key['KEY_NPAGE'] = 'editor.interface.current.go_down(one_page=True)'",
    "editor.config.key['KEY_PPAGE'] = 'editor.interface.current.go_up(one_page=True)'",
    "editor.config.key['M-Oc'] = 'editor.interface.current.go_right(one_word=True)'",
    "editor.config.key['M-Od'] = 'editor.interface.current.go_left(one_word=True)'",
    "editor.config.key['M-^R'] = 'editor.interface.current.go_right(one_word=True)'",
    "editor.config.key['M-^C'] = 'editor.interface.current.go_left(one_word=True)'",
    "editor.config.key['kRIT5'] = 'editor.interface.current.go_right(one_word=True)'",
    "editor.config.key['kLFT5'] = 'editor.interface.current.go_left(one_word=True)'",
    "editor.config.key['KEY_IC'] = 'editor.interface.current.insert = not editor.interface.current.insert'",
    "editor.config.key['^Y'] = 'editor.interface.current.vertedit = not editor.interface.current.vertedit'",
    "editor.config.key['KEY_BACKSPACE'] = 'editor.interface.current.delch()'",
    "editor.config.key['^H'] = 'editor.interface.current.delch(one_word=True)'",
        # 0x7f -> delch()
    "editor.config.key['KEY_DC'] = 'editor.interface.current.delch(before=False)'",
    "editor.config.key['kDC5'] = 'editor.interface.current.delch(before=False, one_word=True)'",
    "editor.config.key['^D'] = 'editor.interface.current.delch(before=False)'",
    "editor.config.key['^K'] = 'editor.interface.current.delch(step=-1, before=False)'",
    "editor.config.key['^P'] = 'editor.interface.current.toogle_mark()'",
    "editor.config.key['^C'] = 'editor.interface.current.copy()'",
    "editor.config.key['^X'] = 'editor.interface.current.cut()'",
    "editor.config.key['^V'] = 'editor.interface.current.paste()'",
        # vi-like keys
    "editor.config.key['M-j'] = 'editor.interface.current.go_down()'",
    "editor.config.key['M-J'] = 'editor.interface.current.go_down(one_page=True)'",
    "editor.config.key['M-k'] = 'editor.interface.current.go_up()'",
    "editor.config.key['M-K'] = 'editor.interface.current.go_up(one_page=True)'",
    "editor.config.key['M-h'] = 'editor.interface.current.go_left()'",
    "editor.config.key['M-l'] = 'editor.interface.current.go_right()'",
    "editor.config.key[u'M-^R'] = 'editor.interface.current.go_right(one_word=True)'",
    "editor.config.key[u'M-^T'] = 'editor.interface.current.go_left(one_word=True)'",
]
