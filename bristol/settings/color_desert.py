# -*- coding: utf-8 -*-
###############################################################################
#       settings/color_desert.py
#       
#       Copyright © 2011-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.settings
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Desert color scheme for Bristoledit

    If you want to make your own color scheme, copy this file in your
    ~/.config/bristoledit and make you changes.
    
    Then, just import it with the name of the module in your config.py.

"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2011-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

# color names
default = 'default'
white = 'white'
black = 'black'
orange = '9'
cyan = 'cyan'
green = '45'
lightgreen = '40'
lightgrey = '80'
lightred = '48'
lightorange = '68'

# background of the window
#bg = lightgrey      # desert background
bg = default        # transparent or terminal-default background

# format constants
F = '%s+%s+%s'
n = 'n'     # normal
r = 'r'     # reverse
b = 'b'     # bold
i = 'i'     # italic
u = 'u'     # underline

_colors = {
                                    #  attr foreground  background
    # === interface ===
    'editor_style':                 F % (n, default,    bg),
    'command_style':                F % (n, lightgreen, black),
    'compl_style':                  F % (n, white,      lightgrey),
    'help_style':                   F % (n, lightgreen, black),
    '80char_style':                 F % (n, default,    lightgrey),
    'reverse':                      F % (n, lightgreen, black),
    
    # === syntax highlighting ===
    
    # language keywords
    'code_style.keyword':           F % (n, lightorange, bg),
    #'code_style.operator':          F % (n, lightred, bg),
    'code_style.name.tag':          F % (n, lightorange, bg),
    
    # comments
    'code_style.comment':           F % (n, cyan, bg),
    
    # constants
    'code_style.literal.number':    F % (n, orange, bg),
    'code_style.literal.string':    F % (n, orange, bg),
    
    # names
    'code_style.name.class':        F % (n, green, bg),
    'code_style.name.function':     F % (n, green, bg),
    'code_style.name.builtin':      F % (n, green, bg),
    'code_style.name.exception':    F % (b, green, bg),
    'code_style.name.entity':       F % (n, green, bg),
    
    # generic format
    'code_style.generic.emph':      F % (i, green, bg),
    'code_style.generic.strong':    F % (b, green, bg),
    
    # === spell checker ===
    'code_style.generic.badword':   F % (u, default, bg),
}

def color_scheme(ci):
    ci.update(_colors)
    
