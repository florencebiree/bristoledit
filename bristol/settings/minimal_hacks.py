# -*- coding: utf-8 -*-
###############################################################################
#       settings/minimal_hacks.py
#       
#       Copyright © 2011-2020, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.settings
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bristoledit minimal hacks file

    This file intend to provide a minimal configuration for Bristoledit in
    emergency cases.

"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2011-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from bristol.editor import _instance as editor
from bristol.utils.hooks import Definer
define = Definer(editor.interface._exec)

# Hacks for keyBindings:
editor.config.key['^N'] = 'editor.new()'
editor.config.key['^O'] = 'editor.open()'
editor.config.key['^Q'] = 'editor.exit()'
editor.config.key['^W'] = 'editor.close()'
editor.config.key['^@'] = 'editor.next()' # Control-space
editor.config.key['M-f'] = 'editor.next()'
editor.config.key['M-d'] = 'editor.prev()'
editor.config.key['^S'] = 'editor.current.save()'
editor.config.key['^B'] = 'editor.current.auto_indent = not editor.current.auto_indent'
editor.config.key['^L'] = 'editor.current.go_to_line()'
editor.config.key['^T'] = 'editor.interface.shell_mode()'
editor.config.key['^Z'] = 'editor.interface.suspend()'
editor.config.key['KEY_RIGHT'] = 'editor.interface.current.go_right()'
editor.config.key['KEY_LEFT'] = 'editor.interface.current.go_left()'
editor.config.key['KEY_DOWN'] = 'editor.interface.current.go_down()'
editor.config.key['KEY_UP'] = 'editor.interface.current.go_up()'
editor.config.key['KEY_HOME'] = 'editor.interface.current.go_home()'
editor.config.key['KEY_END'] = 'editor.interface.current.go_end()'
editor.config.key['^A'] = 'editor.interface.current.go_home()'
editor.config.key['^E'] = 'editor.interface.current.go_end()'
editor.config.key['KEY_NPAGE'] = 'editor.interface.current.go_down(one_page=True)'
editor.config.key['KEY_PPAGE'] = 'editor.interface.current.go_up(one_page=True)'
editor.config.key['M-Oc'] = 'editor.interface.current.go_right(one_word=True)'
editor.config.key['M-Od'] = 'editor.interface.current.go_left(one_word=True)'
editor.config.key['KEY_IC'] = 'editor.interface.current.insert = not editor.interface.current.insert'
editor.config.key['^Y'] = 'editor.interface.current.vertedit = not editor.interface.current.vertedit'
editor.config.key['KEY_BACKSPACE'] = 'editor.interface.current.delch()'
editor.config.key['^H'] = 'editor.interface.current.delch()'
editor.config.key['KEY_DC'] = 'editor.interface.current.delch(before=False)'
editor.config.key['^D'] = 'editor.interface.current.delch(before=False)'
editor.config.key['^K'] = 'editor.interface.current.delch(step=-1, before=False)'
editor.config.key['^P'] = 'editor.interface.current.toogle_mark()'
editor.config.key['^C'] = 'editor.interface.current.copy()'
editor.config.key['^X'] = 'editor.interface.current.cut()'
editor.config.key['^V'] = 'editor.interface.current.paste()'
editor.config.key['M-j'] = 'editor.interface.current.go_down()'
editor.config.key['M-J'] = 'editor.interface.current.go_down(one_page=True)'
editor.config.key['M-k'] = 'editor.interface.current.go_up()'
editor.config.key['M-K'] = 'editor.interface.current.go_up(one_page=True)'
editor.config.key['M-h'] = 'editor.interface.current.go_left()'
editor.config.key['M-l'] = 'editor.interface.current.go_right()'
editor.config.key[u'M-^R'] = 'editor.interface.current.go_right(one_word=True)'
editor.config.key[u'M-^T'] = 'editor.interface.current.go_left(one_word=True)'

# Hacks for PluginShell:
editor.config.key['M-t'] = 'editor.command()'
define[editor.current].search = 'editor.current.pipe("grep -n '", "'")'
define[editor.current].replace = 'editor.current.backpipe("sed \'s/", "//g\'")'
define[editor].compile = '! make || ant'
editor.config.key['^F'] = 'editor.current.search()'
editor.config.key['^R'] = 'editor.current.replace()'
editor.config.key['KEY_F(3)'] = 'editor.current.next_result()'
editor.config.key['^G'] = 'editor.current.next_result()'
editor.config.key['KEY_F(5)'] = 'editor.compile()'

# Hacks for PluginUndo:
editor.config.key['^U'] = 'editor.current.undo()'

