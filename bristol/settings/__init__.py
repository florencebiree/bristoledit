# -*- coding: utf-8 -*-
###############################################################################
#       settings/__init__.py
#       
#       Copyright © 2011-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.settings
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Settings package for Bristoledit.

Here you can found the modules:
  * bristol.settings.minimal_config     minimal configuration file in case of
                                        error in the user's one.
  * bristol.settings.minimal_hacks      minimal hacks file in case of error in
                                        the user's one.
  * bristol.settings.color_default      default color scheme
  * bristol.settings.color_retro        color scheme of bristoledit 0.1
  * bristol.settings.color_desert       desert color scheme.
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2011-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"
