# -*- coding: utf-8 -*-
###############################################################################
#       settings/minimal_config.py
#       
#       Copyright © 2011-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.settings
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bristoledit minimal configuration file

    This file intend to provide a minimal configuration for Bristoledit in
    emergency cases.

"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2011-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from bristol.settings.color_default import color_scheme

# Key bindings options (see hacks.py for bindings)
keyBindings = {}

# Plugins management system:
PluginManager = {
    # List of plugins to be loaded at the startup
    'enabled': ['shell', 'pygmentization', 'undo', 'addnewline', 'backupcopy'],
}

# Configuration for the plugin shell:
PluginShell = {}

# Configuration for the plugin pygmentization:
PluginPygmentization = {
    # Load lexers stored in your ~/.local/share/bristoledit/
    'load_more_lexer': [],
}

# Configuration for the plugin undo:
PluginUndo = {}

# Curses interface configuration:
CursesInterface = {
    # Draw a line at column 80 (max width of standard terminals.
    'show 80 chars line': True,
    # Highlight tabs to avoid confusing with spaces.
    'show tabs': True,
    # Use xclip (if installed) to access to the X11 clipboard
    'use X11 clipboard': True,
    # Enable mouse support.
    'use mouse': False,
}
color_scheme(CursesInterface)

# Buffer default and specific options:
Buffer = {
    # Default mime type for new files
    'mimetype': 'text/plain',
    # Insert spaces instead of a tabulation
    'use spaces for tabs': False,
    # Length of a tabulation
    'tab size': 4,
    'text/x-python': {
        'use spaces for tabs': True,
        'tab size': 4,
    },
    # Default encoding for saving files
    'encoding': 'UTF-8',
    # Plugin to provide syntax highlighting or None
    'syntax highlighter': 'pygmentization',
    # Plugin to use for autocompletion
    'autocompleter': 'statcompleter',
    # Auto indent on new line
    'auto indent': True,
    'text/x-makefile': {
        'use spaces for tabs': False,
    },
    # Default end of line for new files (LF, CR or CRLF).
    'end of line': 'LF',
}

