# -*- coding: utf-8 -*-
###############################################################################
#       settings/color_retro.py
#       
#       Copyright © 2011-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.settings
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Color scheme of Bristoledit 0.1

    For an more customizable example of color scheme, see color_desert.py

"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2011-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

_colors = { 
    # === interface ===
    'editor_style': 'n+default+default',
    'command_style': 'b+white+green',
    'compl_style': 'b+cyan+default',
    'help_style': 'n+white+green',
    '80char_style': 'n+default+red',
    'reverse': 'r+default+default',
    
    # === syntax highlighting ===
    
    # language keywords
    'code_style.keyword': 'b+yellow+default',
    'code_style.operator': 'b+default+default',
    'code_style.name.tag': 'n+blue+default',
    
    # comments
    'code_style.comment': 'n+cyan+default',
    
    # constants
    'code_style.literal.number': 'b+cyan+default',
    'code_style.literal.string': 'b+magenta+default',
    
    # names
    'code_style.name.variable': 'n+cyan+default',
    'code_style.name.builtin': 'b+yellow+default',
    'code_style.name.constant': 'n+cyan+default',
    'code_style.name.function': 'b+red+default',
    'code_style.name.class': 'b+red+default',
    'code_style.name.attribute': 'n+cyan+default',
    'code_style.name.entity': 'b+magenta+default',
    'code_style.name.exception': 'b+red+default',
    
    # generic format
    'code_style.generic.emph': 'i+green+default',
    'code_style.generic.strong': 'b+default+default',
    
    # === spell checker ===
    'code_style.generic.badword': 'n+default+red',
}

def color_scheme(ci):
    ci.update(_colors)
    
