# -*- coding: utf-8 -*-
###############################################################################
#       utils/completion.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.utils
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bases classes for completion tools."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from .texthacks import char_size

class Suggestion(object):
    """A suggestion object for the autocompletion"""
    
    def __init__(self, ulabel, udesc=None, umissing_chars=None,
                 uafter_chars=None, replace=None):
        """Make a new suggestion object
        
        ulabel is the unicode text which is the suggested text
        udesc it the unicode description of the object
        umissing_chars is the unicode text to add to the current position to
            complete the current text
        uafter_chars it the unicode text to add after the current position
        replace is a method that get a buffer and do what it want. replace will
            only be used if umissing_chars is None.
        """
        self.ulabel = ulabel
        self.udesc = udesc if udesc else ''
        self.umissing_chars = umissing_chars
        self.uafter_chars = uafter_chars
        self.replace = replace
    
    def __repr__(self):
        """Return the internal representation of this suggestion"""
        return '<Suggestion u"%s">' % str(self.ulabel)
    
    def __unicode__(self):
        """Return the unicode representation of this object"""
        return self.ulabel
    
    def __str__(self):
        """Return the string representation of this object"""
        return self.ulable
    
    def starts_like(self, suggestion):
        """Return the common starting parts of umissing_chars between self and
            suggestion.
        
        suggestion can either be another suggestion or an unicode text
        
        Return '' if there is no umissing_chars in one of both.
        """
        if self.umissing_chars:
            my_chars = self.umissing_chars
        else:
            return ''
        if type(suggestion) == Suggestion and suggestion.umissing_chars:
            o_chars = suggestion.umissing_chars
        elif isinstance(suggestion, str):
            o_chars = suggestion
        else:
            return ''
        
        commonpart = ''
        for c1, c2 in zip(my_chars, o_chars):
            if c1 == c2:
                commonpart += c1
            else:
                break
        return commonpart

def get_current_token(utext, position, other_chars=None, left=True, right=False):
    """Return the current token (word) from a text and at the left of position.
    
    Characters are consired as in the word if they are in letters or digit, or
    in other_chars
    If right, the right part of the position will also be append.
    """
    if other_chars is None:
        other_chars = ''
    token = ''
    if left:
        try:
            for i in range(position-1, -1, -1):
                if utext[i].isalnum() or utext[i] in other_chars:
                    token = utext[i] + token
                else:
                    break
        except IndexError:
            pass
    if right:
        for i in range(position, len(utext)):
            if utext[i].isalnum() or utext[i] in other_chars:
                token += utext[i]
            else:
                break
    return token

def tokenize(utext, tab_size, more_spaces=None):
    """Return a list of token (words) and spaces.
    
    The returned list is under the form
        [(0, 'token1', True, 6), (7, '    ', False, 4),],
    where the first element is the start position of the token, and the third
    element in the tupple is True for a word, and False for a space.
    The fourth element is the token size, with tab expanded.
    
    Spaces are ' ', '\t', '\n' or characters in more_spaces.
    """
    if more_spaces is None:
        more_spaces = ''
    token = ''
    tklist = []
    is_word = None
    startpos = 0
    chsize = char_size(utext, tab_size)
    tksize = 0
    for i in range(len(utext)):
        if not (utext[i].isspace() or utext[i] in more_spaces):
            # word
            if is_word is not None and not is_word:
                # save spaces, change state
                tklist.append((startpos, token, False, tksize))
                token = ''
                startpos = i
                tksize = 0
            is_word = True
            token += utext[i]
            tksize += chsize[i]
        else:
            # space
            if is_word is not None and is_word:
                # save word, change state
                tklist.append((startpos, token, True, tksize))
                token = ''
                startpos = i
                tksize = 0
            is_word = False
            token += utext[i]
            tksize += chsize[i]
    # save last token
    if is_word is not None:
        tklist.append((startpos, token, is_word, tksize))
    return tklist
