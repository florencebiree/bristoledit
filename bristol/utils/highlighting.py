# -*- coding: utf-8 -*-
###############################################################################
#       utils/highlighting.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.utils
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Bases classes for syntax highlighting tools."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from itertools import chain, tee

from .intervalmap import intervalmap

class AttributeMap(intervalmap):
    """An attribute map object for syntax highlighting
    
    Its map a bristoledit text attribute to positions of a text.

    You can set an attribute using attrmap[start:end] = attr
    
    You can retreive the attribute for one position using attrmap[pos].
    You can retreive the list of attributes in the map using attrmap.items()
        which will return a generator of ((start, end), attr)

    AttributeMap only contain bristol attributes, it's up to the interface to
        convert it to font and color attributes.
        
    A character position is in range(begin_position, end_position) ie include in
    begin_position, but not in end_position.
    """
    
    def submap(self, min_position, max_position):
        """Return an AttributeMap in range(min_position, max_position).

        All positions will be shifted to have min_position = 0
        """
        attrm = AttributeMap()
        for ((start, end), attr) in self.items():
            if (start <= max_position and end >= min_position):
                start = max(min_position, start) - min_position
                end = min(max_position, end) - min_position
                attrm[start:end] = attr
        return attrm
    
    def split(self, text, sep='\n'):
        """Split the AttributeMap in a corresponding list to the splitted text.
        
        text is the reference text.
        separator is the separator used to split the text.
        """
        # get two generator of separators
        startgen, endgen = tee((i + 1 for i, c in enumerate(text) if c == sep))
        # build a generator of submaps
        return (
            self.submap(start, end)
            for (start, end) in zip(
                chain((0,), startgen),      # start position generator
                chain(endgen, (len(text),)) # end position generator
            )
        )

