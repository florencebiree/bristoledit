# -*- coding: utf-8 -*-
###############################################################################
#       utils/__init__.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.utils
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Utils package for bristol.

Here you can found the modules:
  * bristol.utils.charsetdet        Character set detection tools
  * bristol.utils.completion        Classes to manage the completion system
  * bristol.utils.highlighting      Tools to help syntax highlighting
  * bristol.utils.hooks             Hooking sys., to extend existing functions
  * bristol.utils.intervalmap       A useful data structure for interval maps
  * bristol.utils.texthacks         Various functions to manage piece of text
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"
