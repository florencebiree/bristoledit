# -*- coding: utf-8 -*-
###############################################################################
#       utils/charsetdet.py
#       
#       Copyright © 2010-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.utils
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Character set detection tools"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2010-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import re
import codecs
import chardet

# regexp to regonize python/emacs/vi encoding declaration style
# (see: <http://www.python.org/dev/peps/pep-0263/>)
# and xml declaration style, and html http-equiv meta tags
CHARSETDEC = re.compile(r'(?:coding|charset)[:=]\s*"?([-\w.]+)')

def charset_detect(raw_text):
    """Try to find a suitable chaset for raw_text.
    
    If the encoding detection fails, return None.
    
    The two step of charset detection:
    
        First lookup for a charset declaration in the file (recognized
    declarations: python/emacs/vi-style, xml declaration and http-equiv html
    meta tags).
    
        If that fails, ask the chardet module to infer the charset by looking
    the used special characters.
    """
    encoding = None
    # first step : python/emacs/vi/xml/html declarations
    # for Python 3, we need to decode raw_text from ascii, ignoring unknown
    # characters
    chardec_search = CHARSETDEC.search(raw_text.decode('ascii', 'ignore'))
    if chardec_search:
        encoding = chardec_search.group(1)
        try:
            codecs.lookup(encoding)
        except LookupError:
            # bad result
            encoding = None
    # second step : try chardet
    if not encoding:
        encoding = chardet.detect(raw_text)['encoding']
    return encoding
