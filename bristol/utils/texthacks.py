# -*- coding: utf-8 -*-
###############################################################################
#       utils/texthacks.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.utils
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Various functions to manage piece of text."""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import string

# Text position constants
END_OF_FILE = -1
END_OF_LINE = -1

def detect_end_of_line(rawtext):
    """Return the kind of end of line found in rawtext (bytes).
    
    An end of line can be: 'LF' (Unix), 'CR' (Mac) or 'CRLF' (Windows).
    """
    if b'\r\n' in rawtext:
        return 'CRLF'
    elif b'\r' in rawtext:
        return 'CR'
    else:
        return 'LF'

def decode_end_of_line(rawtext, eol_from):
    """Change all eol_from in rawtext (bytes) to LN"""
    if eol_from.upper() == 'CRLF':
        return rawtext.replace(b'\r\n', b'\n')
    elif eol_from.upper() == 'CR':
        return rawtext.replace(b'\r', b'\n')
    else:
        return rawtext

def encode_end_of_line(rawtext, eol_to):
    """Change all LN in rawtext (bytes) to eol_to"""
    if eol_to.upper() == 'CRLF':
        return rawtext.replace(b'\n', b'\r\n')
    elif eol_to.upper() == 'CR':
        return rawtext.replace(b'\n', b'\r')
    else:
        return rawtext

def char_size(utext, tab_size, offset=0):
    """Return the list of character size of utext"""
    csize = []
    for c in utext:
        if c == '\t':
            csize.append(tab_size - (offset % tab_size))
            offset += csize[-1]
        elif c == '\n':
            csize.append(1)
            offset = 0
        else:
            csize.append(1)
            offset += 1
    return csize

def tab_len(utext, tab_size, offset=0):
    """Return the length of utext (unicode text), where one tabulation count
        for its size in 'normal' characters.
    """
    return sum(char_size(utext, tab_size, offset))

def tabbed(utext, tab_size, offset=0):
    """Expand tabs in utext by using tab stops with tab_size
    
    If utext is doesn't start at the begining of the text, use offset
    to set the number of characters at the begining.
    """
    return ''.join(
        char if char != '\t' else ' ' * width
        for char, width in zip(utext, char_size(utext, tab_size, offset))
    )

def is_text(blob):
    """Return if the blob (bytes string) is a text or a binary file
    
        Inspired from the code of Andrew Dalke
        <http://code.activestate.com/recipes/173220/>
    """
    valid_bytes = bytes(range(32, 127)) + b"\n\r\t\b"
    #_null_trans = string.maketrans(b"", b"")
    
    if b"\0" in blob:
        return False
    
    if not blob:  # Empty files are considered text
        return True
    
    # Get the non-text characters (maps a character to itself then
    # use the 'remove' option to get rid of the text characters.)
    #t = blob.translate(_null_trans, text_bytes)
    nbnont = len([c for c in blob if not bytes((c,)) in valid_bytes])
    
    # If more than 30% non-text characters, then
    # this is considered a binary file
    if nbnont / len(blob) > 0.30:
        return False
    return True
