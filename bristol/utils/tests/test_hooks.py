# -*- coding: utf-8 -*-
###############################################################################
#       utils/tests/test_hooks.py
#       
#       Copyright © 2010, Florian Birée <florian@biree.name>
#       
#       This file is a part of bristol.utils
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Unit tests for Bristoledit hooking system"""

import unittest
import os
import sys

# fix import path
dirname = os.path.dirname(__file__)
if dirname == '':
    dirname = '.'
dirname = os.path.realpath(dirname)
updir = os.path.split(dirname)[0]
if updir not in sys.path:
    sys.path.append(updir)

# tests

from hooks import hooks, improve

class HooksTest(unittest.TestCase):
    
    def test_hooks(self):
        """Test hooking"""
        class C:
            def f(self, x):
                return x
        
        c = C()
        d = C()
        
        self.assertEquals(c.f(2), 2)
        
        @hooks(C, 'f', self)
        def newf(oldself, oldf, x):
            return oldf(oldself, x + 40)
        
        self.assertEquals(c.f(0), 40)
        self.assertEquals(d.f(1), 41)
        e = C()
        self.assertEquals(e.f(2), 42)

class ImproveTest(unittest.TestCase):
    
    def test_improve(self):
        class C:
            pass
        
        c = C()
        d = C()
        
        self.assertEquals(hasattr(c, 'f'), False)
        self.assertEquals(hasattr(d, 'f'), False)
        
        @improve(C, 'f')
        def newf(cobj, x):
            return x + 40
        
        self.assertEquals(hasattr(c, 'f'), True)
        self.assertEquals(hasattr(d, 'f'), True)
        
        self.assertEquals(c.f(0), 40)
        self.assertEquals(d.f(1), 41)
        e = C()
        self.assertEquals(e.f(2), 42)

class DefinerTest(unittest.TestCase):
    
    def test_define(self):
        class C:
            pass
        c = C()
        
        runner = lambda vals: ('ThisIsOk:' + vals)
        define = Definer(runner)
        
        define[c].f = 'my code'
        
        self.assertEquals(c.f(), 'ThisIsOk:my code')
        
        d = C()
        
        self.assertEquals(d.f(), 'ThisIsOk:my code')


# test suite
def test_suite():
    tests = [ unittest.makeSuite(HooksTest), unittest.makeSuite(ImproveTest),
        unittest.makeSuite(ImproveTest)]
    return unittest.TestSuite(tests)

if __name__ == '__main__':
    unittest.main(defaultTest='test_suite')
