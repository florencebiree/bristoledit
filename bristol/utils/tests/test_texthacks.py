# -*- coding: utf-8 -*-
###############################################################################
#       utils/tests/test_texthacks.py
#       
#       Copyright © 2010, Florian Birée <florian@biree.name>
#       
#       This file is a part of bristol.utils
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Unit tests for Bristoledit test hacks"""

import unittest
import os
import sys

# fix import path
dirname = os.path.dirname(__file__)
if dirname == '':
    dirname = '.'
dirname = os.path.realpath(dirname)
up = lambda adir: os.path.split(adir)[0]
updir = up(up(up(dirname)))
if updir not in sys.path:
    sys.path.append(updir)

from bristol.utils.upy import *

# tests
from bristol.utils.texthacks import detect_end_of_line, decode_end_of_line
from bristol.utils.texthacks import encode_end_of_line, tab_len, is_text
from bristol.utils.texthacks import tabbed

class TextHacksTest(unittest.TestCase):
    
    def test_detect_end_of_line(self):
        wintxt = b'one line\r\nsecond line'
        self.assertEquals(detect_end_of_line(wintxt), 'CRLF')
        mactxt = b'one line\rsecond line'
        self.assertEquals(detect_end_of_line(mactxt), 'CR')
        txt = b'one line\nsecond line'
        self.assertEquals(detect_end_of_line(txt), 'LF')
    
    def test_decode_end_of_line(self):
        txt = b'one line\nsecond line'
        wintxt = b'one line\r\nsecond line'
        self.assertEquals(decode_end_of_line(wintxt, 'CRLF'), txt)
        mactxt = b'one line\rsecond line'
        self.assertEquals(decode_end_of_line(mactxt, 'CR'), txt)
        self.assertEquals(decode_end_of_line(txt, 'LF'), txt)
    
    def test_encode_end_of_line(self):
        txt = b'one line\nsecond line'
        wintxt = b'one line\r\nsecond line'
        self.assertEquals(encode_end_of_line(txt, 'CRLF'), wintxt)
        mactxt = b'one line\rsecond line'
        self.assertEquals(encode_end_of_line(txt, 'CR'), mactxt)
        self.assertEquals(encode_end_of_line(txt, 'LF'), txt)
    
    def test_tabbed(self):
        txt =  '    t	three'
        txt4 = '    t   three'
        txt3 = '    t three'
        self.assertEquals(tabbed(txt, 4), txt4)
        self.assertEquals(tabbed(txt, 3), txt3)
    
    def test_tab_len(self):
        txt = 'one    two	three'
        self.assertEquals(tab_len(txt, 4), 17)
        self.assertEquals(tab_len(txt, 3), 17)
    
    def test_is_text(self):
        nullblob = b'ceci est un\0blob'
        self.assertEquals(is_text(nullblob), False)
        emptyblob = b''
        self.assertEquals(is_text(emptyblob), True)
        binblob = b('\1\2\3blob\4\5\6')
        self.assertEquals(is_text(binblob), False)
        textblob = 'bobéçcoincoin'.encode('utf-8')
        self.assertEquals(is_text(textblob), True)

# test suite
def test_suite():
    tests = [ unittest.makeSuite(TextHacksTest)]
    return unittest.TestSuite(tests)

if __name__ == '__main__':
    unittest.main(defaultTest='test_suite')
