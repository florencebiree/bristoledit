# -*- coding: utf-8 -*-
###############################################################################
#       utils/tests/test_charsetdet.py
#       
#       Copyright © 2010, Florian Birée <florian@biree.name>
#       
#       This file is a part of bristol.utils
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Unit tests for Bristoledit charset detection system"""

import unittest
import os
import sys

# fix import path
dirname = os.path.dirname(__file__)
if dirname == '':
    dirname = '.'
dirname = os.path.realpath(dirname)
up = lambda adir: os.path.split(adir)[0]
updir = up(up(up(dirname)))
if updir not in sys.path:
    sys.path.append(updir)

from bristol.utils.upy import *

# tests
from bristol.utils.charsetdet import charset_detect

class CharsetDetTest(unittest.TestCase):
    
    def test_charset_detect_python_dec(self):
        txt = b'# -*- encoding: latin1 -*-'
        self.assertEquals(charset_detect(txt), 'latin1')
        txt = b'# -*- encoding: utf-8 -*-'
        self.assertEquals(charset_detect(txt), 'utf-8')
        txt = b'# -*- encoding: utf-8 -*-\n# \xc3\xa9'
        self.assertEquals(charset_detect(txt), 'utf-8')
    
    def test_charset_detect_xml_dec(self):
        txt = b'<?xml version="1.0" encoding="windows-1252"?>\n<tag></tag>\n'
        self.assertEquals(charset_detect(txt), 'windows-1252')
    
    def test_charset_detect_html_dec(self):
        txt = b'<html><head>'
                '<meta http-equiv="Content-Type" content="text/html; '
                    'charset=UTF-8" />'
                '</head><body>spam</body></html>'
        self.assertEquals(charset_detect(txt), 'UTF-8')
    
    # we do not test chardet

# test suite
def test_suite():
    tests = [ unittest.makeSuite(CharsetDetTest) ]
    return unittest.TestSuite(tests)

if __name__ == '__main__':
    unittest.main(defaultTest='test_suite')
