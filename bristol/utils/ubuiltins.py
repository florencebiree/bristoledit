# -*- coding: utf-8 -*-
###############################################################################
#       utils/ubuiltins.py
#       
#       Copyright © 2010-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.utils
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Builtins manipulation functions, for any python version

usage:
    from bristol.utils.ubuiltins import isinbuiltins, setbuiltins
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2010-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

def isinbuiltins(name):
    """Return if there is an object name in __builtins__
    
    This function work either if __builtins__ is a dict or a module, either if
    it is called from a module or from a top level script, either with CPython
    or Pypy.
    """
    try:
        return name in __builtins__
    except TypeError:
        return hasattr(__builtins__, name)

def setbuiltins(name, value):
    """Set the builtin name to value
    
    This function work either if __builtins__ is a dict or a module, either if
    it is called from a module or from a top level script, either with CPython
    or Pypy.
    """
    try:
        __builtins__[name] = value
    except TypeError:
        setattr(__builtins__, name, value)
