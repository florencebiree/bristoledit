# -*- coding: utf-8 -*-
###############################################################################
#       utils/hooks.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.utils
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Hooking system, to extend existing functions in Bristoledit.

This module provide various way to modify the behaviour of existing parts of
Bristoledit.

You can use them when writting plugins, or directly in your hacks.py.
They are also used in interfaces, to hook interface event on internal changes.

    * hooks allow to override an existing method
    * improve add a method to an existing class
    * Definer is a magic artefact used in hacks.py to define new methods to
                an existing class, only from one instance of it.

"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import inspect

def hooks(target_class, method, hooker):
    """
    Decorator to replace target_class.method (where method is the string of the
    name) by the decorated function.
    
    The decorated function must have the same parameters than
    target_class.method, more the first parameters which is an instance of
    target_class, and the second which is the hooked method.
    
    You should call the hooked method in your method, and you should always
    return its results unless you know what else to return.
    
    Use this decorator like:
    
        >>> class C(object):
        ...     def f(self, x):
        ...         return x
        ...
        >>> class Hooker(object):
        ...     def set_hook(self):
        ...         @hooks(C, 'f', self)
        ...         def newf(oldself, oldf, x):
        ...             return oldf(oldself, x + 42)
        ... 
        >>> H().set_hook()
    
    """
    def wrap(newmethod):
        """Bind the newmethod to target class, and keep the old somewhere"""
        if not hasattr(hooker, '__hooks'):
            hooker.__hooks = {}
        mid = str(id(getattr(target_class, method)))   # ensure that the hooker
                                                       # doesn't overwrite
                                                       # another method
        hooker.__hooks[method + '_' + mid] = getattr(target_class, method)
        
        def wrapped_method(oldself, *args, **kwargs):
            """Execute the wrapped method"""
            return newmethod(oldself, hooker.__hooks[method + '_' + mid],
                             *args, **kwargs)
        
        # preserve user docstrings
        if hasattr(getattr(target_class, method), '_userdoc_'):
            setattr(wrapped_method, '_userdoc_',
                    getattr(getattr(target_class, method), '_userdoc_'))
        
        # save/preserve initial arg spec
        sig = inspect.signature(getattr(target_class, method))
        setattr(wrapped_method, '__signature__', sig)
        #if hasattr(getattr(target_class, method), '_argspec_'):
        #    setattr(wrapped_method, '_argspec_',
        #            getattr(getattr(target_class, method), '_argspec_'))
        #else:
        #    (args, varargs, varkw, defaults) = inspect.getfullargspec(
        #            getattr(target_class, method))[:4]
        #    setattr(wrapped_method, '_argspec_',
        #                inspect.formatargspec(args, varargs, varkw, defaults)
        #                                        #, unicode)
        #    )
        # replace the old method
        setattr(target_class, method, wrapped_method)
        return wrapped_method
    return wrap

def improve(target_class, method):
    """Decorator to create the method target_class.method (where method is the
    string of the name) with the decorated function.
    
    The first parameter of the decorated function will be an instance of
    target_class.
    
    Use this decorator like:
    >>> class C(object):
    ...     pass
    ...
    >>> class Hooker(object):
    ...     def hook(self):
    ...         @improve(C, 'f')
    ...         def newf(cobj, x):
    ...             cobj.v = x + 1
    ... 
    >>> H().hook()
    """
    def wrap(newmethod):
        """Bind the newmethod to target class"""
        def wrapped_method(oldself, *args, **kwargs):
            """Execute the wrapped method"""
            return newmethod(oldself, *args, **kwargs)
        # save the argspec
        sig = inspect.signature(newmethod)
        setattr(wrapped_method, '__signature__', sig)
        # improve the class
        setattr(target_class, method, wrapped_method)
        return wrapped_method
    return wrap

class _MethSetter(object):
    """A wrapper around a class to define a method from a string"""
    
    def __init__(self, runner, aclass):
        """Initialize the method setter"""
        object.__setattr__(self, 'runner', runner)
        object.__setattr__(self, 'aclass', aclass)
    
    def __setattr__(self, name, value):
        """Add a method named name"""
        runner = self.runner
        setattr(self.aclass, name, lambda self: runner(value))
    

class Definer(object):
    """Magic artefact that allow to define new class method from an object.
    
    To be used with:
    >>> define = Definer(runner)
    >>> define[anobject].method = 'function(to, be, evaluated)'
    
    This will internaly do:
    >>> type(anobject).method = lambda self: function(params)
    """
    
    def __init__(self, runner):
        """Initialize the definer
        
        runner is the function to call to eval a command (the Python shell in
        Bristoledit)
        """
        self.runner = runner
    
    def __getitem__(self, obj):
        """Return an object that will set a method to the class of obj"""
        return _MethSetter(self.runner, type(obj))

