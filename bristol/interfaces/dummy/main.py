# -*- coding: utf-8 -*-
###############################################################################
#       interface/dummy/main.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interface.dummy
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Core of the Dummy interface for bristol (only for tests)"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import sys
import code

# Autocompletion
try:
    import readline
except ImportError:
    print("Module readline not available.")
    pass
else:
    import rlcompleter
    
    class TabCompleter(rlcompleter.Completer):
        """Completer that supports indenting"""
        
        def complete(self, text, state):
            """Return suggestion from the completer"""
            if not text:
                return ('    ', None)[state]
            else:
                return rlcompleter.Completer.complete(self, text, state)

class DummyConsole(code.InteractiveConsole):
    """Console with completr support"""
    
    def __init__(self, local_env=None):
        code.InteractiveConsole.__init__(self, locals=local_env)
        # check if readline is here
        readline.parse_and_bind("tab: complete")
        readline.set_completer(TabCompleter(local_env).complete)

class DummyInterface(object):
    """DummyInterface, for tests"""
    
    def __init__(self, Editor, filenames, screen_charset=None,
                 options=None):
        """Initialize the dummy interface"""
        self._editor = Editor(self, options=options)
        self._filenames = filenames
        self._screen_charset = screen_charset
        self.__console = None
        self.current = None
    
    def __repr__(self):
        """Python representation of this interface"""
        return "%s.DummyInterface(editor, %s)" % (
            self.__module__,
            self.screen_charset,
        )
    
    def _main(self):
        """Start the main interpreter loop"""
        self._editor._std_init(self._filenames) # done in last, 'cause of hacks
        self.__console = DummyConsole(local_env={
            'editor' : self._editor,
            'readline' : readline,
        })
        self.__console.interact("Bristoledit dummy interface")
    
    def _exit(self):
        """Close the interface"""
        sys.exit(0)
    
    def shell_mode(self):
        """We all live in the yellow command mode"""
        print('We all live in the yellow command mode')
    
    def _ask(self, prompt, default=None, help_msg=None,
             history_prev=None, history_next=None):
        """Ask something"""
        if default:
            prompt += '[' + default + ']'
        try:
            res = input(prompt + ' ')
        except EOFError:
            return ''
        else:
            if res:
                return res
            else:
                return default
    
    def _ask_a_filename(self, prompt, default=None):
        """Ask a filename"""
        return self._ask(prompt, default)
    
    def _ask_a_question(self, prompt, choices):
        """Ask a simple question, use a plugin"""
        prompt += repr(choices)
        return self._ask(prompt)
    
    def _set_status_msg(self, status_msg):
        """Display a status message"""
        print('Status:', status_msg)
        return status_msg
    
    def _exec(self, cmd):
        """Execute a command in the internal shell
        
        Display the result.
        """
        self.__console.runsource(cmd)
