# -*- coding: utf-8 -*-
###############################################################################
#       interfaces/curses/clipboard.py
#       
#       Copyright © 2009-2019, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interfaces.curses
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Class to manage a simple clipboard for the Curses interface

    if wayland running and wl-clipboard installed, use it
    if X running and xclip installed, use it
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2019, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import subprocess

class Clipboard(object):
    """Simple clipboard class"""
    
    def __init__(self, config, screen_charset):
        """Create a new clipboard"""
        self.config = config
        self.screen_charset = screen_charset
        self.utext = ''
    
    def set(self, utext):
        """Set unicode text in the clipboard"""
        self.utext = utext
        if 'WAYLAND_DISPLAY' in os.environ and self.config['use X11 clipboard']:
            try:
                wlcopy = subprocess.Popen(['wl-copy', '-p'],
                                        stdin=subprocess.PIPE, close_fds=True)
            except OSError:
                # no wl-copy, will try xclip if XWayland installed
                pass
            else:
                wlcopy.stdin.write(utext.encode(self.screen_charset))
                wlcopy.stdin.close()
                return
        if 'DISPLAY' in os.environ and self.config['use X11 clipboard']:
            try:
                xclip = subprocess.Popen(['xclip', '-i'],
                                         stdin=subprocess.PIPE, close_fds=True)
            except OSError:
                # no xclip
                pass
            else:
                xclip.stdin.write(utext.encode(self.screen_charset))
                xclip.stdin.close()
    
    def get(self):
        """Return the unicode text from the clipboard"""
        if 'WAYLAND_DISPLAY' in os.environ and self.config['use X11 clipboard']:
            try:
                output = subprocess.Popen(['wl-paste', '-n', '-p'],
                                stdout=subprocess.PIPE).communicate()[0]
            except OSError:
                # no wl-paste, will try xclip if XWayland installed
                pass
            else:
                try:
                    return output.decode(self.screen_charset)
                except UnicodeDecodeError:
                    # data is in a wrong charset, do nothing instead of crashing
                    return ""    
        if 'DISPLAY' in os.environ and self.config['use X11 clipboard']:
            try:
                output = subprocess.Popen(["xclip", "-o"],
                                        stdout=subprocess.PIPE).communicate()[0]
            except OSError:
                return self.utext
            else:
                try:
                    return output.decode(self.screen_charset)
                except UnicodeDecodeError:
                    # data is in a wrong charset, do nothing instead of crashing
                    return ""
        else:
            return self.utext
