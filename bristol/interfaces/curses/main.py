# -*- coding: utf-8 -*-
###############################################################################
#       interface/curses/main.py
#       
#       Copyright © 2009-2020, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interface.curses
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Core of the Curses interface for Bristoledit"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2020, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import sys
import time
import signal
import locale
locale.setlocale(locale.LC_ALL, '')
import curses
import curses.ascii as ascii

from ...buffer import Buffer
from ...utils.hooks import hooks, improve
from ...utils.texthacks import END_OF_FILE
from ...settings.color_default import color_scheme as default_color_scheme

from .cursesutils import utf8hack, CursesAttributes
from .cursesutils import get_console_hw, BMessage, BMAbort, BMBufferChanged
from .clipboard import Clipboard
from .cmdbarwidget import CmdBarWidget
from .editorwidget import EditorWidget
from .completerwidget import CompleterWidget

class CursesInterface(object):
    """Curses (console) interface for Bristoledit"""
    
    ## startup and internal methods
    
    def __init__(self, Editor, filenames, screen_charset=None,
                 options=None):
        """Initialize the carses interface"""
        self._editor = Editor(self, options=options)
        self._filenames = filenames
        self._screen_charset = screen_charset
        
        # interface attributes
        self._userdoc_ = _("Curses interface for Bristoledit.")
        self._has_to_exit = False
        self.current = None        # focused widget
        self._ewigs = {}           # editor widget for each buffer
        
        # curses stuff attributes
        self._stdscr = None
        self._use_utf8_tricks = (
                            screen_charset.lower().replace('-', '') == 'utf8')
        self._meta_stroke = False
        self._compose = ''
        self._h, self._w = (None, None)
        self.__use_mouse = False
        
        # Load configuration
        self._config = self._editor.config._get_component_manager(
            'CursesInterface', _("Curses interface configuration:"))
        default_config = {
            'use mouse': (False, _("Enable mouse support.")),
            'use X11 clipboard': (True,
                _("Use xclip (if installed) to"
                    "access to the X11 clipboard or wl-clipboard if wayland")),
            'show tabs': (True,
                _("Highlight tabs to avoid confusing with spaces.")),
            'show 80 chars line': (True,
                _("Draw a line at column 80 "
                    "(max width of standard terminals.")),
        }
        directives = ('color_scheme', )
        self._config.set_default(default_config, directives=directives)
        default_color_scheme(self._config.default)
        self._attrman = CursesAttributes(self._config)

        # Load the interpreter
        # TODO: allow to choice it?
        shell_env = {'editor': self._editor}
        self._editor.plugins.load('pythonshell')
        PythonShell = self._editor.plugins['pythonshell']
        self._shell = PythonShell(local_env = shell_env)
        self._editor.plugins.load('pythonshellcompleter')
        PyShCompl = self._editor.plugins['pythonshellcompleter']
        self._shell_compl = PyShCompl(self._editor, shell_env)

        # Load the clipboard
        self._clipboard = Clipboard(self._config, self._screen_charset)

        # make hooks
        self._init_hooks()
    
    def __repr__(self):
        """Python representation of this interface"""
        return "%s.CursesInterface(editor, %s)" % (
            self.__module__,
            self._screen_charset,
        )

    __str__ = lambda self: self._userdoc_
    __unicode__ = __str__
    
    def _main(self):
        """Build the interface and run the main loop of the Curses interface
        
        This is a wrapper around all the curses stuff.
        """
        # set the stderr to /dev/null, to not poluate the screen when
        # exceptions occurs (or to the log in debug mode)
        olderr = sys.stderr
        if not self._editor._debug:
            sys.stderr = open(os.devnull)
        else:
            #sys.stderr = self._editor._debug_log
            pass
        curses.wrapper(self._run_interface)
        sys.stedrr = olderr
        sys.exit(0)
    
    def _run_interface(self, stdscr):
        """Initialize windows and widgets, finish the editor's init, and then
        start the main loop.
        """
        self._stdscr = stdscr
        # Initialize the curses library
        curses.raw()
        if self._config['use mouse']:
            self.use_mouse = True
        self._h, self._w = self._stdscr.getmaxyx()
        curses.use_default_colors()
        stdscr.nodelay(True)
        stdscr.timeout(500) # time to check for events
        # Initialize widgets of the interface
        self._cmdbar = CmdBarWidget(self._stdscr, self._attrman,
                                    self._h - 1, self._w,
                                    self._clipboard)
        self._complw = CompleterWidget(self._stdscr, self._attrman)
        # End the init of the editor (done in last, 'cause of hacks)
        # Thanks to hooks, this will also create widgets for opened files
        initmsg = self._editor._std_init(self._filenames)
        # Initial focus
        self._focus(self._ewigs[self._editor.current])
        # log the startup time
        _debug_print('Started in %fs.' % (time.process_time()))
        if initmsg:
            welcome_msg = initmsg
        elif self._editor.current._filename:
            welcome_msg = (
                _('Welcome aboard, file "%(filename)s" (%(idx)d/%(total)d).')
                % {'filename': os.path.basename(self._editor.current._filename),
                  'total': len(self._editor.buffers),
                  'idx': self._editor.buffers.index(self._editor.current) + 1}
            )
        else:
            welcome_msg = _( 
                'Welcome aboard, a new file is at your disposition.'
            )
        
        # needed to finish initialization… and display the welcome message
        self._event_manager()
        
        # main loop
        while not self._has_to_exit:
            self._event_manager(welcome_msg)
            welcome_msg = None
        
        # clear the screen before leaving
        stdscr.erase()
    
    ## Event manager
    
    def _event_manager(self, welcome_msg=None):
        """Read a key event and manage it
        
            Each event handle can return a value:
            A None value means that everything is ok.
            A BMessage is a internal message, which
                will not be forwarded to the user. Some BMessage are
                intercepted by the event manager, and not returned, others
                are returned as is.
            Another value (ex: a string) will be returned by the event manager,
            and can be forwarded to the user by the caller.

        """
        result = None
        
        #    ---- check for external events ----    #
        
        eventresult = self._editor._check_events()
        if eventresult:
            result = eventresult
            self._set_status_msg(result)
        
        #    ---- print the welcome message ----    #
        
        if welcome_msg is not None:
            self._set_status_msg(welcome_msg)
        
        #    ---- retreive the event ----    #
        
        # get the keycode
        keycode = self._stdscr.getch() if not eventresult else -1
        if self._use_utf8_tricks:
            uchar = utf8hack(keycode, self._stdscr.getch)
        else:
            uchar = None
        
        # meta (alt or escape) management (part 1)
        if self._meta_stroke:
            keycode += 128
            uchar = None
                
        # check if the keycode is a key bindings (and convert it in command)
        try:
            kn = curses.keyname(keycode).decode('ascii')
        except ValueError:
            kn = None
        
        # compose management (part 1)
        if self._compose and kn:
            kn = self._compose + kn
            self._compose = ''
            self._meta_stroke = True # stay in meta mode
        elif self._compose and not kn:
            self._compose = ''
        
        if not self._meta_stroke and kn and kn.startswith('M-'):
            # avoid to confuse meta keystroke with utf-8 characters
            kn = None
        # get the command from the config key manager
        try:
            command = self._editor.config.key[kn]
        except KeyError:
            if self._meta_stroke:
                # find if a key start by kn (compose mode)
                if filter(lambda key: key.startswith(kn),
                          self._editor.config.key.keys()):
                    self._compose = kn
            command = None
        
        # meta (alt or escape) management (part 2)
        if keycode == 27:
            self._meta_stroke = True
            return	# wait for anothe key in the event loop
        elif self._meta_stroke:
            self._meta_stroke = False
        
        # compose management (part 2)
        if self._compose:
            return # wait for the next compose char
        
        # decode non-utf8 characters
        if uchar is None and keycode in range(256):
            uchar = bytes((keycode, )).decode(self._screen_charset, 'replace')
        
        #    ---- manage the event ----    #
        result = None
        if command:
            result = self._exec(command, no_history=True)
        elif keycode == curses.KEY_RESIZE:
            result = self._resize()
        elif keycode == curses.KEY_MOUSE:
            # TODO: mouse support
            (mid, mx, my, z, bstate) = curses.getmouse()
            # if in the editor widget
                # if not focused, exit current mode, focus editor
                # forward event to the editor widget
            # else if in cmd bar
                # if not focused, go in shell mode
                # forward event to the cmdbar widget
            result = None
        elif keycode == ascii.TAB and self._complw.visible:
            result = self._complw.complete(self.current._buffer)
        # Forward the event to the focused widget (if any)
        elif self.current is not None:
            result = self.current._event_manager(keycode, uchar)
        
        #    ---- update the interface ----    #

        # update the cursor position
        #try:
        #    self.current._move_cursor()
        #except AttributeError:  # if current has no ._buffer
        #    pass                # when exiting

        # update the completion panel
        if result is BMBufferChanged:
            self._refresh_compl()
            result = None   # do not forward a BMBufferChanged
        # refresh the status bar
        if not result and keycode != -1:
            self._refresh_status()

        curses.doupdate()
        return result
    
    def _resize(self):
        """Propagate resize event to the interface"""
        self._h, self._w = get_console_hw()
        self._stdscr.resize(self._h, self._w)
        self._stdscr.touchwin()
        self._stdscr.refresh()
        self._cmdbar._resize(self._w, y=self._h - 1)
        self._complw._resize()
        for buff, ewig in self._ewigs.items():
            ewig._resize()

    def _focus(self, widget):
        """Switch the focus to the given widget"""
        if self.current:
            self.current._focus(release=True)
        if widget is not None:
            self.current = widget._focus()
        else:
            self.current = None

    ## External API

    def _exit(self):
        """Close the interface"""
        self._has_to_exit = True

    def shell_mode(self, start_text=None):
        """Execute an internal shell command"""
        if self._cmdbar._cmdmode:
            # no nested shell modes, but leave the current one
            return BMAbort
        if not start_text:
            start_text = 'editor.'
        # ask a command
        cmd = self._ask(
            self._shell.prompt,
            start_text,
            _("Hit <Tab> for a list of commands"),
            self._shell.history_prev,
            self._shell.history_next,
            self._shell_compl,
        )
        # exec the command
        if cmd:
            return self._exec(cmd)
        else:
            return _('Aborted')
            
    def suspend(self):
        """Send a suspend signal to the current process"""
        # sleep
        self._stdscr.erase()
        self._stdscr.refresh()
        curses.def_prog_mode()
        curses.reset_shell_mode()
        # suspend
        os.kill(0, signal.SIGSTOP)
        # wake up
        curses.def_shell_mode()
        curses.reset_prog_mode()
        self._stdscr.clear()
        self.current._show()
        self._cmdbar._refresh()
        self._stdscr.touchwin()
        self._stdscr.refresh()
        self.current._move_cursor()
        # if in shell mode, leave it
        if self._cmdbar._cmdmode:
            return BMAbort
    
    def _ask(self, prompt, default=None, help_msg=None,
             history_prev=None, history_next=None, completer=None,
             afterchars=None):
        """Ask something using the command bar
        
        If the ask is aborted, return None
        """
        # change the cmdbar mode
        self._cmdbar._set_cmd_mode(prompt, default, help_msg,
                                   history_prev, history_next,
                                   completer=completer, afterchars=afterchars)
        # focus the widget
        self._focus(self._cmdbar)
        # open the completion panel if possible
        self._refresh_compl()
        # start a new event loop
        answer = None
        while answer is None and not self._has_to_exit:
            answer = self._event_manager()
        # unfocus the widget
        self._focus(self._ewigs.get(self._editor.current, None))
        self._cmdbar._set_status_mode()
        # update/remove the completion panel
        self._refresh_compl()
        if isinstance(answer, BMessage):
            return None
        else:
            return answer
    
    def _ask_a_filename(self, prompt, default=None):
        """Ask a filename"""
        # load the completer
        self._editor.plugins.load('filecompleter')
        FileCompleter = self._editor.plugins['filecompleter']
        return self._ask(prompt, default,
                         completer=FileCompleter(self._editor))
    
    def _ask_a_question(self, prompt, choices):
        """Ask a simple question"""
        prompt += ' [' + ', '.join(r.title() for r in choices) + '] '
        chars = [c[0].lower() for c in choices]
        # change the cmdbar mode
        self._cmdbar._set_cmd_mode(prompt, single_char=chars)
        # focus the widget
        self._focus(self._cmdbar)
        # start a new event loop
        answer = None
        while answer is None and not self._has_to_exit:
            answer = self._event_manager()
        # unfocus the widget
        self._focus(self._ewigs.get(self._editor.current, None))
        self._cmdbar._set_status_mode()
        # update/remove the completion panel
        self._refresh_compl()
        try:
            i = chars.index(answer)
            return i
        except ValueError:
            return None
    
    def _set_status_msg(self, status_msg):
        """Display a status message"""
        self._cmdbar._set_status(status_msg)
        #return status_msg
    
    def _exec(self, cmd, no_history=False):
        """Execute a command in the internal shell
        
        If no_history, ask the shell to not keep the command in the history.

        Display and return the result.
        """
        res = self._shell.run(cmd, no_history)
        if isinstance(res, str):
            prettyres = res
        elif res is None:
            #prettyres = _('Success!')
            return res  # do not print something
        elif hasattr(res, '__str__'):
            prettyres = str(res)
        else:
            prettyres = repr(res)
        prettyres = prettyres.replace('\n', ' ')
        self._set_status_msg('-> ' + prettyres)
        return res  # in case of nested _exec
    
    def _get_mouse_support(self):
        """Return the actual mouse support state"""
        return self.__use_mouse
    def _set_mouse_support(self, support):
        """Change the mouse support state"""
        if support:
            curses.mousemask(curses.ALL_MOUSE_EVENTS)
        else:
            curses.mousemask(0)
        self.__use_mouse = support
    mouse_support = property(fget=_get_mouse_support,
                             fset=_set_mouse_support,
                             doc="State of the mouse support")
    
    ## internal methods
    
    def _refresh_compl(self):
        """Refresh the completion panel"""
        if self._complw.visible:
            try:
                self._ewigs[self._editor.current]._refresh()
            except KeyError:
                pass    # quiting, no ewigs left
        try:
            completer = self.current._get_completer()
        except AttributeError:
            suggests = None   # current widget don't provide a completer
        else:
            if completer:
                suggests = completer.suggest(self.current._buffer)
            else:
                suggests = None
        self._complw.update(suggests)
        self._cmdbar._refresh()

    def _refresh_status(self):
        """Refresh the default status message"""
        if self._cmdbar._cmdmode:
            return # no status refresh in cmd mode
        if hasattr(self.current, 'insert') and self.current.insert:
            ins_status = _('INS')
        else:
            ins_status = _('OVR')
        if hasattr(self.current, 'vertedit') and self.current.vertedit:
            ins_status = ins_status[:-2] + _('-V')
        ed = self._editor
        curbuff = ed.current
        try:
            curewig = self._ewigs[curbuff]
        except KeyError:
            return
        msg = _('Ctrl+T: shell | %(filename)s%(modified)s '
                'row: %(row)03d(%(percent)03d%%) col: %(col)03d '
                '%(ins)s %(mime)s %(coding)s %(eol)s') % {
            'filename': os.path.basename(curbuff._filename)
                        if curbuff._filename
                        else _('No name %d') % ed.buffers.index(curbuff),
            'modified': '*' if curbuff._is_modified else ' ',
            'row': curewig._vy + 1,
            'percent': ((curewig._vy + 1) * 100 // 
                        (curbuff._number_of_line() + 1)),
            'col': curewig._vx,
            'ins': ins_status,
            'coding': curbuff.encoding,
            'mime': curbuff._mimetype,
            'eol': curbuff.endofline,
        }
        self._set_status_msg(msg)

    def _init_hooks(self):
        """Make needed hooks"""
        
        Editor = type(self._editor)
        
        @hooks(Editor, 'open', self)
        def open(oldself, oldopen, filename=None, encoding=None,
                 endofline=None):
            """Open a new editor widget on file open"""
            new_buffer = oldopen(oldself, filename, encoding, endofline)
            if isinstance(new_buffer, Buffer):
                if new_buffer not in self._ewigs:
                    # it should be created by _set_text
                    self._ewigs[new_buffer] = EditorWidget(
                        self._stdscr,
                        self._config, self._attrman,
                        new_buffer, self._clipboard
                    )
                self._editor.switch_to(new_buffer)
            return new_buffer

        @hooks(Editor, 'new', self)
        def new(oldself, oldnew):
            """Open a new editor widget on file creation"""
            new_buffer = oldnew(oldself)
            if new_buffer:
                if new_buffer not in self._ewigs:
                    self._ewigs[new_buffer] = EditorWidget(
                        self._stdscr,
                        self._config, self._attrman,
                        new_buffer, self._clipboard,
                    )
                self._editor.switch_to(new_buffer)
            return new_buffer

        @hooks(Editor, 'next', self)
        def next(oldself, oldnext):
            """Display the next buffer"""
            if self._editor.current:
                self._ewigs[self._editor.current]._visible = False
            nbuff = oldnext(oldself)
            self._ewigs[nbuff]._show()
            self._ewigs[nbuff]._refresh()
            self._focus(self._ewigs[nbuff])
            return nbuff

        @hooks(Editor, 'prev', self)
        def prev(oldself, oldprev):
            """Display the previous buffer"""
            if self._editor.current:
                self._ewigs[self._editor.current]._visible = False            
            pbuff = oldprev(oldself)
            self._ewigs[pbuff]._show()
            self._ewigs[pbuff]._refresh()
            self._focus(self._ewigs[pbuff])
            return pbuff

        @hooks(Editor, 'switch_to', self)
        def switch_to(oldself, oldswitch_to, target_buffer):
            """Display the target_buffer"""
            if self._editor.current:
                self._ewigs[self._editor.current]._visible = False            
            tbuff = oldswitch_to(oldself, target_buffer)
            self._ewigs[tbuff]._show()
            self._ewigs[tbuff]._refresh()
            self._focus(self._ewigs[tbuff])
            return tbuff
        
        @hooks(Editor, 'close', self)
        def close(oldself, oldclose, target_buffer=None,
                  keep_a_buffer_open=True):
            """Remove the widget corresponding to the buffer"""
            ret = oldclose(oldself, target_buffer, keep_a_buffer_open)
            # clean ewigs
            for buff in list(self._ewigs):
                if not buff in self._editor.buffers:
                    dewig = self._ewigs[buff]
                    del(dewig._buffer)
                    del(dewig)
                    del(self._ewigs[buff])
            return ret

        @hooks(Buffer, 'save', self)
        def save(oldself, oldsave, filename=None, encoding=None,
                 endofline=None):
            """Update the status line on saving"""
            ret = oldsave(oldself, filename, encoding, endofline)
            if ret is None:
                self._refresh_status()
            return ret

        # TODO: improve hooks bind for keys (and bind compose)
        @hooks(type(self._editor.config), 'bind', self)
        def bind(oldself, oldbind, command, key=None):
            """Bind a key with a command"""
            if key is None:
                self._set_status_msg(_("Press the key to bind."))
                keycode = -1
                while keycode == -1:
                    keycode = self._stdscr.getch()
                if keycode == 27:   # alt/meta
                    keycode = self._stdscr.getch() + 128
                key = curses.keyname(keycode).decode('utf-8')
            oldbind(oldself, command, key)
            return _("The key '%s' is bind.") % key


        @hooks(Buffer, '_set_text', self)
        def set_text(oldself, set_text, new_text=None, new_position=None,
                     insert_position=None, delete_position=None, delete_len=0):
            """Refresh the widget on buffer modifications"""
            # retreive the ewig
            try:
                ewig = self._ewigs[oldself]
            except KeyError:    # the ewig is probably not yet created
                # create it
                ewig  = EditorWidget(
                    self._stdscr,
                    self._config, self._attrman,
                    oldself, self._clipboard
                )
                self._ewigs[oldself] = ewig
            
            self._update_lock = True
            
            if self._update_lock:            
                # compute the line offset for deleting part
                loffset = 0
                if delete_position is not None:
                    loffset -= (oldself.text[delete_position:
                                delete_position + delete_len].count('\n'))
                    dellineno = oldself._line_of_position(delete_position)
                    ewig._drawed_lines[dellineno] = False
                    if loffset < 0:
                        del(ewig._drawed_lines[dellineno + 1:
                                                dellineno + 1 - loffset])
            
            # do the changes
            ret = set_text(oldself, new_text, new_position, insert_position,
                            delete_position, delete_len)
            
            ewig._check_pad_size()

            if self._update_lock and new_text is not None:
                # the text has changed
                if insert_position is None:
                    # full refresh
                    ewig._drawed_lines = [False] * oldself._number_of_line()
                    ewig._draw_line()
                else:
                    # partial refresh
                    iloffset = new_text.count('\n')
                    inslineno = oldself._line_of_position(insert_position)
                    #try:
                    #    ewig._drawed_lines[inslineno] = False
                    #except IndexError:
                    #    ewig._drawed_lines.append(False)
                    #if iloffset > 0:
                    #    ewig._drawed_lines = (
                    #        ewig._drawed_lines[:inslineno + 1] +
                    #        [False] * (iloffset+1) +
                    #        ewig._drawed_lines[inslineno + 2:]
                    #    )
                    curlineno = inslineno
                    while curlineno <= inslineno + (iloffset + 1 if iloffset > 0 else 0):
                        try:
                            ewig._drawed_lines[inslineno] = False
                        except IndexError:
                            ewig._drawed_lines.append(False)
                        curlineno += 1
                    ewig._draw_line(line_offset = loffset + iloffset)
            self._update_lock = False
            
            # update the cursor
            if new_position is not None:
                # here we will use oldself._position, because it may has been
                # modified by the call to set_text....
                ewig._move_cursor(position=oldself._position)
            return ret




