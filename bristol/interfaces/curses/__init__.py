# -*- coding: utf-8 -*-
###############################################################################
#       interfaces/curses/__init__.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interfaces.curses
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Curses (text) interface package for Bristoledit.

Here you can found the modules:
  * bristol.interfaces.curses.clipboard      Clipboard for curses and xclip.
  * bristol.interfaces.curses.cmdbarwidget   Status bar and shell widget.
  * bristol.interfaces.curses.completerwidget    Show current completion
                                                 suggestions in a panel.
  * bristol.interfaces.curses.cursesutils    Useful functions for Curses.
  * bristol.interfaces.curses.editorwidget   Module of the editor curses widget.
  * bristol.interfaces.curses.main           The main module of the interface.
  * bristol.interfaces.curses.textwidget     A generic text widget.
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"
