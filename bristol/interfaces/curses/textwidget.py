# -*- coding: utf-8 -*-
###############################################################################
#       interfaces/curses/textwidget.py
#       
#       Copyright © 2009-2020, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interfaces.curses
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Base text widget for ncurses"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2020, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import weakref

import curses
import curses.ascii as ascii

from ...utils.texthacks import END_OF_LINE, END_OF_FILE
from ...utils.texthacks import tab_len, tabbed, char_size
from ...utils.hooks import hooks
from .cursesutils import BMBufferChanged

class MiniBuffer(object):
    """A minimalist buffer used by the text widget to store data"""

    def __init__(self, text=None):
        """Initialize a new MiniBuffer"""
        if text:
            self._text = text
        else:
            self._text = ''
        self._position = 0  # should be changed through _set_text
        self._mark_position = None
        self._widget = None
    
    def _get_text(self):
        """Return the text"""
        return self._text

    def _set_text(self, new_text=None, new_position=None, insert_position=None,
                  delete_position=None, delete_len=0):
        """Change the text of the buffer"""
        if new_position is not None:
            self._position = new_position
        if new_text is not None and insert_position is None:
            # full text remplacement
            self._text = new_text
        elif new_text is not None:
            if delete_position is not None:
                # delete text
                self._text = (self._text[:delete_position] +
                              self._text[delete_position + delete_len:])
            # insert text
            self._text = (self._text[:insert_position] +
                          new_text +
                          self._text[insert_position:])
    text = property(fget=_get_text)
    
    def _number_of_line(self):
        """Return the number of lines"""
        return self._text.count('\n') + 1

    def _get_line(self, lineno):
        """Return the content of the line lineno.

        \n at the end of line are kept
        """
        return self.text.splitlines(True)[lineno]

class HookedMiniBuffer(MiniBuffer):
    """A mini buffer hooked by the TextWidget.
        
        This allow you to herit from MiniBuffer without
        being hooked.
    """
    pass

def vertical(func):
    """Decorator for methods that may be use in vertical edit mode"""
    def wrapped_func(self, *args, **kwargs):
        if self.vertedit and self._buffer._mark_position is not None:
            self.vertedit = False # disable vertedit to avoid loops
            cur_line = self._buffer._line_of_position(self._buffer._position)
            cur_idx = self._buffer._index_of_position(self._buffer._position)
            # for all lines except the current one in the selection:
            first_line = self._buffer._line_of_position(
                                            self._buffer._mark_position)
            last_line = self._buffer._line_of_position(
                                                self._select_end)
            if last_line < first_line:
                first_line, last_line = last_line, first_line
            for lineno in range(first_line, last_line + 1):
                if lineno != cur_line:
                    # set the position to the same colone in the line
                    newpos = self._buffer._start_of_line(lineno) + cur_idx
                    self._move_cursor(position=newpos)
                    # call the fuction
                    func(self, *args, **kwargs)
            # restore position
            self._move_cursor(
                position=(self._buffer._start_of_line(cur_line) + cur_idx)
            )
            # re-enable vertedit
            self.vertedit = True
        elif self.vertedit and self._buffer._mark_position is None:
            # disable vertedit
            self.vertedit = False
        return func(self, *args, **kwargs)
    return wrapped_func


class TextWidget(object):
    """Base text widget for ncures"""
    
    __hooks_done = False    # make sure hooks are done only one time

    ## widget methods

    def __init__(self, stdscr, attrman, y, x, h, w, text=None,
                 multiline=True, clipboard=None, style=None, completer=None,
                 visible=True):
        """Initialize a new text widget.
        
        stdscr is the curses root window
        attrman is the Bristoledit curses attributes manager
        y, x is the initial position of the widget
        h, w is the initial size of the widget
        text is the initial text of the widget
        multiline allow to enter line break in the widget
        clipboard is the Bristoledit clipboard manager, or None
        style is the style of text/background
        completer is the autocompletion engine
        visible is the default visibility setting
        """
        # set hooks, done one time for the class
        if not self.__hooks_done:
            self.__init_hooks()
        # set attributes
        self._stdscr = stdscr
        self._attrman = attrman
        self._y = y
        self._x = x
        self._h = h
        self._w = w
        self._multiline = multiline
        self._clipboard = clipboard
        if not style:
            self._style = 'editor_style'
        else:
            self._style = style
        self._completer = completer
        self._focused = False
        self._visible = visible
        # constants (may be variables in childs)
        self._smooth_y = 2
        self._smooth_x = 4
        self.__tab_size = 4
        # initialize the ncurses pad
        self._pad = curses.newpad(self._h, self._w)
        self._dy, self._dx = 0, 0            # pad delta
        self._pad.bkgd(' ', attrman.get_attribute(self._style))
        # text default attributes
        self._vx = 0                         # cursor inside the pad
        self._vy = 0
        self.insert = True
        self.vertedit = False
        self._select_end = None              # end of the selection (cursor)
        # set text and buffer for the widget
        if not hasattr(self, '_buffer'):
            self._buffer = HookedMiniBuffer()
            self._buffer._widget = weakref.ref(self)
            if text and multiline:
                self._buffer._set_text(text)
            elif text and not multiline:
                self._buffer._set_text(text.replace('\n', ''))
            else:
                self._buffer._set_text('')
        # display the widget
        #self._check_pad_size()
        #self._draw_line(min_line=self._dy, max_line=self._dy + self._h)
        if self._visible:
            self._show()
    
    def _get_tab_size(self):
        return self.__tab_size
    _tab_size = property(fget=_get_tab_size)

    def __init_hooks(self):
        """Hook changes on the buffer to update the screen"""

        @hooks(HookedMiniBuffer, '_set_text', TextWidget)
        def set_text(oldself, set_text, new_text=None, new_position=None,
                     insert_position=None, delete_position=None, delete_len=0):
            """Refresh the widget on buffer modifications"""
            # compute the line offset
            loffset = 0
            if delete_position is not None:
                loffset -= (oldself.text[delete_position:
                            delete_position + delete_len].count('\n'))
            ret = set_text(oldself, new_text, new_position, insert_position,
                            delete_position, delete_len)
            if new_text is not None:
                # the text has changed
                if new_position is None:
                    # full refresh
                    oldself._widget()._check_pad_size()
                    oldself._widget()._draw_line(
                        min_position=0,
                        max_position=END_OF_FILE
                    )
                else:
                    # partial refresh
                    if delete_position:
                        minp = min(insert_position, delete_position)
                        maxp = max(insert_position + len(new_text),
                                   delete_position)
                    else:
                        minp = insert_position
                        maxp = insert_position + len(new_text)
                    oldself._widget()._check_pad_size()  # better pad resize?
                    loffset += new_text.count('\n')
                    oldself._widget()._draw_line(
                        min_position=minp,
                        max_position=maxp,
                        line_offset=loffset
                    )
            # update the cursor
            if new_position is not None:
                oldself._widget()._move_cursor(position=new_position)
            return ret

    def _show(self):
        """Make the widget visible"""
        self._visible = True
        self._pad.overwrite(
            self._stdscr,
            self._dy, self._dx,
            self._y, self._x,
            self._y + self._h - 1, self._x + self._w - 1,
        )
        self._pad.touchwin()

    def _resize(self, h, w, y=None, x=None):
        """Resize the text widget
        
        h, w is the new size
        if y, x are given, also move the widget
        """
        self._h, self._w = h, w
        if y is not None:
            self._y = y
        if x is not None:
            self._x = x
        self._check_pad_size(dw=self._w)
        self._move_pad(force_redraw=True) # do the refresh
        if self._visible:
            self._move_cursor(position=self._buffer._position)
     
    def _refresh(self):
        """Refresh the widget"""
        if not self._visible:
            return
        self._pad.refresh(
            self._dy, self._dx,
            self._y, self._x,
            self._y + self._h - 1, self._x + self._w - 1,
        )
    
    def _focus(self, release=False):
        """Focus the widget (put cursor in) and return the focus widget
        
        if release is True, release the focus
        """
        self._focused = not release
        if self._focused:
            self.visible = True
            if self.insert:
                curses.curs_set(1)  # underline cursor
            else:
                curses.curs_set(2)  # block cursor
            self._move_cursor()
        return self
    
    def _get_completer(self):
        """Return the bounded completer or None"""
        return self._completer
    
    ## event management methods

    def _event_manager(self, keycode, uchar=None):
        """Manage a curses event in the widget
        
        keycode is the keycode of the event

        If the event is a character input, uchar is the unicode character.
        """
        if keycode == ascii.TAB or keycode == ascii.ctrl('i'):
            return self.tab()
        elif keycode == ascii.LF:
            return self.enter()
        elif uchar is not None:
            return self._addch(uchar)
        return None
    
    def go_right(self, step=1, one_word=False):
        """Move the cursor to the right"""
        if not one_word:
            self._move_cursor(dx=step)
        else:
            pos = self._buffer._position + 1
            while ((pos < len(self._buffer.text)) and
                   (self._buffer.text[pos].isalnum())):
                pos += 1
            self._move_cursor(position=pos)
    go_right._userdoc_ = _("Move right the cursor")

    def go_left(self, step=1, one_word=False):
        """Move the cursor to the left"""
        if not one_word:
            self._move_cursor(dx=-step)
        else:
            pos = self._buffer._position - 1
            while ((pos > 0) and (self._buffer.text[pos - 1].isalnum())):
                pos -= 1
            self._move_cursor(position=max(pos, 0))
    go_left._userdoc_ = _("Move left the cursor")
    
    def go_down(self, step=1, one_page=False):
        """Move down the cursor"""
        if one_page:
            step = self._h
        self._move_cursor(dy=step)
    go_down._userdoc_ = _("Move down the cursor")

    def go_up(self, step=1, one_page=False):
        """Move up the cursor"""
        if one_page:
            step = self._h
        self._move_cursor(dy=-step)
    go_up._userdoc_ = _("Move up the cursor")

    def go_home(self):
        """Move the cursor to the begining of the line"""
        self._move_cursor(ax=0)
    go_home._userdoc_ = _("Go at the begining of the line")

    def go_end(self):
        """Move the cursor to the end of the line"""
        self._move_cursor(ax=END_OF_LINE)
    go_end._userdoc_ = _("Go at the end of the line")
    
    @vertical
    def delch(self, step=1, before=True, one_word=False):
        """Del step character before (or not) the cursor
        
        if one_word, delete one word before or after the cursor
        
        step is the number of deleted characters, and can be END_OF_LINE.

        If a mark is set, delch will delete the current selection,
        no matter values of step or before.
        """
        if self._buffer._mark_position is not None: # delete selection
            before = (self._buffer._mark_position < self._select_end)
            step = abs(self._buffer._mark_position - self._select_end)
        
        if step == END_OF_LINE:
            if self._buffer._position >= len(self._buffer.text): # at the end
                return
            elif self._buffer.text[self._buffer._position] == '\n':
                step = 1    # delete the line break
            else:
                try:
                    step = (self._buffer.text.index('\n',
                            self._buffer._position) - self._buffer._position)
                except:
                    step = len(self._buffer.text) - self._buffer._position

        # if one_word, change step to get the word
        if one_word:
            if before:
                pos = self._buffer._position
                while (((pos - step) > 0) and
                        (self._buffer.text[pos - step].isalnum())):
                    step +=1
            else:
                pos = self._buffer._position
                while (((pos + step) < len(self._buffer.text)) and
                        (self._buffer.text[pos+step].isalnum())):
                    step += 1

        del_pos = (self._buffer._position if not before
                   else self._buffer._position - step)
        # check the text bounds
        if del_pos not in range(len(self._buffer.text)):
            return
        # remove the selection
        if self._buffer._mark_position is not None:
            self._update_selection(toogle_mark=True)
        # delete text
        new_pos = (self._buffer._position if not before
                   else self._buffer._position - step) 
        self._buffer._set_text(
            new_text='',
            new_position=new_pos,
            insert_position=del_pos,
            delete_position=del_pos, delete_len=step,
        )
        return BMBufferChanged  # forward the buffer changed info
    delch._userdoc_ = _("Delete a character")
    
    @vertical
    def tab(self):
        """Insert a tabulation

        If back, remove a tabulation.
        """
        return self._addch('\t')
    tab._userdoc_ = _("Insert a tabulation")
    
    def enter(self):
        """Insert a line break"""
        if self._multiline:
            return self._addch('\n')
    enter._userdoc_ = _("Insert a line break")
    
    def toogle_mark(self):
        """Go in/out selection mode"""
        self._update_selection(toogle_mark=True)
        self._move_cursor()
    toogle_mark._userdoc_ = _("Set a mark for selecting text")
    
    def copy(self):
        """Copy the selected text to the clipboard"""
        if self._clipboard and self._buffer._mark_position is not None:
            minsel = min(self._buffer._mark_position, self._select_end)
            maxsel = max(self._buffer._mark_position, self._select_end)
            self._clipboard.set(self._buffer.text[minsel:maxsel])
            self._update_selection(toogle_mark=True)
            self._move_cursor()
    copy._userdoc_ = _("Copy the selected text")

    def cut(self):
        """Cut the selected text to the clipboard"""
        if self._clipboard and self._buffer._mark_position is not None:
            minsel = min(self._buffer._mark_position, self._select_end)
            maxsel = max(self._buffer._mark_position, self._select_end)
            self._clipboard.set(self._buffer.text[minsel:maxsel])
            return self.delch()
    cut._userdoc_ = _("Cut the selected text")

    def paste(self):
        """Paste the selected text"""
        if self._clipboard:
            newtext = self._clipboard.get()
            
            if not self._multiline:
                newtext = newtext.replace('\n', ' ')

            self._buffer._set_text(
                new_text=newtext,
                new_position=self._buffer._position + len(newtext),                
                insert_position=self._buffer._position
            )
            return BMBufferChanged
    paste._userdoc_ = _("Paste the selected text")
    
    @vertical
    def _addch(self, uchar):
        """Add the given unicode character to the current position.
        
        If self.insert or the cursor is at the end of the line, put the
        character before the curent one, else replace it.
        Then move the cursor forward.
        """
        if (self.insert or
            self._buffer._position == len(self._buffer.text) or
            self._buffer.text[self._buffer._position] == '\n'):
            # insert the char
            self._buffer._set_text(
                new_text = uchar,
                new_position = self._buffer._position + 1,
                insert_position = self._buffer._position
            )
        else:
            # replace the char
            self._buffer._set_text(
                new_text = uchar,
                new_position = self._buffer._position + 1,
                insert_position = self._buffer._position,
                delete_position = self._buffer._position,
                delete_len = 1
            )
        return BMBufferChanged

    ## generic methods
    
    def _check_pad_size(self, dh=None, dw=None):
        """Make sure the pad can contain the text

        The size of the pad cannot be less than the widget size and less
        than (text size + 1).

        You can provide a delta h (dh) or a delta w (dw) to resize the
        pad without recompute size from the text.

        The pad size can only grow.
        """
        curh, curw = self._pad.getmaxyx()
        if dh is not None or dw is not None:
            # delta resize
            if not dh:
                dh = 0
            if not dw:
                dw = 0
            newh = max(curh + dh, self._h, curh)
            neww = max(curw + dw, self._w, curw)
        else:
            # auto resize
            lines = self._buffer.text.split('\n')
            newh = max(len(lines) + 2, curh, self._h)
            if lines:
                longestline = max(tab_len(line, self._tab_size) for line in lines)
            else:
                longestline = 0
            neww = max(longestline, curw, self._w)
        if newh != curh or neww != curw:
            self._pad.resize(newh, neww)
    
    def _move_pad(self, force_redraw=False):
        """Ensure the cursor position is visible by moving the pad"""
        # Retreive data
        vy, vx = self._vy, self._vx     # cursor position
        wh, ww = self._h, self._w       # widget size
        ph, pw = self._pad.getmaxyx()   # pad size
        dy, dx = self._dy, self._dx     # pad offset
        # compute y
        dy = min(max(vy - wh + 1 + self._smooth_y, dy), ph - wh - 0)
        dy = max(min(vy - self._smooth_y, dy), 0)
        # compute x
        dx = min(max(vx - ww + 1 + self._smooth_x, dx), pw - ww - 0)
        dx = max(min(vx - self._smooth_x, dx), 0)
        
        # if needed, refresh the pad
        if force_redraw:
            self._dy, self._dx = dy, dx
            self._draw_line(min_line=self._dy, max_line=self._dy + self._h)
        elif dy != self._dy or dx != self._dx:
            self._dy, self._dx = dy, dx
            self._draw_line()

    def _position_of_yx(self, y, x):
        """Return the position in the text for (y, x) cursor"""
        # should be in MiniBuffer?
        position = 0
        for lineno, linectn in enumerate(self._buffer.text.splitlines(True)):
            if lineno == y and x == 0:
                return position
            elif lineno == y:
                csizes = char_size(linectn, self._tab_size)
                if x == END_OF_LINE:
                    return position + sum(csizes[:-1])
                else:
                    curx = 0
                    for p, cs in enumerate(csizes):
                        curx += cs
                        if curx >= x:
                            return position + p + 1
                # this work with x = -1 to go to the end: it drop the \n
                return position + sum(csizes[:x])
            else:
                position += len(linectn)
        return position

    def _yx_of_position(self, position):
        """Return the (y, x) cursor for position in text"""
        if not self._buffer.text:
            return (0, 0)
        if position == END_OF_FILE:
            position = len(self._buffer.text)
        for lineno, linectn in enumerate(self._buffer.text.split('\n')):
            if position <= len(linectn):
                csizes = char_size(linectn, self._tab_size)
                return (lineno, sum(csizes[:position]))
            else:
                position -= (len(linectn) + 1) # +1 for the \n
        return (lineno + 1, 0)
    
    def _update_selection(self, toogle_mark=False):
        """Update the current selection

        Use toogle_mak to set or unset the mark
        """
        EOF = len(self._buffer.text)
        # compute min and max of old selection
        if self._buffer._mark_position is None:
            oldmin = EOF
            oldmax = 0
        else:
            oldmin = min(self._buffer._mark_position, self._select_end)
            oldmax = max(self._buffer._mark_position, self._select_end)
        if toogle_mark and self._buffer._mark_position is not None:
            # remove mark
            self._buffer._mark_position = None
        elif toogle_mark and self._buffer._mark_position is None:
            # place a mark
            self._buffer._mark_position = self._buffer._position

        # update the selection and compute lines to refresh
        if self._buffer._mark_position is not None:
            # mark posed, update the selection
            self._select_end = self._buffer._position
            newmin = min(self._buffer._mark_position, self._select_end)
            newmax = max(self._buffer._mark_position, self._select_end)
        else:
            newmin = EOF
            newmax = 0
        if oldmin == newmin:
            upmin = min(oldmax, newmax) # do not refresh the unchanged part
        else:
            upmin = min(oldmin, newmin)
        if oldmax == newmax:
            upmax = max(oldmin, newmin)
        else:
            upmax = max(oldmax, newmax)
        
        self._draw_line(min_position=upmin, max_position=upmax)

    def _move_cursor(self, dy=0, dx=0, ay=None, ax=None, position=None):
        """Move the cursor, check it doesn't go out the text
        
        dy, dx is the relative position to the cursor

        If ay, ax are not None, go to the absolute position ay, ax.
        If position is not None, got to the absolute position in the text.
        """
        if position is not None:
            ny, nx = self._yx_of_position(position)
        elif ay is not None and ay == END_OF_FILE:
            ny, nx = self._yx_of_position(END_OF_FILE)
        else:
            ny = ay if ay is not None else self._vy + dy
            nx = ax if ax is not None else self._vx + dx

        # check lines/y
        if ny < 0:
            ny = 0
        nblines = self._buffer._number_of_line()
        if ny > nblines - 1:
            ny = nblines - 1
        try:
            #linectn = self._buffer.text.splitlines(False)[ny] #no \n at the end
            linectn = self._buffer._get_line(ny).replace('\n', '')
        except IndexError: # last line is empty
            linectn = ''
        csizes = char_size(linectn, self._tab_size)
        tlen = sum(csizes)   # tabbed len

        # check columns/x
        if ax and ax == END_OF_LINE:
            nx = tlen
        if nx < 0:
            # if not the first line, go to the end of the prev line
            if ny > 0:
                return self._move_cursor(dy=-1, ax=END_OF_LINE)
            # else go to the begining
            nx = 0
        if nx > tlen:
            # if its only an x deplacement
            if dy == 0 and ay == None and ny < nblines - 1:
                # go to the next line
                return self._move_cursor(dy=1, ax=0)
            # else go to the end
            nx = tlen
        
        # check to not be inside a tab
        if nx != 0:
            xtest = 0
            for cs in csizes:
                xtest += cs
                if xtest == nx: # ok, that is possible
                    break
                elif xtest > nx: # xtest jumped over nx, 'cause a tab
                    if dx < 0:
                        nx = xtest - cs # make nx go before the tab
                    else:
                        nx = xtest      # make nx go after the tab
                    break
        
        # save position / virtual cursor
        # we bypass ._set_text to avoid infinite loop
        self._vy, self._vx = ny, nx
        self._buffer._position = self._position_of_yx(ny, nx)
        # check if there is a bracket to highlight
        try:
            self._highlight_bracket(self._buffer._position)
        except AttributeError:
            pass
        # update the selection
        if self._buffer._mark_position is not None:
            self._update_selection()
        # move the pad to make the cursor visible
        self._move_pad()
        # move the real cursor
        self._stdscr.move(
            self._y + ny - self._dy,
            self._x + nx - self._dx
        )
        self._stdscr.cursyncup()
    
    def _draw_line(self, line_idx=None, min_position=None, max_position=None,
                  line_offset=0, min_line=None, max_line=None):
        """Redraw visibles lines (default)
        
        Redraw the line line_idx if line_idx

        If min_position and max_position are given, draw all lines from the
        min_position line and the max_position line.
        You can use min_position=0 and max_position=END_OF_FILE to redraw the
        whole file.
        If min_line and max_line are given, draw all lines between the two.
        line_offset is the (positive) number of line to add, or the (negative)
        number of line to remove.
        """
        lines = self._buffer.text.split('\n')
        
        if line_idx is not None:
            miny = line_idx
            maxy = line_idx + 1
        elif min_line is not None and max_line is not None:
            miny = max(min_line, 0)
            maxy = min(max_line, len(lines))
        elif min_position is not None and max_position is not None:
            miny, x = self._yx_of_position(min_position)
            maxy, x = self._yx_of_position(max_position)
            maxy += 1
        else:
            miny = 0
            maxy = len(lines)
        # don't draw outside screen
        miny = max(miny, self._dy)
        maxy = min(maxy, self._dy + self._h)
        line_list = range(max(miny, 0), min(maxy, len(lines)))
        
        # manage line insertion/removal
        if line_offset > 0:
            self._pad.move(miny, 0)
            for i in range(line_offset):
                self._pad.insertln()
        elif line_offset < 0:
            self._pad.move(miny, 0)
            for i in range(-line_offset):
                self._pad.deleteln()
        
        # draw stuff
        if self._buffer._mark_position is None:
            minsel, maxsel = None, None
        else:
            minsel = min(self._buffer._mark_position, self._select_end)
            maxsel = max(self._buffer._mark_position, self._select_end)
        
        for lineno in line_list:
            sol = self._position_of_yx(lineno, 0)   # start of line
            eol = sol + len(lines[lineno])          # end of line
            self._pad.move(lineno, 0)
            self._pad.clrtoeol()
            if minsel is not None and minsel < eol and maxsel > sol:
                # the selection is part of the current line
                # text before the selection:
                if minsel > sol:
                    self._pad.addstr(
                        lineno, 0,
                        tabbed(self._buffer.text[sol:minsel], self._tab_size),
                        self._attrman.get_attribute(self._style),
                    )
                # text in the middle
                mstart = tab_len(self._buffer.text[sol:minsel], self._tab_size)
                mdmin = max(minsel, sol)
                mdmax = min(maxsel, eol)
                self._pad.addstr(
                    lineno, mstart,
                    tabbed(self._buffer.text[mdmin:mdmax], self._tab_size),
                    self._attrman.get_attribute('reverse'),
                )
                # text after the selection
                if maxsel < eol:
                    astart = tab_len(self._buffer.text[sol:maxsel], self._tab_size)
                    self._pad.addstr(
                        lineno, astart,
                        tabbed(self._buffer.text[maxsel:eol], self._tab_size),
                        self._attrman.get_attribute(self._style),
                    )
            else:
                # no selection, normal text
                self._pad.addstr(
                    lineno, 0,
                    tabbed(lines[lineno], self._tab_size),
                    self._attrman.get_attribute(self._style),
                )
        
        self._refresh()

