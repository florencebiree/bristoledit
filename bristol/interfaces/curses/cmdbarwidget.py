# -*- coding: utf-8 -*-
###############################################################################
#       interfaces/curses/cmdbarwidget.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interfaces.curses
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Curses widget to have status / command bar"""

__author__ = "Florence Birée"
__version__ = "0.2"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import curses

from ...utils.texthacks import END_OF_LINE
from .textwidget import TextWidget

class CmdWidget(TextWidget):
    """Text widget to enter a command"""

    def __init__(self, stdscr, attrman, y, x, w, text=None,
                 clipboard=None, completer=None,
                 hprev=None, hnext=None, single_char=None):
        """Create a new text widget to enter a command"""
        TextWidget.__init__(self, stdscr, attrman, y, x, 1, w, text=text,
                            multiline=False, clipboard=clipboard,
                            style='command_style', completer=completer)
        self._hprev = hprev     # prev entry in history
        self._hnext = hnext     # next entry in history
        self._single_char = single_char
        # go at the end of the text
        self._move_cursor(ax=END_OF_LINE)

    def _reinit(self, y, x, w, text=None, completer=None,
                hprev=None, hnext=None, single_char=None):
        """Reset text widget parameters"""
        if text:
            self._buffer._set_text(text)
        else:
            self._buffer._set_text('')
        self._completer = completer
        self._hprev = hprev
        self._hnext = hnext
        self._single_char = single_char
        self._resize(w, y, x)
        # go at the end of the text
        self._move_cursor(ax=END_OF_LINE)

    def _resize(self, w, y=None, x=None):
        """Resize the command text widget"""
        TextWidget._resize(self, 1, w, y, x)

    def _event_manager(self, keycode, uchar=None):
        """Event manager for the widget"""
        # if in single_char mode, return uchar if allowed
        if self._single_char:
            if uchar and uchar.lower() in self._single_char:
                return uchar.lower()
            else:
                return None
        return TextWidget._event_manager(self, keycode, uchar)

    def go_down(self, step=1, one_page=False):
        """Go forward in the history"""
        if self._hnext is not None:
            nextcmd = self._hnext()
            if not nextcmd:
                return
            self._buffer._set_text(nextcmd)
            self._draw_line(line_idx=0)
            self._move_cursor(ax=END_OF_LINE)
    go_down._userdoc_ = _("Next command in the history")

    def go_up(self, step=1, one_page=False):
        """Go backward in the history"""
        if self._hprev is not None:
            prevcmd = self._hprev()
            if not prevcmd:
                return
            self._buffer._set_text(prevcmd)
            self._draw_line(line_idx=0)
            self._move_cursor(ax=END_OF_LINE)
    go_up._userdoc_ = _("Previous command in the history")
    
    def tab(self):
        """Do nothing, tab disabled"""
        pass

    def enter(self):
        """Return the command, and leave the command mode"""
        text = self._buffer.text
        self._buffer._set_text('')
        return text
    enter._userdoc_ = _("Validate the command")


class CmdBarWidget(object):
    """Curses widget to have a status/command bar"""
    
    def __init__(self, stdscr, attrman, y, w, clipboard=None):
        """Initialize a new command/status bar widget.
        
        stdscr is the stdscr curses window.
        attrman is the curses attribute manager.
        y is the column where the bar take place
        w is the width of the bar
        clipboard is the Bristoledit clipboard manager
        """
        self._stdscr = stdscr
        self._attrman = attrman
        self._y = y
        self._w = w
        self._focused = False
        self._clipboard = clipboard
        self._cmdmode = False
        self._cmdw = None       # cmd text widget
        self._prompt = ''
        self._msg = ''
        # init curses objects
        self._win = curses.newwin(1, w, y, 0)
        self._win.bkgd(' ', attrman.get_attribute('command_style'))
        self._stdscr.refresh()
        # init the CmdWidget
        self._cmdw = CmdWidget(self._stdscr, self._attrman, self._y,
                               len(self._prompt), self._w - len(self._prompt), 
                               text='', clipboard=self._clipboard)

    def _resize(self, w, y=None):
        """Resize the command bar"""
        if y is not None:
            self._win.mvwin(y, 0)
            self._y = y
        self._win.resize(1, w)
        self._w = w
        self._win.refresh() # not ._refresh() to not refresh cmdw twice
        self._cmdw._resize(w - len(self._prompt), y)

    def _refresh(self):
        """Refresh the widget"""
        #self._win.nooutrefresh()
        self._win.refresh()
        if self._cmdmode:
            self._cmdw._refresh()
    
    def _focus(self, release=False):
        """Get or release the focus"""
        self._focused = not release
        if self._cmdmode:
            return self._cmdw._focus(release=release)
        else:
            return self

    def _event_manager(self, keycode, uchar):
        """Widget event manager"""
        pass    # nothing to do

    def _set_status(self, msg=None):
        """Change the status message of the bar"""
        if not msg:
            msg = self._msg
        else:
            self._msg = msg
        if self._cmdmode:
            return
        # save the cursor
        cy, cx = self._stdscr.getyx()
        # erase the line
        self._stdscr.move(self._y, 0)
        self._win.move(0, 0)
        self._win.clrtoeol()
        # print the new one
        self._win.addnstr(
            0, 0,
            msg.replace('\t', ' ').replace('\n', ' ')[:self._w - 1],
            self._w - 1,
            self._attrman.get_attribute('command_style')
        )
        # restore the cursor
        self._stdscr.move(cy, cx)
        self._refresh()

    def _set_cmd_mode(self, prompt=None, default_text=None, help_msg=None,
                      history_prev=None, history_next=None, single_char=None,
                      completer=None, afterchars=None):
        """Start asking a command.

        prompt is some text printed before the text field.
        default_text will pre-fill the text field.
        help_msg will be printed in the right part, and will disapear on
            the first keystroke.
        history_prev is a function that should return the previous entry
            in the history.
        history_next is a function that should return the next entry in
            the history.
        single_char will make the text fill return the first char entered
        completer, if needed, will be used to provide autocompletion
        afterchars will be default characters after the cursor
        
        After going in command mode, you should _focus() this widget, and
        start a new event loop, which should end when the event manager
        will return you a command.
        """
        if prompt:
            self._prompt = prompt
        else:
            self._prompt = ''
        # set CmdWidget values
        self._cmdw._reinit(
            self._y, len(self._prompt),
            self._w - len(self._prompt),
            text = default_text,
            completer = completer,
            hprev = history_prev, hnext = history_next,
            single_char = single_char,
        )
        # add afterchars
        if afterchars:
            self._cmdw._buffer._set_text(afterchars, None, len(default_text))
        # draw the prompt, draw the help msg
        self._win.move(0, 0)
        self._win.clrtoeol()
        self._win.addstr(
            0, 0,
            self._prompt,
            self._attrman.get_attribute('help_style')
        )
        self._cmdmode = True
        self._refresh()
        # overwrite with help_msg
        if help_msg:
            text = default_text if default_text is not None else ''
            hlen = len(help_msg)
            hstart = self._w - hlen - 1
            if hstart < len(self._prompt + text) + 4:
                hlen -= len(self._prompt + text) + 4 - hstart
                hstart = len(self._prompt + text) + 4
            self._win.addnstr(
                0, hstart,
                help_msg,
                hlen,
                self._attrman.get_attribute('help_style'),
            )
        # just refresh our win
        self._win.refresh()
    
    def _set_status_mode(self):
        """Go back in status mode"""
        self._cmdmode = False
        self._set_status()
