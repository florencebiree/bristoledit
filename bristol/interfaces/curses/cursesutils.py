# -*- coding: utf-8 -*-
###############################################################################
#       interfaces/curses/cursesutils.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interfaces.curses
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Various utility functions for the Curses interface of Bristoledit"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import struct
import fcntl
import termios
import curses
import curses.ascii as ascii

def chgat(window, y, x, num, attr):
    """Change the attribute of num characters starting from y, x
    
    This wrap curses.window.chgat in python 2.6, or do the job for older 
    versions.
    """
    try:
        window.chgat(y, x, num, attr)
    except AttributeError:
        for ix in range(x, x + num):
            cur_char = window.inch(y, x)
            window.addch(y, x, cur_char, attr)

def get_console_hw():
    """Return the height and width of the console in characters
    
    source: http://groups.google.com/group/comp.lang.python/msg/70fa595d97e90040
    """
    try:
        return int(os.environ["LINES"]), int(os.environ["COLUMNS"])
    except KeyError:
        height, width = struct.unpack(
            "hhhh", fcntl.ioctl(0, termios.TIOCGWINSZ ,"\000"*8)
        )[0:2]
        if not height:
            return 25, 80
        return height, width 

def utf8hack(keycode, getch):
    """Some character set like utf-8 use more than one byte for one
    character.
    
    If keycode (a code returned by a curses.window.getch) is the first byte
    of an utf-8 character, call getch to get others bytes, and return the
    unicode character.
    Else, if keycode is an unique (US-ASCII/UTF-8) character, return it as
    an unicode character.
    Else, return None.
    """
    # US-ASCII pritable character:
    if curses.ascii.isprint(keycode) or keycode == curses.ascii.LF:
        return bytes((keycode,)).decode('utf-8')
    # UTF-8 character under 4 bytes
    if (keycode >> 3) == 30: #0b11110:
        ubytes = bytes((keycode, getch(), getch(), getch()))
        return ubytes.decode('utf-8')
    # UTF-8 character under 3 bytes
    if (keycode >> 4) == 14: # 0b1110:
        ubytes = bytes((keycode, getch(), getch()))
        return ubytes.decode('utf-8')
    # UTF-8 character under 2 bytes
    if (keycode >> 5) == 6: #0b110:
        ubytes = bytes((keycode, getch()))
        return ubytes.decode('utf-8')
    # other
    return None

ATTR_MAP = {
    'n': curses.A_NORMAL,
    'b': curses.A_BOLD,
    'u': curses.A_UNDERLINE,
    # (i)talic is not suported, and set to A_DIM (Half bright text)
    'i': curses.A_DIM,
    'r': curses.A_REVERSE,
}

COLOR_MAP = {
    'default': -1,
    'black': curses.COLOR_BLACK,
    'red': curses.COLOR_RED,
    'green': curses.COLOR_GREEN,
    'yellow': curses.COLOR_YELLOW,
    'blue': curses.COLOR_BLUE,
    'magenta': curses.COLOR_MAGENTA,
    'cyan': curses.COLOR_CYAN,
    'white': curses.COLOR_WHITE,
}

class CursesAttributes(object):
    
    available_pair = 1
    pairs = {'default+default': 0}
    
    def __init__(self, config):
        """Initialize the curses attributes manager.
        
        config is the configuration manager of the curses interface.
        """
        self.config = config
    
    def decode(self, color):
        """Return the curse id of the color.
        color can be a curses id (<curses.COLOR_PAIRS) or a known constant
        ValueError is raised in bad cases
        """
        try:
            cid = int(color, 10)
        except ValueError:
            try:
                cid = int(color, 16)
            except ValueError:
                try:
                    cid = COLOR_MAP[color]
                except KeyError:
                    raise ValueError
        if cid < curses.COLORS:
            return cid
        else:
            raise ValueError
    
    def get_attribute(self, attribute_id, more_attr=None):
        """Return a curses attribute for attribute_id.
        
        This search in the config, and initialize colors pairs if needed.
        If attribute_id is unknown, then return editor_style.
        if more_attr, or-append this attribute.
        """
        attr_found = False
        while not attr_found:
            attr_str = self.config[attribute_id]
            if attr_str is None:
                if attribute_id is None or not '.' in attribute_id:
                    attribute_id = 'editor_style'
                else:
                    attribute_id = '.'.join(attribute_id.split('.')[:-1])
            else:
                attr_found = True
        font_style, fgcol, bgcol = attr_str.lower().split('+')
        # parse font attributes
        attr_curses = curses.A_NORMAL
        for attr in font_style:
            attr_curses = attr_curses | ATTR_MAP.get(attr, 0)
        if more_attr is not None:
            attr_curses = attr_curses | more_attr
        # append the color
        if (not (fgcol + '+' + bgcol) in self.pairs and 
                                    len(self.pairs) < curses.COLOR_PAIRS - 1):
            try:
                fgid = self.decode(fgcol)
                bgid = self.decode(bgcol)
            except ValueError:
                pair_id = 0
            else:
                pair_id = self.available_pair
                self.pairs[fgcol + '+' + bgcol] = pair_id
                self.available_pair += self.available_pair
                curses.init_pair(pair_id, fgid, bgid)
        else:
            pair_id = self.pairs[fgcol + '+' + bgcol]
        
        attr_curses = attr_curses | curses.color_pair(pair_id)
        return attr_curses

class BMessage(object):
    """A internal Bristoledit message"""
    pass

BMAbort = BMessage()
BMBufferChanged = BMessage()

