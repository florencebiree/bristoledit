# -*- coding: utf-8 -*-
###############################################################################
#       interfaces/curses/completerwidget.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interfaces.curses
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Curses widget to have a completion panel"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

try:
    from functools import reduce
except ImportError:
    pass    # we assume we use Python 2.5, where reduce is built in
import curses

from ...utils.texthacks import END_OF_FILE
from .cursesutils import BMBufferChanged

class CompleterWidget(object):
    """Curses widget to have a completion panel
    
    This widget is not intended to be focused.
    """
    
    def __init__(self, stdscr, attrman):
        """Initialize a new completion panel
        
        stdscr is the root curses window.
        attrman is the Bristoledit curses attributes manager.
        """
        # curses stuff
        self.pad = curses.newpad(1, 1)
        self.stdscr = stdscr
        self.attrman = attrman
        self.pad.bkgd(' ', self.attrman.get_attribute('compl_style'))
        # current status
        self.visible = False
        self.suggests = []
        self.cur_page = 0
        self.nb_page = 0
        self.nb_col = 1
        self.col_w = 0
        self.pages = []
        self.h = 0      # height
        self.w = 0      # weight
        self.int_h = 0  # interior height
        self.int_w = 0  # interior weight

    ## public interface

    def update(self, suggests):
        """Update the completion panel with suggests
        
        If suggests is empty, clear the suggestion window.
        """
        self.suggests = suggests
        self.cur_page = 0
        if not suggests:
            self._hide()
        else:
            self._compute_size_and_pages()
            self._show()

    def complete(self, tbuffer):
        """Try to complete the text using current suggestion results.
        If it cannot update the text, it may change the display of the
        completion panel.
        
        tbuffer is the buffer inside which the text will be completed. Its
            text, _set_text and _position properties will be used.
        
        This method return BMBufferChanged if some text has been completed.
        """
        position = tbuffer._position
        
        # if one suggestion: replace the current text by the suggestion
        if len(self.suggests) == 1:
            suggest0 = self.suggests[0]
            if suggest0.umissing_chars:
                tbuffer._set_text(
                    suggest0.umissing_chars + (suggest0.uafter_chars 
                             if suggest0.uafter_chars is not None else ''),
                    position + len(suggest0.umissing_chars),
                    position,
                )
                return BMBufferChanged
            elif suggest0.replace is not None:
                return BMBufferChanged if suggest0.replace(tbuffer) else None
            else:
                return None
        
        # if common begining, complete with it
        ucommon_chars = reduce(
            (lambda s1, s2 : s2.starts_like(s1)),
            self.suggests,
        )
        if ucommon_chars:
            tbuffer._set_text(
                ucommon_chars,
                position + len(ucommon_chars),
                position,
            )
            return BMBufferChanged
        
        # if more than one page, go to the next page
        if self.nb_page > 0:
            self.cur_page = (self.cur_page + 1) % self.nb_page
            self._show()
        return None

    ## widget interface
    
    def _resize(self):
        """Resize the completion panel"""
        if self.visible:
            self._hide()
            self._compute_size_and_pages()
            self._show()
    
    def _hide(self):
        """Hide the completion window"""
        self.visible = False
        self.pad.erase()
        self.suggests = []
        self.pages = []
    
    def _show(self):
        """Show the completion window"""
        self.visible = True
        parh, parw = self.stdscr.getmaxyx()
        cury, curx = self.stdscr.getyx()
        # compute the position
        posx = 0
        if cury in range(parh - self.h - 1, parh - 1):
            posy = 0
        else:
            posy = parh - self.h - 1
        # move the window
        self.pad.resize(self.h, self.w)
        self.pad.erase()
        # draw the box
        self.pad.box()
        # print the current page
        page = self.pages[self.cur_page]
        col_x = 1
        for col in page:
            item_y = 1
            for item in col:
                self.pad.addnstr(
                    item_y, col_x,
                    item,
                    self.int_w
                )
                item_y += 1
            col_x += self.col_w
        # refresh the screen
        self.pad.refresh(
            0, 0,
            posy, posx,
            posy + self.h-1, posx + self.w-1
        )
        # reset the cursor
        self.stdscr.move(cury, curx)

    ## internal method

    def _compute_size_and_pages(self):
        """Compute the size (height, weight) and the pages, thanks suggets"""
        parh, parw = self.stdscr.getmaxyx()
        # get weight
        self.w = parw
        self.int_w = parw - 2
        # build items
        max_label_len = max(len(suggest.ulabel) for suggest in self.suggests)
        items = [
            suggest.ulabel + (' ' * (max_label_len - len(suggest.ulabel)))
                           + '  ' + suggest.udesc + ' '
            for suggest in self.suggests
        ]
        # compute the number of columns
        max_item_len = min(max(len(i) for i in items), self.int_w)
        self.col_w = max_item_len
        self.nb_col = max(self.int_w // max_item_len, 1)
        # compute height
        self.int_h = max(min(
            (len(items) // self.nb_col) + 
                (1 if (len(items) % self.nb_col) else 0),
            (parh // 2) - 2
        ), 1)
        self.h = self.int_h + 2
        # compute number of page
        nb_items = len(items)
        nb_items_per_page = self.int_h * self.nb_col
        self.nb_page = nb_items // nb_items_per_page + (
            1 if (nb_items % nb_items_per_page) else 0
        )
        # build the pages
        # a page is a list of colums
        # a column is a list of items
        self.pages = [
            [
                [ item for item in items[
                    ((pageid * self.nb_col) + colid) * self.int_h:
                    ((pageid * self.nb_col) + colid + 1) * self.int_h]]
                for colid in range(self.nb_col)]
            for pageid in range(self.nb_page)
        ]
    
