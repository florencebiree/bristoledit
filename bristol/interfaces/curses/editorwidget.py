# -*- coding: utf-8 -*-
###############################################################################
#       interfaces/curses/editorwidget.py
#       
#       Copyright © 2009-2020, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interfaces.curses
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Main text editor widget for Bristoledit"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import curses

from ...buffer import Buffer
from ...utils.texthacks import END_OF_LINE, END_OF_FILE, char_size, tabbed
from ...utils.hooks import hooks

from .cursesutils import BMBufferChanged, chgat
from .textwidget import TextWidget, vertical

class EditorWidget(TextWidget):
    """Text editor widget"""

    def __init__(self, stdscr, configman, attrman, bbuffer, clipboard=None):
        """Create a new editor widget

            stdscr is the root curses window
            configman is the configuration manager of the Curses interface
            attrman is the Bristoledit nCurses attributes manager
            h is the height of the widget
            bbuffer is the bounded buffer to this widget
            clipboard is the Bristoledit nCurses clipboard manager
        """
        self._buffer = bbuffer  # avoid the creation of a MiniBuffer
        h, w = stdscr.getmaxyx()
        self._config = configman
        self._drawed_lines = []
        self._bracket_position = None
        self._update_lock = False
        TextWidget.__init__(self, stdscr, attrman, 0, 0, h - 1, w,
                            clipboard=clipboard, visible=False)
        
    def _get_tab_size(self):
        return self._buffer.tab_size
    _tab_size = property(fget=_get_tab_size)
    
    def _get_completer(self):
        return self._buffer._get_completer()

    def _resize(self):
        """Resize the editor widget"""
        h, w = self._stdscr.getmaxyx()
        TextWidget._resize(self, h -1,  w)
    
    # tab/indent management
    @vertical
    def tab(self):
        """Indent wit tabs or spaces"""
        pos = self._buffer._position
        if self._buffer.indent_with_spaces:
            # get the length of a tab
            lineno = self._buffer._line_of_position(pos)
            sol = self._buffer._start_of_line(lineno)
            faketxt = self._buffer.text[sol:pos] + '\t'
            nbspaces = char_size(faketxt, self._buffer.tab_size)[-1]
            self._buffer._set_text(
                ' ' * nbspaces, new_position = pos + nbspaces,
                insert_position = pos
            )
        else:
            self._buffer._set_text('\t', pos + 1, pos)
        return BMBufferChanged
    tab._userdoc_ = _("Insert a tabulation")

    def enter(self):
        """Insert a line break, autoindent if enabled"""
        msg = self._addch('\n')
        if self._buffer.auto_indent: #TODO:should be in buffer._set_text
            self._buffer.indent(self._buffer._position)
        return msg
    enter._userdoc_ = _("Insert a line break")

    def _eo_indent(self, lineno=None):
        """Return the end of the indentation part of the line lineno

        (first non space non tab character)
        If lineno is None, use the current line.
        """
        if lineno is None:
            lineno = self._vy
        linectn = self._buffer._get_line(lineno)
        sol = self._buffer._start_of_line(lineno)
        return sol + len(linectn) - len(linectn.lstrip(' \t'))
    
    @vertical
    def delch(self, step=1, before=True, one_word=False):
        """Unindent of backspace"""
        if (step == 1 and before and self._vx > 0 and
                self._buffer._position <= self._eo_indent()):
            # unindent (will move the cursor to the right position)
            self._buffer.indent(delta=-1, ref=0)
            return BMBufferChanged
        else:
            return TextWidget.delch(self, step, before, one_word)
    delch._userdoc_ = _("Delete a character")

    def go_home(self):
        """Move the cursor to the begining of the line (smart version)"""
        eo_indent = self._eo_indent()
        if self._buffer._position == eo_indent:
            self._move_cursor(ax=0)
        else:
            self._move_cursor(position=eo_indent)
    go_home._userdoc_ = _("Go at the begining of the line")

    def go_end(self):
        """Move the cursor to the end of the line (smart version)"""
        linectn = self._buffer._get_line(self._vy)
        sol = self._buffer._start_of_line(self._vy)
        eo_ctn = sol + len(linectn.rstrip())
        if self._buffer._position == eo_ctn:
            self._move_cursor(ax=END_OF_LINE)
        else:
            self._move_cursor(position=eo_ctn)
    go_end._userdoc_ = _("Go at the end of the line")

    def _highlight_bracket(self, position):
        """If needed, highlight the corresponding bracket ('()[]{}')"""
        corresp = {
            '(': ')',
            ')': '(',
            '[': ']',
            ']': '[',
            '{': '}',
            '}': '{',
        }
        # unselect the last corresponding bracket
        oldpos = self._bracket_position
        if oldpos is not None:
            oldy, oldx = self._yx_of_position(oldpos)
            self._bracket_position = None
        # check if there is a bracket to find
        start_pos = None
        try:
            br_char = self._buffer.text[position]
        except IndexError:
            pass
        else:
            if br_char in '()[]{}':
                start_pos = position
        if start_pos is None:
            try:
                br_char = self._buffer.text[position - 1]
            except IndexError:
                pass    # start_pos is already None
            else:
                if br_char in '()[]{}':
                    start_pos = position - 1
        if start_pos is not None:
            # find the corresponding bracket
            #TODO: don't go outside the screen!
            if br_char in '([{':
                pos_list = range(start_pos, len(self._buffer.text))
            else:
                pos_list = range(start_pos, -1, -1)
            br_num = 0
            for cur_pos in pos_list:
                if self._buffer.text[cur_pos] == br_char:
                    br_num += 1
                elif self._buffer.text[cur_pos] == corresp[br_char]:
                    br_num -= 1
                if br_num == 0:
                    # its here !
                    self._bracket_position = cur_pos
                    break
        # unhighlight the last corresponding bracket
        if oldpos is not None and self._bracket_position != oldpos:
            self._draw_line(line_idx=oldy)
        # highlight the corresponding bracket
        if self._bracket_position is not None and \
        self._bracket_position != oldpos:
            (by, bx) = self._yx_of_position(self._bracket_position)
            chgat(self._pad, by, bx, 1, self._attrman.get_attribute('reverse'))
            self._refresh()

    def _position_of_yx(self, y, x):
        """Return the position in the text for (y, x) cursor"""
        sol = self._buffer._start_of_line(y)
        linectn = self._buffer._get_line(y).replace('\n', '')
        csizes = char_size(linectn, self._buffer.tab_size)
        if x == END_OF_LINE:
            return sol + sum(csizes)
        else:
            curx = 0
            for p, cs in enumerate(csizes):
                if curx >= x:
                    return sol + p
                curx += cs
            return sol + len(csizes)

    def _yx_of_position(self, position):
        """Return the (y, x) cursor for position in text"""
        if not self._buffer.text:
            return (0, 0)
        if position == END_OF_FILE:
            position = len(self._buffer.text)
        y = self._buffer._line_of_position(position)
        sol = self._buffer._start_of_line(y)
        linectn = self._buffer._get_line(y)
        csizes = char_size(linectn, self._buffer.tab_size)
        return (y, sum(csizes[:position-sol]))

    def _check_pad_size(self, dh=None, dw=None):
        """Make sure the pad is at least as big as the widget.

        Other size checks are done in draw_line.
        """
        curh, curw = self._pad.getmaxyx()
        h = max(curh, self._h, self._buffer._number_of_line())
        w = max(curw, self._w)

        if curh != h or curw != w:
            self._pad.resize(h, w)

    def _draw_line(self, line_idx=None, min_position=None, max_position=None,
                  line_offset=0, min_line=None, max_line=None):
        """By default, draw visibles lines that need to be updated.
        (This is the most optimised behavior, and should be the most used)
        
        If line_idx is given, redraw the line line_idx.
        If min_position and max_position are given, draw all lines from the
        min_position line and the max_position line.
        You can use min_position=0 and max_position=END_OF_FILE to redraw the
        whole file.
        If min_line and max_line are given, draw all lines between the two.
        line_offset is the (positive) number of line to add, or the (negative)
        number of line to remove.
        """
        # save the pad cursor, get useful values
        cy, cx = self._pad.getyx()
        curh, curw = self._pad.getmaxyx()
        buff = self._buffer
        nblines = buff._number_of_line()
        # make sure the drawed_list is long enough
        if len(self._drawed_lines) < nblines:
            self._drawed_lines += [False] * (nblines - len(self._drawed_lines))
        
        # make the line list 
        miny = None
        maxy = None
        if line_idx is not None:
            miny = line_idx
            maxy = line_idx + 1
        elif min_line is not None and max_line is not None:
            miny = max(min_line, 0)
            maxy = min(max_line, nblines)
        elif min_position is not None and max_position is not None:
            miny, x = self._yx_of_position(min_position)
            maxy, x = self._yx_of_position(max_position)
            maxy += 1

        if miny is None or maxy is None:
            # make sure all the pad, just the pad is drawn
            # ...almost: if there is not a lot of line, try
            # to draw more of them
            miny = self._dy
            maxy = min(self._dy + self._h, nblines)
            bigmaxy = min(self._dy + (self._h * 2), nblines)
        else:
            # tag asked lines as undrawed
            for i in range(miny, maxy):
                self._drawed_lines[i] = False
        # find the undrawed subset
        notdrawed = [lno for lno in range(miny, maxy)
                     if not self._drawed_lines[lno]]
        if not notdrawed:
            # just refresh
            self._refresh()
            return
        elif len(notdrawed) == 1:
            try:
                notdrawed += [lno for lno in range(maxy, bigmaxy)
                              if not self._drawed_lines[lno]]
            except NameError:
                pass
        miny, maxy = notdrawed[0], notdrawed[-1] + 1

        minpos = buff._start_of_line(miny)
        if maxy >= nblines:
            maxpos = len(buff.text)
        else:
            maxpos = buff._start_of_line(maxy) + len(buff._get_line(maxy))
        
        # retreive subset of data to work on
        line_list = range(max(miny, 0), min(maxy, nblines))
        subtext = buff.text[minpos:maxpos]
        
        # manage line insertion/removal
        if line_offset > 0:
            self._pad.move(miny, 0)
            for i in range(line_offset):
                self._pad.insertln()
        elif line_offset < 0:
            self._pad.move(miny, 0)
            for i in range(-line_offset):
                self._pad.deleteln()
        
        # retreive the attribute map
        attrmap = buff._get_attribute_map(minpos, maxpos)
        
        # highlight tabs
        if self._config['show tabs']:
            for pos in (pos for pos, c in enumerate(subtext) if c == '\t'):
                attrmap[pos:pos+1] = 'reverse'
        
        # highlight brackets -- disarmed
        # (for performances reason, it's done by a chgat)
        #if self._bracket_position is not None:
        #    bpos = self._bracket_position - minpos
        #    attrmap[bpos:bpos+1] = 'reverse'
        
        # show the selection
        if self._buffer._mark_position is not None:
            minsel = min(self._buffer._mark_position, self._select_end)
            maxsel = max(self._buffer._mark_position, self._select_end )
            minsel = max(minsel, minpos) # truncate to minpos
            maxsel = min(maxsel, maxpos) # truncate to maxpos
            if minsel < maxsel: # else selection outside our scope
                attrmap[minsel-minpos:maxsel-minpos] = 'reverse'
        
        # split the map in lines
        attrmapgen = attrmap.split(subtext)
        
        # draw stuff
        for lineno, l_attrmap in zip(line_list, attrmapgen):
            sol = self._position_of_yx(lineno, 0)   # start of line
            # clear the line
            self._pad.move(lineno, 0)
            self._pad.clrtoeol()
            # display the line:
            # for each token
            xoffset = 0
            for (startc, endc), attrid in l_attrmap.items():
                token = buff.text[sol+startc:sol+endc]
                # print the token with its style, with expanded tabs
                exptxt = tabbed(token, buff.tab_size, xoffset)
                # check if the pad has the right size:
                if curw < xoffset + len(exptxt) + 1:
                    curw += xoffset + len(exptxt) + 10
                    self._pad.resize(curh, curw)
                self._pad.addstr(
                    lineno, xoffset, exptxt,
                    self._attrman.get_attribute(attrid),
                )
                xoffset += len(exptxt)
            # display the 80 charachers line
            if self._config['show 80 chars line']:
                try:
                    chgat(self._pad, lineno, 80, 1,
                      self._attrman.get_attribute('80char_style'))
                except curses.error:
                    _debug_print("drawline: chgat for 80char failed")
        
        # if a line is deleted, and empty lines after the end of file are
        # displayed, clear them
        if (nblines < self._dy + self._h):
            for lineno in range(nblines, self._dy + self._h):
                self._pad.move(lineno, 0)
                self._pad.clrtoeol()

        # update drawed lines
        self._drawed_lines[miny:maxy] = [True] * (maxy - miny)

        # restore the cursor
        self._pad.move(cy, cx)

        self._refresh()

