# -*- coding: utf-8 -*-
###############################################################################
#       interfaces/gtk/commandenv.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interfaces.gtk
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""A GTK environment for the bristol command bar"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import sys

import pygtk
pygtk.require("2.0")
import gtk

MODE_EDIT = 0
MODE_SHELL = 1
MODE_WITCH = 2

DEFAULT = 'editor.'

class CommandEnv:
    """Command bar environment for bristol"""
    
    def __init__(self, editor):
        """Initialize the environment"""
        self.editor = editor
        
        self.mode = MODE_EDIT
        self._statusbar = None
        self._commandbar = None
        self._statusmsg = None
        self._prompt = None
        
        # Shell mode stuff
        self.shell_env = {'editor':self.editor}
        self.witch_compl = None
        
        self.editor.plugins.load('pythonshell')
        PythonShell = self.editor.plugins.get['pythonshell']
        self.python_shell = PythonShell(local_env=self.shell_env)
        
        self.editor.plugins.load('pythonshellcompleter')
        PythonShellCompleter = self.editor.plugins.get['pythonshellcompleter']
        self.shell_compl = PythonShellCompleter(self.editor, self.shell_env)
    
    def _get_statusbar(self):
        """Get the statusbar widget when the interface is ready"""
        if not self._statusbar:
            self._statusbar = self.editor.interface._gtk_interface.\
                                get_object("statusbar")
        return self._statusbar
    statusbar = property(fget=_get_statusbar)
    
    def _get_commandbar(self):
        """Get the commandbar widget when the interface is ready"""
        if not self._commandbar:
            self._commandbar = self.editor.interface._gtk_interface.\
                               get_object("commandbar")
        return self._commandbar
    commandbar = property(fget=_get_commandbar)
    
    def _get_statusmsg(self):
        """Get the statusmsg widget when the interface is ready"""
        if not self._statusmsg:
            self._statusmsg = self.editor.interface._gtk_interface.\
                               get_object("statusmsg")
        return self._statusmsg
    statusmsg = property(fget=_get_statusmsg)
    
    def _get_prompt(self):
        """Get the prompt widget when the interface is ready"""
        if not self._prompt:
            self._prompt = self.editor.interface._gtk_interface.\
                            get_object("prompt")
        return self._prompt
    prompt = property(fget=_get_prompt)
    
    def switch(self, interpreter=None, completer=None, start_text=None,
               help_msg=None):
        """Change the current command mode
        
        If not in MODE_EDIT, go to MODE_EDIT. 
        If interpreter is not None, go to MODE_WITCH
        Else, go to MODE_SHELL.
        
        For the MODE_WITCH, you can specify a completer
        """
        if self.mode != MODE_EDIT:
            self.mode = MODE_EDIT
            # commandbar.size <- little
            # print std status
        elif interpreter is not None:
            self.mode = MODE_WITCH
            # commandbar.size <- big
            # print hlp if needed
        else:
            self.mode = MODE_SHELL
            self.commandbar.set_text(u'editor.')
            self.statusmsg.set_text(_(u'Hit <Tab> for a list of commands'))
            self.expand()
    
    def expand(self):
        """Expand the commandbar to let user enter text"""
        self.commandbar.set_width_chars(-1)
        self.statusbar.set_child_packing(self.statusmsg, False, False, 2,
                                         gtk.PACK_END)
        self.statusbar.set_child_packing(self.commandbar, True, True, 2,
                                         gtk.PACK_START)
        print('plop')
    
    def unexpand(self):
        """Unexpand the commandbar to allow status messages to be printed"""
        self.statusbar.set_child_packing(self.commandbar, True, True, 2,
                                         gtk.PACK_START)
        self.commandbar.set_width_chars(len(DEFAULT))
        self.statusbar.set_child_packing(self.statusmsg, False, False, 2,
                                         gtk.PACK_START)
        
