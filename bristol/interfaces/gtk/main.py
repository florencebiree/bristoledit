# -*- coding: utf-8 -*-
###############################################################################
#       interfaces/gtk/main.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol.interfaces.gtk
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Core of the GTK+ interface for bristol"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import sys

import pygtk
pygtk.require("2.0")
import gtk
import gtksourceview2

from bristol.buffer import Buffer
from bristol.utils.hooks import hooks, improve
from bristol.utils.texthacks import END_OF_FILE, END_OF_LINE

from bristol.interfaces.gtk.commandenv import CommandEnv
from bristol.interfaces.gtk.commandenv import MODE_EDIT, MODE_SHELL, MODE_WITCH

class GtkInterface(object):
    """GtkInterface, to use Bristol with a GUI"""
    
    _gui_name = 'gui.glade'
    _gui_path = [os.path.join(prefix, 'share', 'bristoledit', _gui_name)
                 for prefix in (
                     sys.prefix,
                     os.path.join(os.path.dirname(sys.argv[0]), os.path.pardir)
                 )]
    
    ### initialization stuff
    
    def __init__(self, Editor, filenames, screen_charset=None,
                 options=None):
        """Initialize the curses interface"""
        self.editor = Editor(self, options=options)
        self._screen_charset = screen_charset
        self._init_filenames = filenames
        
        self._userdoc_ = _(u"GTK+ interface for Bristoledit.")
        
        # Find the gui description file
        self._gui_file = None
        for path in self._gui_path:
            if os.path.exists(path):
                self._gui_file = path
                break
        
        # Initialize the command bar environment
        self._cmdbar = CommandEnv(self.editor)
        
        self._config = self.editor.config._get_component_manager(
            'GtkInterface',
            _(u"This section configure the GTK interface.")
        )
        
        self._ewigs = {}
        self._hook()
    
    ### externals entry points
    
    def __repr__(self):
        """Python representation of this interface"""
        return "%s.GtkInterface(editor)" % (
            self.__module__
        )
    
    def main(self):
        """Build the interface and run the main loop of the GTK interface"""
        self._gtk_interface = gtk.Builder()
        self._gtk_interface.add_from_file(self._gui_file)
        self._mainwin = self._gtk_interface.get_object("mainwin")
        
        # add the editor widget
        self._view = gtksourceview2.View()
        scrolledview = self._gtk_interface.get_object("scrolledview")
        scrolledview.add(self._view)
        self._view.show()
        self._view.grab_focus()
        
        self._mainwin.maximize()
        
        self._gtk_interface.connect_signals(self)
        
        self.editor._std_init(self._init_filenames)
        
        gtk.main()
    
    ### Public interface
    def switch_command_mode(self, interpreter=None, completer=None,
                            start_text=None, help_msg=None):
        """Change the current command mode
        
        If not in MODE_EDIT, go to MODE_EDIT. 
        If interpreter is not None, go to MODE_WITCH
        Else, go to MODE_SHELL.
        
        For the MODE_WITCH, you can specify a completer
        """
        self._cmdbar.switch(interpreter, completer, start_text, help_msg)
    
    def ask(self, prompt, default=None, help_msg=None):
        """Open a command input to get text and return it"""
        pass
    
    def ask_a_filename(self, default=None, prompt=None):
        """Open a command input to choose a filename.
        
        Return the filename.
        """
        pass
    
    def ask_a_question(self, question_text, answer_list):
        """Ask a simple question, and return the index of the answer"""
        pass
    
    def set_status_msg(self, status_msg):
        """Set a temporary status message"""
        pass
    
    ### Event manager
    
    def exit(self):
        """Quit Bristol"""
        sys.exit(0)
    
    def _on_mainwin_destroy(self, widget):
        """Exit Bristol when closing the window"""
        self.editor.exit()
    
    def _on_commandbar_grab_focus(self, widget):
        """Switch to the shell mode"""
        self.switch_command_mode()
    
    #### hooks
    def _hook(self):
        """Sets hooks of the main interface"""
        
        #@hooks(type(self.editor), 'open', self)
        def open(oldself, oldopen, filename=None, encoding=None,
                 endofline=None):
            """Open a new editor widget after opening the buffer
            
            If there is no filename given, ask for a filename.
            
            Return the buffer
            """
            new_buffer = oldopen(oldself, filename, encoding=encoding,
                                 endofline=endofline)
            if new_buffer not in self.ewigs:
                self.ewigs[new_buffer] = gtksourceview2.Buffer()
            
            self.editor.switch_to(new_buffer)
            return new_buffer
        
        #@hooks(type(self.editor), 'new', self)
        def new(oldself, oldnew):
            """Open a new editor widget after creating a new buffer, and switch
            to it.
            
            Return the buffer
            """
            new_buffer = oldnew(oldself)
            self.ewigs[new_buffer] = gtksourceview2.Buffer()
            self.editor.switch_to(new_buffer)
            #self.command_bar.refresh()
            return new_buffer
        
        #@hooks(type(self.editor), 'next', self)
        def next(oldself, oldnext):
            """Go to the next buffer, and display the right editor widget
            
            Return the new buffer
            """
            buffer = oldnext(oldself)
            
            #self.current_ewig().move_cursor()
            #self.current_ewig().refresh()
            #self.edwin.overwrite(self.stdscr)
            #self.command_bar.refresh()
            #return buffer
        
        #@hooks(type(self.editor), 'prev', self)
        #def prev(oldself, oldprev):
            #"""Go to the previous buffer, and display the right editor widget
            
            #Return the new buffer
            #"""
            #buffer = oldprev(oldself)
            #self.current_ewig().move_cursor()
            #self.current_ewig().refresh()
            #self.edwin.overwrite(self.stdscr)
            #self.command_bar.refresh()
            #return buffer
        
        #@hooks(type(self.editor), 'switch_to', self)
        #def switch_to(oldself, oldswitch_to, buffer):
            #"""Change the current buffer to the given buffer, and display the
            #right editor widget
            
            #Return the new current buffer
            #"""
            #buffer = oldswitch_to(oldself, buffer)
            #self.current_ewig().move_cursor()
            #self.current_ewig().refresh()
            #self.edwin.overwrite(self.stdscr)
            #self.command_bar.refresh()
            #return buffer
        
        #@hooks(Buffer, 'save', self)
        #def save(oldself, oldsave, filename=None, encoding=None,
                 #endofline=None):
            #oldsave(oldself, filename, encoding, endofline)
            #self.command_bar.refresh() # CHECK RETURN VALUE
        
        #@hooks(Buffer, 'set_text', self)
        #def set_text(oldself, set_text, new_text, new_position=None,
                     #insert_position=None, delete_position=None, delete_len=0):
            #"""Refresh the editor widget after the modification of the text"""
            ## check line insertions/removes
            #line_offset = new_text.count('\n')
            #if delete_position is not None:
                #line_offset -= oldself.text[delete_position:
                                    #delete_position + delete_len].count('\n')
            
            #set_text(oldself, new_text, new_position, insert_position,
                     #delete_position, delete_len)
            ## if there is a editor widget bound to this buffer:
            #if oldself in self.ewigs :
                ## if needed, resize the pad
                #self.ewigs[oldself].resize(
                    #dh=line_offset,
                    #dw=len(new_text) + delete_len,
                #)
                ## refresh the changed area
                #if insert_position is None:
                    #self.ewigs[oldself].pad.erase()
                    ## full refresh
                    #self.ewigs[oldself].drawed_lines = []
                    #self.ewigs[oldself].draw_line(
                        #min_position=0,
                        #max_position=END_OF_FILE,
                        #line_offset=line_offset,
                    #)
                #else:
                    ## compute bounds
                    #if delete_position is not None:
                        #min_pos = min(insert_position, delete_position)
                        #max_pos = max(insert_position + len(new_text),
                                      #delete_position + delete_len)
                    #else:
                        #min_pos = insert_position
                        #max_pos = insert_position + len(new_text)
                    ## draw the area
                    #self.ewigs[oldself].draw_line(
                        #min_position=min_pos,
                        #max_position=max_pos,
                        #line_offset=line_offset,
                    #)
                ## if needed, move the cursor
                #if new_position is not None:
                    #self.ewigs[oldself].move_cursor(position=new_position)
                ## if its the current buffer, put changes to the screen
                #if oldself is self.editor.current:
                    #self.ewigs[oldself].refresh()
                    #self.edwin.overwrite(self.stdscr)
                
        #@hooks(type(self.editor.config), 'bind', self)
        #def bind(oldself, oldbind, function, key=None):
            #"""Bind a key with a function"""
            #if key is None:
                #self.command_bar.display_msg(_(u"Press the key to bind."))
                #key = curses.keyname(self.stdscr.getch())
            #oldbind(oldself, function, key)
            #self.command_bar.display_msg(_(u"The key %s is bind.") % key)
    
