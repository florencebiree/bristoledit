# -*- coding: utf-8 -*-
###############################################################################
#       __init__.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Main Bristoledit package.

Here you can found the modules:
  * bristol.editor          Main program for bristol, define general editor
                            functions
  * bristol.buffer          Text buffer, to manage the content of a file
  * bristol.config          The configuration manager of bristol
  * bristol.pluginmanager   The plugin manager of bristol

And thoses packages:
  * bristol.interface       All interfaces for bristol
  * bristol.plugins         Official bristol plugins
  * bristol.utils           Usefull tools for others modules
  * bristol.settings        Settings files for bristoledit.
"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"
