# -*- coding: utf-8 -*-
###############################################################################
#       timedtest.py
###############################################################################
"""Utility python module to make performances tests

Code from http://www.biologeek.com/bonnes-pratiques,python/benchmarks-map-filter-vs-list-comprehensions/

    Copyright David Larlet (and maybe Tarek Ziade)
    Licence: CC-BY-NC-SA

"""

from test import pystone
import time

kPs = 1000
TOLERANCE = 0.5 * kPs

def mesure_pystone():
    pystone.pystones(loops=pystone.LOOPS)
    pystone.pystones(loops=pystone.LOOPS)
    return pystone.pystones(loops=pystone.LOOPS)

def timedtest(function, local_pystones=mesure_pystone()):
    """ Decorator to measure execution time in pystones """
    def wrapper(*args, **kw):
        all = []
        for i in range(3):
            start_time = time.time()
            try:
                res = function(*args, **kw)
            finally:
                total_time = time.time() - start_time
                if total_time == 0:
                    temps = 0
                else:
                    ratio = local_pystones[0] / local_pystones[1]
                    temps = total_time / ratio
                all.append(temps)
        print '%d pystones' % min(all)
        return res
    return wrapper

def timedtestOnetime(function, local_pystones=mesure_pystone()):
    """ Decorator to measure execution time in pystones """
    def wrapper(*args, **kw):
        start_time = time.time()
        try:
            res = function(*args, **kw)
        finally:
            total_time = time.time() - start_time
            if total_time == 0:
                temps = 0
            else:
                ratio = local_pystones[0] / local_pystones[1]
                temps = total_time / ratio
        print '%d pystones' % temps
        return res
    return wrapper
