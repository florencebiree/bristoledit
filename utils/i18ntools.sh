#! /bin/sh
###############################################################################
#       utils/i18ntools.sh
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of Bristoledit
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
# Script to generate/update i18n files

if [ $# -lt 1 ]
then
	echo "Bad number of parameters." >&2
	echo "Usage: $0 make-pot|init|merge|compile" >&2
	echo "\tmake-pot\tre-generate the translation template" >&2
	echo "\tinit\tinitialize a new translation for your language" >&2
	echo "\tmerge\tupdate the translation file for your language" >&2
	echo "\tcompile\tcompile the translation file for your language" >&2
	exit 1
fi

appname="bristoledit"
mode="$1"
lang=`echo $LANG | cut -c 1,2`
project_dir=`dirname $0`/..
template=$project_dir/$appname.pot
locale_dir=$project_dir/share/locale/$lang/LC_MESSAGES

if [ $mode = 'make-pot' ]
then
	# generation of the template
	xgettext -k_ -kN_ -o $template `find $project_dir -name '*.py'`
	
elif [ $mode = 'init' ]
then
	# generation of a new language file
	mkdir -p $locale_dir
	msginit -i $template -o $locale_dir/$appname.po
	
elif [ $mode = 'merge' ]
then
	# update a language file
	msgmerge -U $locale_dir/$appname.po $template
	
elif [ $mode = 'compile' ]
then
	# compile the language file
	cd $locale_dir
	msgfmt $appname.po -o $appname.mo
	cd -
	
else
	echo "Unreconized command" >&2
	exit 42
fi

exit 0
