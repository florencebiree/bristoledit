#! /usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
#       utils/test_all.py
#       
#       Copyright © 2010-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of bristol
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Run all unit tests for bristoledit"""

import unittest
import os
import sys

# make the root directory for searching tests
up = lambda adir: os.path.split(adir)[0]
testroot = os.path.dirname(__file__)
if testroot == '':
    testroot = '.'
testroot = os.path.realpath(testroot)
testroot = os.path.join(up(testroot), 'bristol')

# make the all test suite
def test_suite(rdir=testroot):
    tests = []
    for fname in os.listdir(rdir):
        fpath = os.path.join(rdir, fname)
        if os.path.isdir(fpath):
            if fname == 'tests':
                sys.path.append(fpath)
                mods = (f.replace('.py', '') for f in os.listdir(fpath)
                        if f.startswith('test_') and f.endswith('.py'))
                for pymod in mods:
                    tests.append(__import__(pymod).test_suite())
            else:
                tests += test_suite(fpath)
    return unittest.TestSuite(tests)

if __name__ == '__main__':
    unittest.main(defaultTest='test_suite')
