#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
#       setup.py
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of Bristoledit
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Setup for BristolEdit"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2009-2018, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from distutils.core import setup
from os.path import join

setup(
    name="bristoledit",
    version=__version__,
    url="http://dev.filyb.info/bristoledit/",
    download_url="http://dev.filyb.info/bristoledit/",
    description="Powerful text editor.",
    long_description= """\
Bristoledit is a powerful text editor aimed for developers, with a smart
object-like interface.

It target people wo want a simple keyboard-driven interface, without the pain
of years of learning, and a full integration with part of your system you
already love, like your shell.
""",
    author="Florence Birée",
    author_email="flo@biree.name",
    license="GNU General Public License version 3",
    platforms=["Unix"],
    classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console :: Curses',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: GNU General Public License (GPL)',
          'Natural Language :: English',
          'Natural Language :: French',
          'Operating System :: POSIX :: Linux',
          'Operating System :: POSIX',
          'Operating System :: Unix',
          'Programming Language :: Python',
          'Topic :: Text Editors',
          ],
    packages=['bristol', 'bristol.plugins', 'bristol.utils', 'bristol.settings',
                'bristol.interfaces', 'bristol.interfaces.dummy',
                'bristol.interfaces.curses', 'bristol.interfaces.gtk'],
    requires=['xdg', 'chardet', 'pygments', 'pyenchant'],
    scripts=[
        join('bin', 'bristoledit'),
        join('bin', 'synctexinfos.sh'),
    ],
    
    data_files=[
        # share/bristoledit
        (join('share', 'bristoledit'), [
            join("share", "bristoledit", "gui.glade"),
        ]),
        # share/doc/bristoledit
        (join('share', 'doc', 'bristoledit'), [
            "AUTHORS",
            "CHANGELOG",
            "COPYING",
            "INSTALL.rst",
            "README.rst",
            join("doc", "plugins.rst"),
            join("doc", "internal-plugins.rst"),
            join("doc", "dev-plugins.rst"),
            join("doc", "keybindings.rst"),
        ]),
        # share/locale/fr/LC_MESSAGES
        (join('share', 'locale', 'fr', 'LC_MESSAGES'), [
            join("share", "locale", "fr", "LC_MESSAGES", "bristoledit.po"),
            join("share", "locale", "fr", "LC_MESSAGES", "bristoledit.mo"),
        ]),
        # share/man/man1
        (join("share", "man", "man1"), [
            join("man", "bristoledit.1.gz"),
        ]),
    ],
)
