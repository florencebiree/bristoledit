#!/bin/sh
###############################################################################
#       synctexinfos.sh
#       
#       Copyright © 2009-2018, Florence Birée <flo@biree.name>
#       
#       This file is a part of Bristoledit
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
# utility script to echo synctex infos to stdout
# to be used with zathura and bristoledit
# usage: synctexinfos.sh line column input

line="$1"
column="$2"
input="$3"

echo "SyncTeX result begin"
echo "Input:$input"
echo "Line:$line"
echo "Column:$column"
echo "SyncTeX result end"
exit 0
